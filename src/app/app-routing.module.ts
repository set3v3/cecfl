import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Error404Component } from './components/error-404/error-404.component';
import { ComponentsModule } from './components/components.module';


const routes: Routes = [
  { path: '', loadChildren: () => import('./modules/web-site/web-site.module').then(m => m.WebSiteModule) },
  { path: 'pre-auth', loadChildren: () => import('./modules/pre-auth/pre-auth.module').then(m => m.PreAuthModule) },
  { path: 'post-auth', loadChildren: () => import('./modules/post-auth/post-auth.module').then(m => m.PostAuthModule) },

  { path: '404', component: Error404Component },
  //{ path: '', redirectTo: 'fontend/home', pathMatch: 'full' },
  { path: '**', redirectTo: '404', pathMatch: 'full' }
  
]

@NgModule({
  imports: [
  	RouterModule.forRoot(routes),
  	ComponentsModule
  ],
  exports: [RouterModule],
  declarations: []
})
export class AppRoutingModule { }



