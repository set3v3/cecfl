import { Injectable } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import 'rxjs/add/operator/map';

@Injectable()
export class EmailValidatorsProvider {

  constructor() {

  }
  validateEmail(control: FormControl) {
    // tslint:disable-next-line:prefer-const
    let emailPattern = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (!emailPattern.test(control.value)) {
      return ({ InvalidEmail: true });
    }
    return (null);
  }

}
