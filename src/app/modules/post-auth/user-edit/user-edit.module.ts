import { NgModule } from '@angular/core';
import { UserEditRouteModule } from './user-editroutes.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { AuthService } from '../../../services/apis/auth.service';
import { UserEditComponent } from './user-edit.component';
import { MatSortModule } from '@angular/material';
import { UserMasterService } from 'src/app/services/apis/usermaster';
import { CKEditorModule } from 'ngx-ckeditor';
import { RegistrationService } from '../../../services/apis/registration.service';
import { UserEditService } from '../../../services/apis/useredit.service';




@NgModule({
  imports: [
    UserEditRouteModule,
    SharedModule,
    MatSortModule,
    CKEditorModule

  ],
  declarations: [
    UserEditComponent,
    
  ],
  entryComponents: [
    UserEditComponent,
    
  ],
  providers: [AuthService, UserMasterService,RegistrationService,UserEditService]
})
export class UserEditModule{ }
