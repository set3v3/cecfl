import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ToastProvider } from '../../../shared/modules/toast/toast.provider';
import { UserEditService } from '../../../services/apis/useredit.service';
import { UserMasterService } from 'src/app/services/apis/usermaster';
import { UserStorageProvider } from 'src/app/services/storage/user-storage.service';
import { NgxUiLoaderService,SPINNER } from 'ngx-ui-loader';
import { CommonService } from 'src/app/services/apis/common.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
    selector: 'app-user-edit',
    templateUrl: './user-edit.component.html',
    styleUrls: ['./user-edit.component.scss']
})

export class UserEditComponent implements OnInit {
    spinnerType = SPINNER.rectangleBounce;
    showLoader: boolean;
    public editorValue: string = '';
    addRegisterform: FormGroup;
    msg;
    userId: any;
    successMsg: boolean = false;
    errorMsg: boolean = false;
    startDate = new Date(1990, 0, 1);
    show: boolean = false;
    pageEdit:boolean =false;
    pageView:boolean = false;
    Citylisting: Array < any > ;
    searchParam: any;
    entityNameTitle: any;
    entityData:any= [];
    length;
    p: number = 1;
    limit:number =10
	itemsPerPage:number = 10;
    EntityParam: any = {};
    f1submitted: boolean;
    postalAddress:boolean = false;
    minDate = new Date(1900, 0, 1);
    maxDate = new Date();
    constructor(
        private translate: TranslateService,
        private router: Router,
        private toast: ToastProvider,
        private route: ActivatedRoute,
        private UserMasterApi: UserMasterService,
        private UsersupdateApi: UserEditService,
        private memberApi: UserMasterService,
        private userStorage: UserStorageProvider,
        private ngxService:NgxUiLoaderService,
        private common :CommonService,
        
        ) 
   {
        this.newsformbuild();
        this.Citylisting = [];
        this.getCityList();
        this.showLoader = false;
        this.entityData = [];
    }

    ngOnInit() {
        console.log('oj')
        this.route.params.subscribe(params => {
            if (params) {
                //console.log(params);
                if (params['id']) {
                    //console.log(params['id']);
                    this.userId = +params['id'];
                    if (this.userId > 0) {
                        this.getuserdetaiils();
                    }
                    //this.pageView = param['param']
                }
                if(params['param'] == 'edit'){
                    this.pageEdit = true
                }
                if(params['param'] == 'view'){
                    this.pageView = true
                }
            }
        });
        this.EntityParam.page = this.p;
		this.getEntityList();
    }
    getEntityList = () => {
		this.ngxService.start();
		this.memberApi.memberList(this.userId).subscribe((res: any) => {
			this.ngxService.stop();
      this.entityData = res.members;
      console.log(this.entityData)
			this.length = res['total'];

		}, (err) => {
			this.ngxService.stop()
		});
	}
    handleChangetax(value) {

        if (value == 1) {
            this.show = true;
            this.addRegisterform.get('taxexmptconfno').setValidators([Validators.required, Validators.pattern("^[a-zA-Z0-9\.\/\,\@\#\$\%\^\&\*\+\~\!\d./-]+(?: [a-zA-Z0-9\.\/\,\@\#\$\%\^\&\*\+\~\!\d./-]+)*$")]);
            this.addRegisterform.get('dateofissue').setValidators([Validators.required]);
        } else if (value == 0) {
            this.show = false;
            // this.addRegisterform.removeControl('taxexmptconfno');
            // this.addRegisterform.removeControl('dateofissue')
            this.addRegisterform.get('taxexmptconfno').setValidators([]);
            this.addRegisterform.get('dateofissue').setValidators([]);
            this.addRegisterform.patchValue({
                taxexmptconfno: '',
                dateofissue: ''
            });
            
            //console.log('false')
        }
        this.addRegisterform.updateValueAndValidity();
    }


    // get f1() { return this.addRegisterform.controls; };
    newsformbuild() {
        const emailRegx = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        const phoneNumber = "^(\+\d{1,3}[- ]?)?\d{10}$";
        const phoneRegx = /^[6-9]\d{9}$/;
        const addrRegx = "^[a-zñáéíóúüA-ZÑÁÉÍÓÚÜ0-9\.\/\,\@\#\$\%\^\&\*\+\~\!\d./-]+(?: [a-zñáéíóúüA-ZÑÁÉÍÓÚÜ0-9\.\/\,\@\#\$\%\^\&\*\+\~\!\d./-]+)*$";
        this.addRegisterform = new FormGroup({

            category: new FormControl('', [Validators.required]),

            name_of_director: new FormControl('', [Validators.required, Validators.pattern(addrRegx)]),

            entity_name: new FormControl('', [Validators.required, Validators.pattern("^[a-zñáéíóúüA-ZÑÁÉÍÓÚÜ0-9]+(?: [a-zñáéíóúüA-ZÑÁÉÍÓÚÜ0-9]+)*$")]),

            dateofregis: new FormControl('', [Validators.required]),

            dateofincorporation: new FormControl('', [Validators.required]),

            registrationnumber: new FormControl('', [Validators.required, Validators.pattern(addrRegx)]),

            taxexmpt: new FormControl('', [Validators.required]),

            email: new FormControl('', [Validators.required, Validators.pattern(emailRegx)]),

            phone: new FormControl('', [Validators.required, Validators.pattern(phoneRegx)]),

            taxexmptconfno: new FormControl('', [Validators.required]),

            dateofissue: new FormControl('', [Validators.required]),
            
            physicaladdr1: new FormControl('', [Validators.required, Validators.pattern(addrRegx)]),

            city: new FormControl('', [Validators.required]),

            state1: new FormControl('', [Validators.required]),

            zip: new FormControl('', [Validators.required]),

            postal_addr2: new FormControl('', [Validators.required, Validators.pattern(addrRegx)]),
            postal_city: new FormControl('', [Validators.required]),

            postal_state: new FormControl('', [Validators.required]),

            postal_zip: new FormControl('', [Validators.required]),

            activeStatus: new FormControl('', [Validators.required]),

            checkPostalAddress:new FormControl()

        });
    }

    addRegister = (value) => {
        value.id=this.userId
        //console.log(value)
        this.f1submitted = true;
        if (this.addRegisterform.valid) {
            this.ngxService.start();
            this.UsersupdateApi.edituser(value).subscribe(
                (res: any) => {
                    //console.log(res);
                    this.ngxService.stop();
                    this.toast.success(this.translate.instant('MESSAGE.PROFILE_UPDATE_SUCCESS'));
            },
                (err) => {
                    this.ngxService.stop();
                    this.msg = err['message'];
                    this.toast.success(this.msg); return; 
                }
            );
        }

    }




    samefieldcopy(value) {
        //console.log(value)
        //console.log('value.target.value' ,value)
        if(value==true){
          this.addRegisterform.patchValue({
              postal_addr2: this.physicaladdr1.value,
              postal_city: this.city.value,
              postal_state: this.state1.value,
              postal_zip: this.zip.value
          })
        }else{
          this.addRegisterform.patchValue({
              postal_addr2: '',
              postal_city: '',
              postal_state:'',
              postal_zip: ''
          })
        }
        
    }


    getCityList() {
        this.searchParam = {
          search: 'PR',
        };
        this.UserMasterApi.citySearch(this.searchParam).subscribe((res: any) => {
          if (res.success) {
            this.Citylisting = res.data;

            console.log('==City==>' + this.Citylisting);
          }
        }, (err) => {});
    }


    get city() {
        return this.addRegisterform.get('city')
    }
    get category(){
        return this.addRegisterform.get('category')
      }
    get entity_name() {
        return this.addRegisterform.get('entity_name')
    }
    get name_of_director() {
        return this.addRegisterform.get('name_of_director')
    }
    get dateofincorporation() {
        return this.addRegisterform.get('dateofincorporation')
    }
    get dateofregis() {
        return this.addRegisterform.get('dateofregis')
    }
    get registrationnumber() {
        return this.addRegisterform.get('registrationnumber')
    }
    get taxexmpt() {
        return this.addRegisterform.get('taxexmpt')
    }

    get taxexmptconfno() {
        return this.addRegisterform.get('taxexmptconfno')
    }
    get dateofissue() {
        return this.addRegisterform.get('dateofissue')
    }
    get email() {
        return this.addRegisterform.get('email')
    }

    get phone() {
        return this.addRegisterform.get('phone')
    }

    get physicaladdr1() {
        return this.addRegisterform.get('physicaladdr1')
    }

    get state1() {
        return this.addRegisterform.get('state1')
    }

    get zip() {
        return this.addRegisterform.get('zip')
    }

    get postal_addr2() {
        return this.addRegisterform.get('postal_addr2')
    }

    get postal_city() {
        return this.addRegisterform.get('postal_city')
    }

    get postal_state() {
        return this.addRegisterform.get('postal_state')
    }

    get postal_zip() {
        return this.addRegisterform.get('postal_zip')
    }
    get activeStatus() {
        return this.addRegisterform.get('activeStatus')
    }
    get checkPostalAddress(){
        return this.addRegisterform.get('checkPostalAddress')
    }
    getuserdetaiils = () => {
        this.UsersupdateApi.view(this.userId).subscribe(
            data => {
                console.log('==activeStatus==>' + data.user.activeStatus);
                this.handleChangetax(data.user.taxexmpt)

                this.entityNameTitle = data.user.entity_name;
                if( data.user.physicaladdr1 == data.user.postal_addr2 && data.user.city == data.user.postal_city && data.user.postal_state == data.user.postal_state && data.user.zip ==data.user.postal_zip){
                    this.postalAddress = true
                }else{
                    this.postalAddress = false
                }
                this.addRegisterform.patchValue({
                    id:this.userId,
                    category: data.user.category,
                    name_of_director: data.user.name_of_director,
                    entity_name: data.user.entity_name,
                    dateofincorporation: new Date(data.user.dateofincorporation),
                    dateofregis: new Date(data.user.dateofregis),
                    registrationnumber: data.user.registrationnumber,
                    taxexmpt: data.user.taxexmpt,
                    taxexmptconfno: data.user.taxexmptconfno,
                    dateofissue: new Date(data.user.dateofissue),
                    email: data.user.email,
                    phone: data.user.phone,
                    physicaladdr1: data.user.physicaladdr1,
                    city: data.user.city,
                    state1: data.user.postal_state,
                    zip: data.user.zip,
                    postal_addr2: data.user.postal_addr2,
                    postal_city: data.user.postal_city,
                    postal_state: data.user.postal_state,
                    postal_zip: data.user.postal_zip,
                    activeStatus: data.user.activeStatus.toString(),

                }
                );
                console.log('==========================', data.user)
                //this.handleChangetax(data.user.taxexmpt);
                //this.addRegisterform.updateValueAndValidity();
                //console.log('=================json========='  , JSON.stringify(this.addRegisterform));
            }
        )
    }

    onActivate(event) {
        window.scroll(0,0);
      }

}











