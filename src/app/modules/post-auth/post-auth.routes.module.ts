import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PostAuthComponent } from './post-auth.component';
import { AuthGuard, EntityGuard } from '../../services/guards/auth-guard.service';
import { Error404Component } from '../../components/error-404/error-404.component';
import { NoAccessComponent } from '../../components/no-access/no-access-component';
import { DashboardComponent } from './dashboard/dashboard.component';

const routerConfig: Routes = [
  { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
  {
    path: '',
    component: PostAuthComponent,
    children: [
      { path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuard], pathMatch: 'full' },
      { path: 'proposal-add', loadChildren: () => import('./proposal/proposal.module').then(m => m.ProposalModule) },
      { path: 'proposal-edit/:id', loadChildren: () => import('./proposal/proposal.module').then(m => m.ProposalModule) },
      { path: 'proposal-list', loadChildren: () => import('./proposal-list/proposal-list.module').then(m => m.ProposalListModule) },
      { path: 'proposal-view/:id', loadChildren: () => import('./proposal-view/proposal-view.module').then(m => m.ProposalViewModule) },
      { path: 'news-add', loadChildren: () => import('./news/news.module').then(m => m.NewsModule) },
      { path: 'news-edit/:id', loadChildren: () => import('./news/news.module').then(m => m.NewsModule) },
      { path: 'news-list', loadChildren: () => import('./news-list/news-list.module').then(m => m.NewsListModule) },
      { path: 'entity-list', loadChildren: () => import('./entity-search/entity-search.module').then(m => m.EntitySearchModule) },
      { path: 'entity-add', loadChildren: () => import('./registration/registration.module').then(m => m.RegistrationModule) },
      { path: 'entity-view', loadChildren: () => import('./user-edit/user-edit.module').then(m => m.UserEditModule) },
      { path: 'entity-view/:id/:param', loadChildren: () => import('./user-edit/user-edit.module').then(m => m.UserEditModule) },
      
      { path: 'quaterly-reports/:id', loadChildren: () => import('./quaterly-reports/quaterly-reports.module').then(m => m.QuaterlyReportsModule) },
      { path: 'audittrail', loadChildren: () => import('./audittrail/audittrail.module').then(m => m.AuditTrailModule) },
      { path: 'budget', loadChildren: () => import('./budget/budget.module').then(m => m.BudgetModule) },

      { path: 'member-edit/:id', loadChildren: () => import('./add-entity-member/add-entity-member.module').then(m => m.AddEntityMemberModule) },
      { path: 'add-entity-member', loadChildren: () => import('./add-entity-member/add-entity-member.module').then(m => m.AddEntityMemberModule) },
      { path: 'member-list', loadChildren: () => import('./member-list/member-list.module').then(m => m.MemberListModule) },
     
      { path: '404', component: Error404Component },
      { path: 'no-access', component: NoAccessComponent },

      { path: 'admin-user', loadChildren: () => import('./user-admin/user-admin.module').then(m => m.UserAdminModule) },
      { path: 'user', loadChildren: () => import('./user-master/user-master.module').then(m => m.UserMasterModule) },
      { path: 'role', loadChildren: () => import('./role-master/role-master.module').then(m => m.RoleMasterModule) },

      { path: 'project-add', loadChildren: () => import('./project/project.module').then(m => m.ProjectModule) },
      { path: 'project-edit/:id', loadChildren: () => import('./project/project.module').then(m => m.ProjectModule) },
      { path: 'project-list', loadChildren: () => import('./project-list/project-list.module').then(m => m.ProjectListModule) },
      { path: 'edit-profile', loadChildren: () => import('./edit-profile/edit-profile.module').then(m => m.EditProfileModule) },

      { path: 'cms-add', loadChildren: () => import('./cms/cms.module').then(m => m.CmsModule) },
      { path: 'cms-edit/:id', loadChildren: () => import('./cms/cms.module').then(m => m.CmsModule) },
      { path: 'cms-list', loadChildren: () => import('./cms-list/cms-list.module').then(m => m.CmsListModule) },

    ],
    canActivate: [AuthGuard]
  },
  { path: '**', redirectTo: '404', pathMatch: 'full' }
];

@NgModule({
  imports: [
    RouterModule.forChild(routerConfig)
  ],
  exports: [RouterModule],
  declarations: [],
  providers: [AuthGuard, EntityGuard]
})
export class PostAuthRouteModule { }
