import { NgModule } from '@angular/core';
import { NewsRouteModule } from './news.routes.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { AuthService } from '../../../services/apis/auth.service';
import { NewsComponent } from './news.component';
import { MatSortModule } from '@angular/material';
import { UserMasterService } from 'src/app/services/apis/usermaster';
import { CKEditorModule } from 'ngx-ckeditor';
import { NewsService } from '../../../services/apis/news.service';


@NgModule({
  imports: [
    NewsRouteModule,
    SharedModule,
    MatSortModule,
    CKEditorModule

  ],
  declarations: [
    NewsComponent,
  ],
  entryComponents: [
    NewsComponent,
  ],
  providers: [AuthService, UserMasterService,NewsService ]
})
export class NewsModule { }
