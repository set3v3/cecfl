import { Component, OnInit, ViewChild } from '@angular/core';
import { NewsService } from '../../../services/apis/news.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastProvider } from 'src/app/shared/modules/toast/toast.provider';
import { UserStorageProvider } from '../../../services/storage/user-storage.service';
import { AppConst } from 'src/app/app.constants';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.scss']
})
export class NewsComponent implements OnInit {
  @ViewChild('fileInput', { static: false} ) fileInput;
  public editorValue: string = '';
  addNewsform: FormGroup;
  msg;
  successMsg: boolean = false;
    errorMsg: boolean = false;
    DocMessage: string;
    userid: number;
  newslistid: number;
  filevaidation: boolean;
  public f1submitted: boolean = false;
  filesToUpload: Array<File> = [];
  imageUrl: Array<any> = [];
  upFiles: any = [];
  allfiles: any = [];
  constructor(
    private newsApi: NewsService,
    public router: Router,
    public route: ActivatedRoute,
    private userStorage: UserStorageProvider,
    private toast: ToastProvider,
  ) {
    this.userid = 1;
    this.newsformbuild();
    this.filevaidation = false;
    this.DocMessage = '';
    this.newslistid = 0;
  }



  ngOnInit() {
    this.route.params.subscribe(params => {
      if (params) {
        //console.log(params);
        if (params['id']) {
          //console.log(params['id']);
          this.newslistid = +params['id'];
          if (this.newslistid > 0) {
            this.view();
          }
        }
      }
    });

  }
  get f1() { return this.addNewsform.controls; };
  newsformbuild() {
    this.addNewsform = new FormGroup({

      NewsTitle: new FormControl('', [
        Validators.required,
      ]),

      NewsTitleSpanish: new FormControl('', [
        Validators.required,
      ]),

      NewsBody: new FormControl('', [
        Validators.required,
      ]),
      NewsBodySpanish: new FormControl('', [
        Validators.required,
      ]),

     

      Status: new FormControl('', [
        Validators.required,
      ]),

      UpdatedBy: new FormControl(this.userStorage.get().user.id ? this.userStorage.get().user.id : '', [
      ]),
      CreatedBy: new FormControl(this.userStorage.get().user.id ? this.userStorage.get().user.id : '', [
      ]),


    });
  }

  getImgUrl(imagename) {
    if (imagename && imagename !== '') {
      return AppConst.API_BASE_URL + '/' + imagename;
    } else {
      return 'assets/images/icon/patients.png';
    }

  }

  deleteImge(index,fileInfo) {
    console.log(fileInfo)
    // const files: Array<File> = this.filesToUpload;
    // const index = this.imageUrl.findIndex(x => x === data);
    this.imageUrl.splice(index, 1);
    this.filesToUpload.splice(index, 1);
    this.upFiles.splice(index, 1);
    this.allfiles.splice(index, 1);
    console.log(this.allfiles)
    console.log(this.upFiles)
    this.newsApi.delFile(fileInfo).subscribe((response: any) => {
      // console.log(response);
      if (response.success) {
       // this.view();
        this.toast.success('Removed Successfully');
      }
    }, (errorData: any) => {});
  }
  removefile(file: any,) {
    const index = this.upFiles.indexOf(file);
    this.upFiles.splice(index, 1);
    this.allfiles.splice(index, 1);


  }
  onImagechange(event: any) {
    this.filevaidation = false;
    const files = event.target.files;
    const nameObj = event.target.name;
    //console.log(event)
    // tslint:disable-next-line:no-angle-bracket-type-assertion
    // this.filesToUpload = <Array<File>>event.target.files;
    // const files: Array<File> = this.filesToUpload;
    //this.filesToUpload = [];
    // for (let i = 0; i <= event.target.files.length; i++) {
    //   const selectedFile = event.target.files[i];
      
    // }
    this.filesToUpload.push(event.target.files);
    if (files) {
      for (let i = 0; i < files.length; i++) {
        const fileObj = {
          name: '',
          type: '',
          url: ''
        }
        this.allfiles.push(files[i]);
        fileObj.name = files[i].name;
        fileObj.type = files[i].type;

        const reader = new FileReader();
        reader.onload = (filedata) => {
          fileObj.url = reader.result + '';
          this.upFiles.push(fileObj);
        }
        reader.readAsDataURL(files[i]);
      }
    }
    console.log(this.upFiles)
    event.srcElement.value = null;
    // tslint:disable-next-line:prefer-for-of
    // this.DocMessage = '';
    // const arry = [];
    // this.imageUrl = [];
    
      // tslint:disable-next-line:prefer-for-of
      // for (let i = 0; i < this.filesToUpload.length; i++) {

      //   const type = this.filesToUpload[i].type.
      ('/')[1];
      //   const size = this.filesToUpload[i].size / 1024;
      //   if (type ) {
      //     if (size < 5120) {
      //       const img = new Image();
      //       img.src = window.URL.createObjectURL(this.filesToUpload[i]);
      //       // console.log('img', files[i].size / 1024)
      //       const reader = new FileReader();
      //       img.onload = () => {
      //         window.URL.revokeObjectURL(img.src);
      //         reader.onload = (e: any) => {
      //           this.imageUrl.push(e.target.result);
      //           arry.push(this.filesToUpload[i].name);
      //           // console.log(e.target.result);
      //         };
      //         reader.readAsDataURL(this.filesToUpload[i]);
      //       };
      //     } else {

      //       this.toast.error('Upload Image With Max Size 5 Mb');
      //     }
      //   } else {
      //     this.toast.error('Uplaod Image Only');

      //   }
      // }
      // // console.log(this.imageUrl);
      // this.DocMessage = arry.join(', ');
    


  }
addNews(value) {
  console.log(value)
    this.f1submitted = true;
    //const files: Array<File> = this.filesToUpload;
    const files: Array<File> = this.allfiles;
    if (this.newslistid > 0) {
      const formData = new FormData();
      // tslint:disable-next-line:prefer-for-of
      for (let i = 0; i < files.length; i++) {
        
        formData.append("esNewsDocUrl[]", files[i], files[i]['name']);;
      }
      formData.append('NewsTitle', value.NewsTitle);
      formData.append('NewsTitleSpanish', value.NewsTitleSpanish);
      formData.append('NewsBody', value.NewsBody);
      formData.append('NewsBodySpanish', value.NewsBodySpanish);
      formData.append('Status', value.Status);
      formData.append('UpdatedBy', value.UpdatedBy);
      this.newsApi.editnews(formData, this.newslistid).subscribe(
        (res: any) => {
          console.log('==' + res);
          //this.successMsg  = true ; 
          this.msg=res['message'];
          this.router.navigate(['post-auth/news-list']);
          this.toast.success( this.msg)
        },
        (err) => {
          this.f1submitted = false;
        });
    } else {
      //console.log(this.addNewsform)
      if (this.addNewsform.invalid) { this.toast.error('Please enter the required fields !!'); return; }
      //console.log( value)
      const formData = new FormData();
      // tslint:disable-next-line:prefer-for-of
     
        //console.log('loop=>',files);
        if(files.length > 0){
          for(let i =0; i < files.length; i++){
            //console.log(files[i]);
            formData.append("esNewsDocUrl[]", files[i], files[i]['name']);
          }
        }
      formData.append('NewsTitle', value.NewsTitle);
      formData.append('NewsTitleSpanish', value.NewsTitleSpanish);
      formData.append('NewsBody', value.NewsBody);
      formData.append('NewsBodySpanish', value.NewsBodySpanish);
      formData.append('Status', value.Status);
      formData.append('CreatedBy', value.CreatedBy);
      formData.append('UpdatedBy', value.UpdatedBy);
      
      this.newsApi.addnews(formData).subscribe(
        (res: any) => {
          //this.successMsg  = true ; 
          this.msg=res['message'];
          this.router.navigate(['post-auth/news-list']);
          this.toast.success( this.msg)
          this.addNewsform.reset();
          setTimeout(() =>{
            this.errorMsg  = false;
           
         }, 3000); 
         

       //this.router.navigate(['post-auth/news-list']);
        },
        (err) => {
          this.f1submitted = false;
          this.msg=err['message'];
          this.toast.error( this.msg)
        });
    }



  }



  view() {

    //console.log(this.id);
    this.newsApi.view(this.newslistid).subscribe(
      data => {
        console.log(data);
        
        if(this.newslistid){
          this.imageUrl  = data.news_documents
          this.addNewsform.setValue({
            NewsTitle: data.NewsTitle,
            NewsTitleSpanish: data.NewsTitleSpanish,
            NewsBody: data.NewsBody,
            NewsBodySpanish: data.NewsBodySpanish,
            Status: data.Status,
            UpdatedBy: this.userStorage.get().user.id ? this.userStorage.get().user.id : '',
            CreatedBy:''
            
          }
          );
        }else{
          this.imageUrl  = data.news_documents
          this.addNewsform.setValue({
            NewsTitle: data.NewsTitle,
            NewsTitleSpanish: data.NewsTitleSpanish,
            NewsBody: data.NewsBody,
            NewsBodySpanish: data.NewsBodySpanish,
            Status: data.Status,
            CreatedBy: this.userStorage.get().user.id ? this.userStorage.get().user.id : '',
            UpdatedBy:''
          }
          );
        }
        
        this.addNewsform.updateValueAndValidity();
        console.log(this.addNewsform);
      }
    )

  }
  
  onActivate(event) {
    window.scroll(0,0);
  }

 
}     
