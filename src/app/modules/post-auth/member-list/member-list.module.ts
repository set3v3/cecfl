import { NgModule } from '@angular/core';
import { SharedModule } from 'src/app/shared/shared.module';
import { AuthService } from '../../../services/apis/auth.service';
import { MatSortModule } from '@angular/material';
import { UserMasterService } from 'src/app/services/apis/usermaster';
import { RegistrationService } from '../../../services/apis/registration.service';
import { MemberListComponent, MemberDeleteModalComponent} from './member-list.component';
import { MemberListRoutesModule } from './member-list-routes.module';
import { UserEditService } from 'src/app/services/apis/useredit.service';


@NgModule({
  imports: [
    MemberListRoutesModule,
    SharedModule,
    MatSortModule,
   

  ],
  declarations: [
    MemberListComponent,
    MemberDeleteModalComponent
    
  ],
  entryComponents: [
    MemberListComponent,
    MemberDeleteModalComponent
   
  ],
  providers: [AuthService, UserMasterService,RegistrationService,UserEditService]
})
export class MemberListModule { }
