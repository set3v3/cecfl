import { Component, OnInit, Inject } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { ToastProvider } from 'src/app/shared/modules/toast/toast.provider';
import { NgxUiLoaderService,SPINNER } from 'ngx-ui-loader';
import { CommonService } from 'src/app/services/apis/common.service';
import { UserMasterService } from 'src/app/services/apis/usermaster';
import { UserStorageProvider } from 'src/app/services/storage/user-storage.service';


@Component({
  selector: 'app-member-list',
  templateUrl: './member-list.component.html',
  styleUrls: ['./member-list.component.scss']
})
export class MemberListComponent implements OnInit {
  entityData:any= [];
  user_id: any;
  length;
  msg;
  p: number = 1;
  limit:number =10
  itemsPerPage:number = 10;
  EntityParam: any = {};
  spinnerType = SPINNER.rectangleBounce;
  language;
  searchString;
  
  constructor(
    private memberApi: UserMasterService,
    public router: Router,
	public dialog: MatDialog,
	private toast: ToastProvider,
	private ngxService:NgxUiLoaderService,
    private common :CommonService,
    private userStorage: UserStorageProvider

  ) { 
    this.entityData = [];
    this.user_id = this.userStorage.get().user.id
  }

  ngOnInit() {
    this.common.currLang.subscribe(
			res=>{
				this.language = res;
			}
		)
		this.EntityParam.page = this.p;
		this.getEntityList();
  }
  getEntityList = () => {
		this.ngxService.start();
		this.memberApi.memberList(this.user_id).subscribe((res: any) => {
			this.ngxService.stop();
      this.entityData = res.members;
      console.log(this.entityData)
			this.length = res['total'];

		}, (err) => {
			this.ngxService.stop()
		});
	}
	
	deleteMember(id) {
		const dialogRef = this.dialog.open(MemberDeleteModalComponent, {
			panelClass: 'dialog-xs',
			data:{status:'member-delete'},
			width: '600px'
		});
		dialogRef.afterClosed().subscribe(response =>{
			//console.log(response)
			if(response.delStatus == 'deleted'){
			  //console.log('hi')
				this.memberApi.deleteMember(id).subscribe((res: any) => {
					//console.log(res);
					this.msg = res['message']
					this.toast.success( this.msg)
					this.getEntityList();
				}, (err) => {
					this.msg = err['message']
					this.toast.error( this.msg)
				});
			}
	  
		})
	}
  
  paginationFeed(page){
    this.p = page;
    let start = (page - 1) * this.itemsPerPage;
    this.EntityParam.page = page;
    this.getEntityList()
    
}
memberSearch(){
  this.EntityParam.search = this.searchString;
  this.getEntityList();
}

}

@Component({
	selector: 'app-reject-modal',
	templateUrl: '../proposal/reject-modal/reject-modal.component.html',
  })
export class MemberDeleteModalComponent {
	public showLoader: boolean;
	constructor(
		public dialogRef: MatDialogRef<MemberDeleteModalComponent>,
		@Inject(MAT_DIALOG_DATA) public data:any
	) { }
   
	deleteMember(){
		this.dialogRef.close({delStatus:'deleted'});
	}
	rejectproposal(){}

	deleteNews(){}
  
	onCloseClick(){
		this.dialogRef.close();
	}
  }
  
