
import { AppConst } from 'src/app/app.constants';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatTableDataSource, PageEvent, MatSort } from '@angular/material';
import { ConsoleProvider } from 'src/app/shared/modules/console/console.provider';
import { EditUserAdminComponent } from './edit-user-admin/edit-user-admin.component';
import { AddUserAdminComponent } from './add-user-admin/add-user-admin.component';
import { UserMasterService } from 'src/app/services/apis/usermaster';
import { ViewAdminUserComponent } from './view-user-admin/view-user-admin.component';
import { ADMINUserlistApiResponse, AdminUserListInfo, AdminUserReportExportApiResponse } from 'src/app/models/usermaster';
import { PermissionProvider } from 'src/app/services/permission/permission.service';
import * as _ from 'lodash';
import { NgxUiLoaderService,SPINNER } from 'ngx-ui-loader';

@Component({
  selector: 'app-user-admin',
  templateUrl: './user-admin.component.html',
  styleUrls: ['./user-admin.component.scss'],
  providers: []
})
export class UserAdminComponent implements OnInit {
  spinnerType = SPINNER.rectangleBounce;
  isLoadingResults: boolean;
  showMessage: string;
  userlisting: Array<any>;
  totalItem: number;
  page: number;
  pageindex: number;
  viewstatus: boolean;
  editstatus: boolean;
  createtatus: boolean;
  exportstatus: boolean;
  searchParam: any;
  fixVar: boolean;
  open: boolean;
  spin: boolean;
  direction: string;
  animationMode: string;
  bydefatseleteduserType: any;
  userType: Array<any>;
  displayedColumns = ['admin_first_name','admin_last_name', 'email' ,'admin_phone','roleName','activeStatus'];
  dataSource: MatTableDataSource<any>;
  //@ViewChild(MatSort) sort: MatSort;
  @ViewChild('MatSort', {static: true}) sort: MatSort; 
  constructor(
    private consoleProvider: ConsoleProvider,
    private UserMasterApi: UserMasterService,
    public dialog: MatDialog,
    private Permission: PermissionProvider,
    private ngxService:NgxUiLoaderService,
  ) {
    this.isLoadingResults = false;
    this.userlisting = [];
    this.showMessage = '';
    this.fixed = false;
    this.open = false;
    this.spin = false;
    this.direction = 'up';
    this.userType = AppConst.DATA_POTAL_USER_TYPE;
    this.animationMode = 'fling';
    this.bydefatseleteduserType = 0;
    this.editstatus = Permission.permission('admin-user', 'edit');
    this.viewstatus = Permission.permission('admin-user', 'view');
    this.createtatus = Permission.permission('admin-user', 'create');
    this.exportstatus = Permission.permission('admin-user', 'export');
    if (this.editstatus) {
      this.displayedColumns.push('edit');
    }
    if (this.viewstatus) {

      this.displayedColumns.push('view');
    }
    this.showMessage = '';
  }

  ngOnInit() {
    this.fetchUserListing(0);
  }
  /*
  * function use for pagenation
  * @param
  * @memberof UserAdminComponent
  */
  getNext(event: PageEvent) {
    this.consoleProvider.log('getNext event', event);
    this.page = event.pageSize;
    this.pageindex = event.pageIndex;
    this.fetchUserListing(event.pageIndex, event.pageSize);
  }
  /*
  * function use for search
  * @param
  * @memberof UserAdminComponent
  */
  applyFilter(filterValue: string) {
    console.log(filterValue);
    filterValue = filterValue.trim(); // Remove whitespace
    // this.userlisting = [];
    this.fetchUserListing(0, '', filterValue);
  }
  /*
  * function use add new admin user open modal
  * @param
  * @memberof UserAdminComponent
  */
  AddUsertype() {
    const dialogRef = this.dialog.open(AddUserAdminComponent, {
      panelClass: 'dialog-sm',
      disableClose: true
    });
    dialogRef.componentInstance.OnAddAdmin.subscribe((data: any) => {
      this.fetchUserListing(0);
    });
  }
  /*
  * function use edit  admin user open edit modal
  * @param
  * @memberof UserAdminComponent
  */
  editUsertype(object) {
    const dialogRef = this.dialog.open(EditUserAdminComponent, {
      panelClass: 'dialog-sm',
      disableClose: true,
      data: object
    });
    dialogRef.componentInstance.OnEditAdmin.subscribe((data: any) => {
      this.fetchUserListing(this.pageindex, this.page);
    });
  }
  /*
  * function use view  admin user open edit modal
  * @param
  * @memberof UserAdminComponent
  */
  viewUser(object) {
    const dialogRef = this.dialog.open(ViewAdminUserComponent, {
      panelClass: 'dialog-sm',
      disableClose: true,
      data: object,
      width: '400px'
    });


  }
  /*
  * function use ferch  admin user with paramiter usertype page ,pageindex
  * @param
  * @memberof UserAdminComponent
  */
  fetchUserListing(index?, limit?, searchTxt?) {
    //console.log('searchTxt=>',searchTxt);
    this.ngxService.start();
    this.showMessage = '';
    this.isLoadingResults = true;
    this.searchParam = {
      search: (searchTxt) ? searchTxt : '',
      page: (limit) ? limit : 10,
      pageIndex: (index) ? (index) : 0
    };
    this.UserMasterApi.getAdminUserList(this.searchParam).subscribe((res: ADMINUserlistApiResponse) => {
      this.ngxService.stop();
      this.isLoadingResults = false;
      //this.consoleProvider.log('getUserList', res);
      if (res.success) {
        this.userlisting = res.data;
        this.totalItem = res.totalCount;
        this.dataSource = new MatTableDataSource(this.userlisting);
        this.dataSource.sort = this.sort;
        // this.dataSource.paginator = this.paginator;
      }
      if (this.userlisting.length <= 0) {
        this.showMessage = 'No Record Found.';
      }

    }, (err) => {
      this.ngxService.stop();
      this.isLoadingResults = false;
      console.log('api_http-------errr', err);
    });
  }
  downloadExport() {
    this.isLoadingResults = true;
    const search = {
      search: (this.searchParam.userType) ? this.searchParam.userType : '',
    };
    this.UserMasterApi.downloadAdminUserExport(search).subscribe((res: AdminUserReportExportApiResponse) => {
      this.isLoadingResults = false;
      if (res.success) {
        window.location.href = res.data;
      }
    }, (err) => {
      this.isLoadingResults = false;
    });
  }
  get fixed(): boolean {
    return this.fixVar;
  }

  set fixed(fixed: boolean) {
    this.fixVar = fixed;
    if (this.fixVar) {
      this.open = true;
    }
  }

  public doAction(event: any) {
    // console.log(event);
  }


  UserTypechange(event) {

  }

}

