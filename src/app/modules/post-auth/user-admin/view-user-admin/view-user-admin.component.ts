import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ToastProvider } from 'src/app/shared/modules/toast/toast.provider';
import { ConsoleProvider } from 'src/app/shared/modules/console/console.provider';



@Component({
  selector: 'app-view-user-admin',
  templateUrl: './view-user-admin.component.html',
  styleUrls: ['./view-user-admin.component.scss'],
  providers: []
})
export class ViewAdminUserComponent implements OnInit {
  userInfo: any;
  showLoader: boolean;
  constructor(
    private toast: ToastProvider,
    private dialogRef: MatDialogRef<ViewAdminUserComponent>,
    private consoleProvider: ConsoleProvider,
    @Inject(MAT_DIALOG_DATA) public data: any

  ) {
    this.userInfo = this.data;
    this.showLoader = false;
    //this.consoleProvider.log('userInfo', this.userInfo);
  }
  ngOnInit() {
  }
}
