import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from 'src/app/services/guards/auth-guard.service';
import { UserAdminComponent } from './user-admin.component';

const routerConfig: Routes = [
  { path: '', component: UserAdminComponent, canActivate: [AuthGuard], pathMatch: 'full' },
];

@NgModule({
  imports: [
    RouterModule.forChild(routerConfig)
  ],
  exports: [RouterModule],
  declarations: [],
  providers: [AuthGuard]
})
export class UserAdminRouteModule { }
