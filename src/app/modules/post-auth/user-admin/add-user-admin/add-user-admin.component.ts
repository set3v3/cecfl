import { AppConst } from 'src/app/app.constants';
import { Component, OnInit, Inject, ViewChild, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA, MatSelectChange } from '@angular/material';
import { ToastProvider } from 'src/app/shared/modules/toast/toast.provider';
import { ConsoleProvider } from 'src/app/shared/modules/console/console.provider';
import { RoleMasterService } from 'src/app/services/apis/role.service';
import { equalvalidator } from 'src/app/directives/validators/equal-validator';
import { UserMasterService } from 'src/app/services/apis/usermaster';
import { UserStorageProvider } from 'src/app/services/storage/user-storage.service';


@Component({
  selector: 'app-add-user-admin',
  templateUrl: './add-user-admin.component.html',
  styleUrls: ['./add-user-admin.component.scss'],
  providers: []
})
export class AddUserAdminComponent implements OnInit {
  addAdminUserform: FormGroup;
  showLoader: boolean;
  RoleListing: Array<any>;
  StateList: Array<any>;
  Citylisting: Array<any>;
  statevalidation: boolean;
  @Output() OnAddAdmin = new EventEmitter<any>(true);

  constructor(
    private formBulder: FormBuilder,
    private toast: ToastProvider,
    private dialogRef: MatDialogRef<AddUserAdminComponent>,
    private consoleProvider: ConsoleProvider,
    private RoleApi: RoleMasterService,
    private UserMasterApi: UserMasterService,
    private userStorage: UserStorageProvider,
    @Inject(MAT_DIALOG_DATA) public data: any

  ) {
    this.RoleListing = [];
    this.StateList = [];
    this.statevalidation = false;
    this.Citylisting = [];
    this.fetchRoleListing();
    this.getStateList();
  }
  /*
    * function  used for buildform
    * @param
    * @memberof AddUserAdminComponent
    */
  ngOnInit() {
    /* tslint:disable */
    const emailRegx = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    /* tslint:enable */
    this.addAdminUserform = new FormGroup({
      roleId: new FormControl('', [
        Validators.required,
      ]),
      email: new FormControl('', [
        Validators.required,
        , Validators.pattern(emailRegx)
      ]),
      admin_first_name: new FormControl('', [
        Validators.required,
      ]),
      admin_last_name: new FormControl('', [
        Validators.required,
      ]),
      admin_phone: new FormControl('', [
        Validators.required,
      ]),
      admin_address: new FormControl('', [
        Validators.required,
      ]),
      admin_state: new FormControl('', [
        Validators.required,
      ]),
      admin_city: new FormControl('', [
        Validators.required,
      ]),
      password: new FormControl('', [
        Validators.required
      ]),
      password_confirmation: new FormControl('', [
        Validators.required, equalvalidator('password')
      ]),
      activeStatus: new FormControl('1', [
        Validators.required,
      ]),
      created_by: new FormControl(this.userStorage.get().user.id, []),
    });
  }
  getStateList() {
    this.UserMasterApi.getState().subscribe((res: any) => {
      if (res.success) {
        this.StateList = res.data;
      }
    }, (err) => {
    });
  }
  StateChange(event) {
    const index = this.StateList.findIndex(x => x.id === event.value);
    if (index > -1) {
      this.Citylisting = this.StateList[index].cities;
    }
    console.log(this.Citylisting);
  }
  /*
  * function  used for call api for fetching role
  * @param
  * @memberof AddUserAdminComponent
  */
  fetchRoleListing() {
    this.showLoader = true;
    this.RoleApi.rolelisting().subscribe((response: any) => {
      // console.log(response);
      this.showLoader = false;
      if (response.resCode === 200) {
        this.RoleListing = response.data;
      }
    }, (errorData: any) => {
      this.showLoader = false;
    });
  }
  /*
  * function  used for call api for fetching role
  * @param
  * @memberof AddUserAdminComponent
  */
  addAdminUser(userInfo) {
    // console.log(userInfo);
    if (this.addAdminUserform.valid) {
      this.showLoader = true;
      this.UserMasterApi.AddAdminUser(userInfo).subscribe((response: any) => {
        this.showLoader = false;
        // console.log(response);
        if (response.success) {
          this.OnAddAdmin.emit(response);
          this.dialogRef.close();
          this.toast.success('User Added Successfully');
        }
      }, (errorData: any) => {
        this.showLoader = false;
      });
    }

  }
}
