import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { AuthService } from '../../../services/apis/auth.service';
import { MatSortModule } from '@angular/material';
import { UserMasterService } from 'src/app/services/apis/usermaster';
import { NgxPaginationModule } from 'ngx-pagination';
import { CmsListComponent,CmsModalComponent } from './cms-list.component';
import { CmsListRouteModule } from './cms-list-route.module';
import { CmsService } from 'src/app/services/apis/cms.service';
@NgModule({
  imports: [
    CommonModule,
    CmsListRouteModule,
    SharedModule,
    MatSortModule,
    NgxPaginationModule
  ],
  declarations: [
    CmsListComponent,
    CmsModalComponent

  ],
  entryComponents: [
    CmsListComponent,
    CmsModalComponent

  ],
  providers: [AuthService, UserMasterService, CmsService]
})
export class CmsListModule { }
