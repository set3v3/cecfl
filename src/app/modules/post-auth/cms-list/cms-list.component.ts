import { Component, OnInit, Inject } from '@angular/core';
import { ProjectService } from '../../../services/apis/project.service';
import { Router, ActivatedRoute } from '@angular/router';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { ToastProvider } from 'src/app/shared/modules/toast/toast.provider';
import { NgxUiLoaderService,SPINNER } from 'ngx-ui-loader';
import { CommonService } from 'src/app/services/apis/common.service';
import { CmsService } from 'src/app/services/apis/cms.service';

@Component({
  selector: 'app-cms-list',
  templateUrl: './cms-list.component.html',
  styleUrls: ['./cms-list.component.scss']
})
export class CmsListComponent implements OnInit {

    cmsData: Array<any>;
	length;
    msg;
    p: number = 1;
    limit:number =10
	itemsPerPage:number = 10;
	cmsParam: any = {};
	spinnerType = SPINNER.rectangleBounce;
	language;
	searchString;
	constructor(
		private cmsApi: CmsService,
		public router: Router,
		public dialog: MatDialog,
		private toast: ToastProvider,
		private ngxService:NgxUiLoaderService,
		private common :CommonService
	) {
		this.cmsData = [];
	}

	ngOnInit() {
		this.common.currLang.subscribe(
			res=>{
				this.language = res;
			}
		)
		this.cmsParam.page = this.p;
		this.getCmsList();
	}

	getCmsList(){
		this.ngxService.start();
		this.cmsApi.cmsList(this.cmsParam).subscribe((res: any) => {
			this.ngxService.stop();
			this.cmsData = res.data;
			this.length = res['total'];

		}, (err) => {
			this.ngxService.stop()
		});
	}
	paginationFeed(page){
        this.p = page;
        let start = (page - 1) * this.itemsPerPage;
        this.cmsParam.page = page;
        this.getCmsList()
        
	}

	projectSearch(){
		this.cmsParam.search = this.searchString;
		this.getCmsList();
    }
    onClickDeleteCms(event){
		const dialogRef = this.dialog.open(CmsModalComponent, {
			panelClass: 'dialog-xs',
			data:{},
			width: '600px'
		});
		dialogRef.afterClosed().subscribe(response =>{
		console.log(response)
			if(response.status == 'delete'){
				this.ngxService.start()
				this.cmsApi.deleteCms(event).subscribe(
					res=>{
						if(res.success){
							this.ngxService.stop()
							this.getCmsList()
							this.msg = res['message'];
							this.toast.success(this.msg) 
						}
					},
					error=>{
						this.ngxService.stop()
						this.msg = error['message'];
						this.toast.error(this.msg) 
					}
				)
			}
		})
    }
}

@Component({
	selector: 'app-cms-modal',
	templateUrl: '../cms-modal/cms-modal.component.html',
  })
  
  export class CmsModalComponent {
	constructor(
		public dialogRef: MatDialogRef<CmsModalComponent>,
		@Inject(MAT_DIALOG_DATA) public data:any
	) { }
   
	deleteCms(){
		this.dialogRef.close({status:'delete'});
	}
  
	onCloseClick(){
		this.dialogRef.close();
	}
   
  }
