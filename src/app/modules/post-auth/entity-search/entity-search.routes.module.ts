import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from 'src/app/services/guards/auth-guard.service';
import { EntitySearchComponent } from './entity-search.component';
import { NgxPaginationModule } from 'ngx-pagination';

const routerConfig: Routes = [
  { path: '', component: EntitySearchComponent, canActivate: [AuthGuard], pathMatch: 'full' },
];
@NgModule({
  imports: [
    RouterModule.forChild(routerConfig),
    NgxPaginationModule
    
  ],
  exports: [RouterModule],
  declarations: [],
  providers: [AuthGuard]
})
export class EntitySearchRouteModule { }
