import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EntitySearchRouteModule } from "./entity-search.routes.module";
import { SharedModule } from 'src/app/shared/shared.module';
import { AuthService } from '../../../services/apis/auth.service';
import { MatSortModule } from '@angular/material';
import { UserMasterService } from 'src/app/services/apis/usermaster';
import { EntitysearchService } from '../../../services/apis/entitysearch.service';
import { EntitySearchComponent } from './entity-search.component';
import { NgxPaginationModule } from 'ngx-pagination';

@NgModule({
  imports: [
    EntitySearchRouteModule,
    SharedModule,
    MatSortModule,
    NgxPaginationModule
    

  ],
  declarations: [
    EntitySearchComponent,
  ],
  entryComponents: [
    EntitySearchComponent,
  ],
  providers: [AuthService, UserMasterService, EntitysearchService]
})
export class EntitySearchModule { }





