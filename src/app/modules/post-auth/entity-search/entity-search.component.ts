import { Component, OnInit } from '@angular/core';
import { EntitysearchService } from '../../../services/apis/entitysearch.service';
import { Router } from '@angular/router';
import { ToastProvider } from 'src/app/shared/modules/toast/toast.provider';
import { NgxUiLoaderService,SPINNER } from 'ngx-ui-loader';
import { CommonService } from 'src/app/services/apis/common.service';
import { ExcelService } from 'src/app/services/apis/excel.service';
import { UserStorageProvider } from 'src/app/services/storage/user-storage.service';

@Component({
	selector: 'app-entity-search',
	templateUrl: './entity-search.component.html',
	styleUrls: ['./entity-search.component.scss']

})



export class EntitySearchComponent implements OnInit {
	userlist: any;
	searchString: string;
	length;
	itemsPerPage:number = 10;
	p: number = 1;
	entityParam: any = {};
	spinnerType = SPINNER.rectangleBounce;
	language;
	excelData:any =[];
	loggedUserType: any;
	constructor(
		private entitysearchApi: EntitysearchService,
		public router: Router,
		private toast: ToastProvider,
		private ngxService:NgxUiLoaderService,
		private common:CommonService,
		private excelService:ExcelService,
		private userStorage: UserStorageProvider
	) {
		this.userlist = [];
		this.searchString = '';
		this.loggedUserType = this.userStorage.get().user.user_type;
	}

	ngOnInit() {
		this.common.currLang.subscribe(
			res=>{
				this.language =res
			}
		)
		this.getuserlist();

	}

	getuserlist = () => {
		this.entityParam.page = this.p;
		this.entityParam.language = this.language
		this.entityParam.searchString = this.searchString
		this.ngxService.start();
		this.entitysearchApi.userlist(this.entityParam).subscribe((res: any) => {
			//console.log(res);
			this.ngxService.stop()
			this.userlist = res.data;
			this.length =res.totalCount;
		}, (err) => {
			this.ngxService.stop()
		});
	}

	paginationFeed(page){
        this.p = page;
        let start = (page - 1) * this.itemsPerPage;
        this.entityParam.page = page;
        this.getuserlist();
        
	}

	exportAsXLSX() {
        this.entityParam.language = this.language;
        this.entitysearchApi.userlist(this.entityParam).subscribe(
            res => {
                this.excelData = res.excelData
                this.excelService.exportAsExcelFile(this.excelData, 'entityList');
            }
        )
    }
	

}


