import { Component, OnInit, ViewChild, TemplateRef, ElementRef, Inject } from '@angular/core';
import { FormBuilder, Validators, ControlContainer, FormGroup, FormControl, AsyncValidatorFn, AbstractControl, ValidationErrors, FormArray } from '@angular/forms';
import { AuthService } from 'src/app/services/apis/auth.service';
import { ProposalService } from 'src/app/services/apis/proposal.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastProvider } from 'src/app/shared/modules/toast/toast.provider';
import { ConsoleProvider } from 'src/app/shared/modules/console/console.provider';
import { UserMasterService } from 'src/app/services/apis/usermaster';
import { UserStorageProvider } from 'src/app/services/storage/user-storage.service';
import { requireCheckboxesToBeCheckedValidator } from "src/app/directives/validators/require-checkboxes-to-be-checked.validator";
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

import { AppConst } from 'src/app/app.constants';
import { DatePipe } from '@angular/common';
import { NgxUiLoaderService, SPINNER } from 'ngx-ui-loader';
import { PopupModalsComponent } from '../proposal/popup-modals/popup-modals.component';
import { AttachPopupComponent } from '../proposal/attach-popup/attach-popup.component';
import { MoreInfoComponent } from '../proposal/more-info/more-info.component';
import { CommonService } from 'src/app/services/apis/common.service';

declare var $: any;
@Component({
    selector: 'app-proposal-view',
    templateUrl: './proposal-view.component.html',
    styleUrls: ['./proposal-view.component.scss']
})
export class ProposalViewComponent implements OnInit {

    spinnerType = SPINNER.rectangleBounce;
    public showLoader: boolean;
    budgetBreakdown:any = []
    adminExpanceId:any = []
    directExpanceId:any = []
    administrativeExpenses: any;
    directExpenses: any;
    public tightBudgetStatus: boolean = false;
    public budgetPrevStatus: boolean = true;
    public approveGrantStatus: boolean = false;
    Deptlisting = [{
        key: 'puerto_rico_state_department',
        value: 'DEPARTAMENTO DE ESTADO DE PUERTO RICO'
    },
    {
        key: 'internal_revenue_services',
        value: 'INTERNAL REVENUE SERVICES (IRS)'
    },
    {
        key: 'puerto_rico_state_insurance_fund_corporation',
        value: 'CORPORACIÓN DEL FONDO DEL SEGURO DEL ESTADO DE PUERTO RICO'
    },
    {
        key: 'municipal_income_collection_center',
        value: 'CENTRO DE RECAUDACIONES DE INGRESOS MUNICIPALES'
    },
    {
        key: 'department_of_finance',
        value: 'DEPARTAMENTO DE HACIENDA'
    },
    {
        key: 'department_of_labor_and_human_resources',
        value: 'DEPARTAMENTO DEL TRABAJO Y RECURSOS HUMANOS'
    },
    {
        key: 'health_department',
        value: 'DEPARTAMENTO DE SALUD'
    },
    {
        key: 'administration_of_regulations_and_permits',
        value: 'ADMINISTRACIÓN DE REGLAMENTOS Y PERMISOS'
    },
    {
        key: 'puerto_rico_fire_fighter_body',
        value: 'CUERPO DE BOMBEROS DE PUERTO RICO'
    },
    {
        key: 'custody_agency',
        value: 'AGENCIA CUSTODIO'
    },
    {
        key: 'additional_documents',
        value: 'DOCUMENTOS ADICIONALES'
    },
    {
        key: 'seguro_social',
        value: 'SEGURO SOCIAL'
    },
    {
        key: 'administration_for_child_support',
        value: 'ADMINISTRACION PARA EL SUSTENTO DE MENORES (ASUME)'
    },
    {
        key: 'puerto_rico_police',
        value: 'POLICIA DE PUERTO RICO'
    },
    {
        key: 'negotiated_conventions_of_san_jun',
        value: 'NEGOCIADO DE CONVENCIONES DE SAN JUN, PR'
    },
    {
        key: 'higher_education_council',
        value: 'CONSEJO SUPERIOR DE EDUCACION'
    },
    {
        key: 'osfl',
        value: 'OSFL'
    }
    ]

    Evidencelist = [
    {
      key: 'EVIDENCE_OF_MATCHING_SPICE'
    },
    {
      key: 'FUNDING_SOURCES'
    },
    {
      key: 'COLLABORATIVE_AGREEMENT'
    },
    {
      key: 'CERTIFICATION_OF_MATCHING_FUNDS'
    },
    {
      key: 'OTHER_ANNEXES'
    },
    {
      key: 'EVIDENCE_OF_MATCHING_CASH_FUNDS'
    }
  ]

    //@ViewChild('fileInput', {static: false}) fileInput;
    startDate = new Date(1990, 0, 1);
    minDate = new Date(2000, 0, 1);
    maxDate = new Date();
    searchParam: any;
    selectedDepartment: any;
    Projectlisting: Array<any>;
    Citylisting: Array<any>;
    Pareotypelisting:any =[] ;
    Pareocategorylisting:any[];
    adminUserlisting;
    memberlisting: Array < any > ;
    loggedUserType: any;
    loggedUserRole: any;
    fileBaseUrl: any;
    apiBaseUrl: any;
    today: any;
    pareoCalculation: any;


    public proposal: any;
    urls: Array<any>;

    //upFiles: any = [];
    //allfiles: any = [];

    upFiles: any = {
        necessity_statement_file: [],
        work_plan_file: [],
        property_inventory_file: [],
        budget_requirement_file: [],

        puerto_rico_state_department: [],
        internal_revenue_services: [],
        puerto_rico_state_insurance_fund_corporation: [],
        municipal_income_collection_center: [],
        department_of_finance: [],
        department_of_labor_and_human_resources: [],
        health_department: [],
        administration_of_regulations_and_permits: [],
        puerto_rico_fire_fighter_body: [],
        custody_agency: [],
        additional_documents: [],
        seguro_social: [],
        administration_for_child_support: [],
        puerto_rico_police: [],
        negotiated_conventions_of_san_jun: [],
        higher_education_council: [],
        osfl: [],
    };
    allfiles: any = {
        necessity_statement_file: [],
        work_plan_file: [],
        property_inventory_file: [],
        budget_requirement_file: [],

        puerto_rico_state_department: [],
        internal_revenue_services: [],
        puerto_rico_state_insurance_fund_corporation: [],
        municipal_income_collection_center: [],
        department_of_finance: [],
        department_of_labor_and_human_resources: [],
        health_department: [],
        administration_of_regulations_and_permits: [],
        puerto_rico_fire_fighter_body: [],
        custody_agency: [],
        additional_documents: [],
        seguro_social: [],
        administration_for_child_support: [],
        puerto_rico_police: [],
        negotiated_conventions_of_san_jun: [],
        higher_education_council: [],
        osfl: [],
    };

    public f1submitted: boolean = false;
    public f2submitted: boolean = false;
    public f3submitted: boolean = false;
    public f4submitted: boolean = false;
    public f5submitted: boolean = false;
    public f6submitted: boolean = false;
    public f7submitted: boolean = false;
    public fActionsubmitted: boolean = false;

    public agreeChk: boolean = false;

    public show: any;
    public show1: boolean = true;
    public show2: boolean = false;
    public show3: boolean = false;
    public showProp: boolean = false;
    public showBudget: boolean = false;
    public showLegal: boolean = false;
    public showReq: boolean = false;
    public showEvidence: boolean = false;
    public showSubmit: boolean = false;

    public active: any;
    public active1: boolean = true;
    public active2: boolean = false;
    public active3: boolean = false;
    public activeProp: boolean = false;
    public activeBudget: boolean = false;
    public activeLegal: boolean = false;
    public activeReq: boolean = false;
    public activeEvidence: boolean = false;
    public activeSubmit: boolean = false;

    public proposalForm_1: FormGroup;
    public proposalForm_2: FormGroup;
    public proposalForm_3: FormGroup;
    public proposalFormProp: FormGroup;
    public proposalFormBudget: FormGroup;
    public proposalFormLegal: FormGroup;
    public proposalFormReq: FormGroup;
    public proposalFormEvidence: FormGroup;
    public proposalFormSubmit: FormGroup;

    public proposalFormAction: FormGroup;
    public admExpenceList: FormArray;
    public directExpenceList: FormArray;

    ageGroupArr: any = [];
    populationArr: any = [];
    filesToUpload: Array<File> = [];
    proposalId: boolean = false;
    typeEvaluationsArr: any = [];
    typeEvaluationsResArr: any = [];
    typeevaluationsArr: any = []
    legalArr: any = [];

    get admExpenceFormGroup() {
        return this.proposalFormBudget.get('admExpences') as FormArray;
    }
    
      // returns all form groups under directExpences
    get directExpenceFormGroup() {
        return this.proposalFormBudget.get('directExpences') as FormArray;
    }

    language;
    proposal_id = localStorage.getItem('proposal_id')
    constructor(
        elementRef: ElementRef,
        private fb: FormBuilder,
        private authProvider: AuthService,
        public router: Router,
        private route: ActivatedRoute,
        private toast: ToastProvider,
        private consoleProvider: ConsoleProvider,
        private UserMasterApi: UserMasterService,
        private userStorage: UserStorageProvider,
        private proposalApi: ProposalService,
        public dialog: MatDialog,
        private datePipe: DatePipe,
        private ngxService: NgxUiLoaderService,
        private common: CommonService
    ) {
        this.ngxService.start();
        this.Projectlisting = [];
        this.Pareotypelisting = [] ;
        this.Pareocategorylisting = [];
        this.Citylisting = [];
        this.proposal = [];
        this.getProjectList();
        this.getCityList();
        this.getPareotypeList();
        this.getAdminUserList();
        this.getMemberList();
        if (localStorage.getItem('proposal_id')) {
            this.getProposal();
        }
        this.loggedUserType = this.userStorage.get().user.user_type;
        this.loggedUserRole = this.userStorage.get().user.role.roleKey;
        this.fileBaseUrl = AppConst.FILE_BASE_URL;
        this.apiBaseUrl = AppConst.API_BASE_URL;
        this.today = this.datePipe.transform(new Date(), "MM/dd/yyyy");
        this.Pareocategorylisting = [];
    }

    ngOnInit() {
        this.common.currLang.subscribe(
            res=>{
              this.language = res
            }
          )

        const emailRegx = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        /** First Step form initialize */
        this.proposalForm_1 = this.fb.group({
            project_id: ['', ''],
            city: ['', ''],
            categories: ['', Validators.required],
            community: ['', Validators.required],
            necessity_statement: ['', Validators.required],
            programmatic_approach: ['', Validators.required],
            user_id: [this.userStorage.get().user.id, ''],
            population_status: ['', ''],
            necessity_status: ['', ''],
            programmatic_status: ['', ''],
            pareo_type: ['', Validators.required],
            pareo_origin: ['', Validators.required],
            pareo_category: ['', Validators.required],
            pareoCal: ['', ''],
            pareo_approved_grant: ['', '']
        });

        if (!localStorage.getItem('proposal_id')) {
            this.ageGroupArr = [{
                id: 1,
                value: '0-3 Infants',
                checked: false
            },
            {
                id: 2,
                value: '4-6 Preschool',
                checked: false
            },
            {
                id: 3,
                value: '7-10 Children',
                checked: false
            },
            {
                id: 4,
                value: '11-14 Teens',
                checked: false
            },
            {
                id: 5,
                value: '15-18 Young',
                checked: false
            },
            {
                id: 6,
                value: '19-21 Young Adult',
                checked: false
            },
            {
                id: 7,
                value: '22-59 Adults',
                checked: false
            },
            {
                id: 8,
                value: '60 Older Adult',
                checked: false
            }
            ]
            for (let item of this.ageGroupArr) {
                this.proposalForm_1.addControl('ageGroup_' + item.id, new FormControl());
            }

            for (var i = 0; i < this.ageGroupArr.length; i++) {
                //console.log('ageGroup_' + i);
                //population: this.fb.array([])
            }
        }

        this.proposalForm_2 = this.fb.group({
            goal_to_achieve: ['', Validators.required],
            changes_or_benefits: ['', Validators.required],
            specific_activities: ['', Validators.required],
            who_will_affected: ['', Validators.required],
            how_will_pay: ['', Validators.required],
            resources_required: ['', Validators.required],

            goal_status: ['', ''],
            changes_status: ['', ''],
            specific_status: ['', ''],
            affected_status: ['', ''],
            pay_status: ['', ''],
            resources_status: ['', ''],
            work_plan_status: ['', ''],
        });

        this.proposalForm_3 = this.fb.group({
            collecting_relevant_data: ['', Validators.required],
            how_often_organization_evaluates: ['', Validators.required],
            who_evaluates: ['', Validators.required],
            statements_describes_type_of_evaluations: this.fb.array([]),
            type_of_evaluation: ['', Validators.required],
            type_of_data: ['', Validators.required],
            type_of_evaluator: ['', Validators.required],
            step: [3, ''],

            collecting_status: ['', ''],
            who_evaluates_status: ['', ''],
            organization_evaluates_status: ['', ''],
            statements_describes_type_of_evaluations_status: ['', ''],
            type_of_evaluation_status: ['', ''],
            type_of_data_status: ['', ''],
            type_of_evaluator_status: ['', ''],
        });

        this.proposalFormReq = this.fb.group({
          department: ['', ''],
          desc: ['', ''],
          requirement_file_status: ['', ''],
        });

        this.proposalFormLegal = this.fb.group({
          legal: ['', []],
          legal_status: ['', ''],
        });

        this.proposalFormProp = this.fb.group({
          inventory_file_status: ['', ''],
          inventory_of_property: ['', ''],
        });
        
        this.proposalFormBudget = this.fb.group({
          administrative_budget_file_status: ['', ''],
          direct_budget_file_status: ['', ''],
          admExpences: this.fb.array([]),
          directExpences: this.fb.array([])
        });

        this.proposalFormEvidence = this.fb.group({
          descEvidence: ['', ''],
          evidence_doc_status: ['', ''],
        });

        this.proposalFormSubmit = this.fb.group({
          name: ['', Validators.required],
          proposal_member: [''],
          date: [ new Date(), Validators.required],
          proposalAgree: ['', Validators.required],
          application_status: ['']
        });

        this.proposalFormAction = this.fb.group({
            open_status: [''],
            entityName: [''],
            proposalUniqueId: [''],
            applicationDate: [''],
            assigned_user: [''],
            application_status: [''],
            requested_grant: ['', Validators.required],
            donation_grant: [''],
            approved_grant: ['']
        });
    }

    ngAfterViewInit() {
        this.ngxService.stop();
    }

    // convenience getter for easy access to form fields
    get f1() { return this.proposalForm_1.controls; };
    get f2() { return this.proposalForm_2.controls; };
    get f3() { return this.proposalForm_3.controls; };
    get fReq() { return this.proposalFormReq.controls; };
    get fProp() { return this.proposalFormProp.controls; };
    get fBudget() { return this.proposalFormBudget.controls; };
    get f7() { return this.proposalFormSubmit.controls; };
    get fAction() { return this.proposalFormAction.controls; };

    createAdmExpance(): FormGroup {
        return this.fb.group({
          description: [null, Validators.compose([Validators.required])],
          budget_amount: [null, Validators.compose([Validators.required])],
          tight_budget: [null],
          budget_type:[null],
          id:[null],
          proposal_id:[null],
          created_at:[null],
          updated_at:[null]
        });
      }
    
      // Direct budget formgroup
      createDirectExpance(): FormGroup {
        return this.fb.group({
          description: [null, Validators.compose([Validators.required])],
          budget_amount: [null, Validators.compose([Validators.required])],
          tight_budget: [null],
          budget_type:[null],
          id:[null],
          proposal_id:[null],
          created_at:[null],
          updated_at:[null]
        });
      }
    
      // add an Administrative budget form group
      addAdmExpance() {
        this.admExpenceFormGroup.push(this.createAdmExpance());
      }
    
      // add an Direct budget form group
      addDirectExpance() {
        this.directExpenceFormGroup.push(this.createDirectExpance());
      }
    
      // remove Administrative budget from group
      removeAdmExpance(index,id?) {
        console.log(id)
        var adminExpance = this.budgetBreakdown.filter(res=>res.id == id)
        if(id > 0){
          this.proposalApi.deleteExpance(id).subscribe(
            res=>{
              if(res.success){
                this.toast.success(res.message)
              }
      
            },
            error=>{
              this.toast.error(error.message)
            }
          )
        }
        
        this.admExpenceFormGroup.removeAt(index);
      }
    
      // remove Administrative budget from group
      removeDirectExpance(index,id?) {
        var directExpance = this.budgetBreakdown.filter(res=>res.id == id)
        if(id > 0){
          this.proposalApi.deleteExpance(id).subscribe(
            res=>{
              if(res.success){
                this.toast.success(res.message)
              }
      
            },
            error=>{
              this.toast.error(error.message)
            }
          )
        }
        
        this.directExpenceFormGroup.removeAt(index);
      }


    onSelectDepartment(v) {
        this.selectedDepartment = v;
    }

    getAdminUserList() {
        this.UserMasterApi.getAssignedUser().subscribe((res: any) => {
            if (res.success) {
                this.adminUserlisting = res.data;
            }
        }, (err) => { });
    }

    getMemberList() {
        this.UserMasterApi.memberList(this.userStorage.get().user.id).subscribe((res: any) => {
          this.memberlisting = res.members;
        }, (err) => {});
      }

    getProjectList() {
        this.UserMasterApi.projectList().subscribe((res: any) => {
            if (res.success) {
                //console.log(res.data);
                this.Projectlisting = res.data;
            }
        }, (err) => { });
    }

    getCityList() {
        this.searchParam = {
            search: 'PR',
        };
        this.UserMasterApi.citySearch(this.searchParam).subscribe((res: any) => {
            if (res.success) {
                this.Citylisting = res.data;
            }
        }, (err) => { });
    }

    getPareotypeList(){
        this.UserMasterApi.pareotypeList().subscribe((res: any) => {
          if (res.success) {
            this.Pareotypelisting = res.data;
          }
        }, (err) => {});
      }

    getProposal() {
        this.proposalId = true
        this.proposalApi.viewProposal().subscribe((res: any) => {
            if (res.success) {
                var adminExpance = []
                var directExpences = []
                this.proposal = res.data;
                this.ageGroupArr = JSON.parse(this.proposal.population)
                //console.log(this.proposal)
                // console.log("population ========", JSON.parse(this.proposal.population));
                this.budgetBreakdown = this.proposal.budget_breakdown
                //console.log("population ========", JSON.parse(this.proposal.population));
                if(this.budgetBreakdown.length > 0){
                
                //this.admExpenceList;
                    this.adminExpanceId = [];
                    this.directExpanceId = [];
                    for(let item of this.budgetBreakdown){
                        if(item.budget_type == 'Administrative'){
                        this.adminExpanceId.push(item.id)
                        this.admExpenceFormGroup.push(this.createAdmExpance());
                        adminExpance.push(item);
                        }
                        if(item.budget_type == 'Direct'){
                        this.directExpanceId.push(item.id)
                        this.directExpenceFormGroup.push(this.createDirectExpance());
                        directExpences.push(item);
                        }
                
                    }
                    this.proposalFormBudget.patchValue({
                        admExpences:adminExpance,
                        directExpences:directExpences
                    })
                
                }

                // step 1
                this.proposalForm_1.controls["project_id"].setValue(this.proposal.project_id);
                this.proposalForm_1.controls["city"].setValue(this.proposal.city);
                this.proposalForm_1.controls["categories"].setValue(this.proposal.categories);
                this.proposalForm_1.controls["community"].setValue(this.proposal.community);
                this.proposalForm_1.controls["necessity_statement"].setValue(this.proposal.necessity_statement);
                this.proposalForm_1.controls["programmatic_approach"].setValue(this.proposal.programmatic_approach);

                for (let item of JSON.parse(this.proposal.population)) {
                    this.proposalForm_1.addControl('ageGroup_' + item.id, new FormControl(item.population));
                }

                this.proposalForm_1.controls["population_status"].setValue(this.proposal.population_status);
                this.proposalForm_1.controls["necessity_status"].setValue(this.proposal.necessity_status);
                this.proposalForm_1.controls["programmatic_status"].setValue(this.proposal.programmatic_status);

                //pareo calculation
                this.pareoCalculation = (this.proposal.requested_grant * 20 )/100;
                this.onSelectPAP(this.proposal.pareo_type);
                this.proposalForm_1.controls["pareo_type"].setValue(this.proposal.pareo_type);
                this.proposalForm_1.controls["pareo_origin"].setValue(this.proposal.pareo_origin);
                this.proposalForm_1.controls["pareo_category"].setValue(this.proposal.pareo_category);

                this.proposalForm_1.controls["pareoCal"].setValue(this.pareoCalculation);
                this.proposalForm_1.controls["pareo_approved_grant"].setValue(this.proposal.approved_grant);
        

                // step 2
                this.proposalForm_2.controls["goal_to_achieve"].setValue(this.proposal.goal_to_achieve);
                this.proposalForm_2.controls["changes_or_benefits"].setValue(this.proposal.changes_or_benefits);
                this.proposalForm_2.controls["specific_activities"].setValue(this.proposal.specific_activities);
                this.proposalForm_2.controls["who_will_affected"].setValue(this.proposal.who_will_affected);
                this.proposalForm_2.controls["how_will_pay"].setValue(this.proposal.how_will_pay);
                this.proposalForm_2.controls["resources_required"].setValue(this.proposal.resources_required);

                this.proposalForm_2.controls["goal_status"].setValue(this.proposal.goal_status);
                this.proposalForm_2.controls["changes_status"].setValue(this.proposal.changes_status);
                this.proposalForm_2.controls["specific_status"].setValue(this.proposal.specific_status);
                this.proposalForm_2.controls["affected_status"].setValue(this.proposal.affected_status);
                this.proposalForm_2.controls["pay_status"].setValue(this.proposal.pay_status);
                this.proposalForm_2.controls["resources_status"].setValue(this.proposal.resources_status);
                this.proposalForm_2.controls["work_plan_status"].setValue(this.proposal.work_plan_status);

                // step 3
                this.proposalForm_3.controls["collecting_relevant_data"].setValue(this.proposal.collecting_relevant_data);
                this.proposalForm_3.controls["who_evaluates"].setValue(this.proposal.who_evaluates);
                this.proposalForm_3.controls["how_often_organization_evaluates"].setValue(this.proposal.how_often_organization_evaluates);
                this.proposalForm_3.controls["type_of_evaluation"].setValue(this.proposal.type_of_evaluation);
                this.proposalForm_3.controls["type_of_data"].setValue(this.proposal.type_of_data);
                this.proposalForm_3.controls["type_of_evaluator"].setValue(this.proposal.type_of_evaluator);

                //console.log("Evaluates ========", JSON.parse(this.proposal.statements_describes_type_of_evaluations));

                this.typeEvaluationsArr = JSON.parse(this.proposal.statements_describes_type_of_evaluations);
                if (this.typeEvaluationsArr) {
                    for (let item of this.typeEvaluationsArr) {
                        this.proposalForm_3.addControl('typeEvaluation_' + item.id, new FormControl(item.typeevaluation));
                    }
                } else {
                    this.typeEvaluationsArr = [{
                              id: 1,
                              value: 'Rarely use the evaluation data',
                              checked: false,
                              label: 'evaluation1'
                            },
                            {
                              id: 2,
                              value: 'Changes are made to programs and services based on data evaluation',
                              checked: false,
                              label: 'evaluation2'
                            },
                            {
                              id: 3,
                              value: 'Share evaluation data with the media',
                              checked: false,
                              label: 'evaluation3'
                            },
                            {
                              id: 4,
                              value: 'Share evaluation data with policy designers',
                              checked: false,
                              label: 'evaluation4'
                            },
                            {
                              id: 5,
                              value: 'Share evaluation data with community representatives',
                              checked: false,
                              label: 'evaluation5'
                            },
                            {
                              id: 6,
                              value: 'Share evaluation data with financing sources',
                              checked: false,
                              label: 'evaluation6'
                            },
                            {
                              id: 7,
                              value: 'Share evaluation data with central office',
                              checked: false,
                              label: 'evaluation7'
                            },
                            {
                              id: 8,
                              value: 'Share evaluation data with others not listed above',
                              checked: false,
                              label: 'evaluation8'
                            },
                            {
                              id: 9,
                              value: 'Use evaluation data to measure performance of team / stakeholders',
                              checked: false,
                              label: 'evaluation9'
                            }
                          ]
                    for (let item of this.typeEvaluationsArr) {
                        this.proposalForm_3.addControl('typeEvaluation_' + item.id, new FormControl());
                    }
                }
                this.proposalForm_3.controls["collecting_status"].setValue(this.proposal.collecting_status);
                this.proposalForm_3.controls["who_evaluates_status"].setValue(this.proposal.who_evaluates_status);
                this.proposalForm_3.controls["organization_evaluates_status"].setValue(this.proposal.organization_evaluates_status);
                this.proposalForm_3.controls["statements_describes_type_of_evaluations_status"].setValue(this.proposal.statements_describes_type_of_evaluations_status);
                this.proposalForm_3.controls["type_of_evaluation_status"].setValue(this.proposal.type_of_evaluation_status);
                this.proposalForm_3.controls["type_of_data_status"].setValue(this.proposal.type_of_data_status);
                this.proposalForm_3.controls["type_of_evaluator_status"].setValue(this.proposal.type_of_evaluator_status);

                // step 4
                this.proposalFormReq.controls["requirement_file_status"].setValue(this.proposal.requirement_file_status);

                // step 5
                this.proposalFormProp.controls["inventory_file_status"].setValue(this.proposal.inventory_file_status);

                // step 6
                this.proposalFormBudget.controls["administrative_budget_file_status"].setValue(this.proposal.administrative_budget_file_status);
                this.proposalFormBudget.controls["direct_budget_file_status"].setValue(this.proposal.direct_budget_file_status);

                // step legal

                this.proposalFormLegal.controls["legal_status"].setValue(this.proposal.legal_status);
                this.legalArr = JSON.parse(this.proposal.legal);
                if (this.legalArr) {
                  for (let item of this.legalArr) {
                    this.proposalFormLegal.addControl('legal_' + item.id, new FormControl(item.status));
                  }
                } else {

                  this.legalArr = [{
                      id: 1,
                      value: 'legal1',
                      checked: false,
                      label: 'legal1'
                    },
                    {
                      id: 2,
                      value: 'legal2',
                      checked: false,
                      label: 'legal2'
                    },
                    {
                      id: 3,
                      value: 'legal3',
                      checked: false,
                      label: 'legal3'
                    },
                    {
                      id: 4,
                      value: 'legal4',
                      checked: false,
                      label: 'legal4'
                    },
                    {
                      id: 5,
                      value: 'legal5',
                      checked: false,
                      label: 'legal5'
                    },
                    {
                      id: 6,
                      value: 'legal6',
                      checked: false,
                      label: 'legal6'
                    },
                    {
                      id: 7,
                      value: 'legal7',
                      checked: false,
                      label: 'legal7'
                    },
                    {
                      id: 8,
                      value: 'legal8',
                      checked: false,
                      label: 'legal8'
                    },
                    {
                      id: 9,
                      value: 'legal9',
                      checked: false,
                      label: 'legal9'
                    }
                  ]
                  for (let item of this.legalArr) {
                    this.proposalFormLegal.addControl('legal_' + item.id, new FormControl());
                  }
                }

                // step Evidence
                this.proposalFormEvidence.controls["evidence_doc_status"].setValue(this.proposal.evidence_doc_status);

                // step 7
                this.proposalFormSubmit.controls["name"].setValue(this.proposal.name);
                this.proposalFormSubmit.controls["date"].setValue(this.proposal.date);
                this.proposalFormSubmit.controls["proposalAgree"].setValue(this.proposal.proposalAgree);

                this.proposalFormAction.controls["entityName"].setValue(this.loggedUserType === 1 ? this.userStorage.get().user.entity_name : this.proposal.proposal_user.entity_name);
                if(this.proposal.date !=null){
                    this.proposalFormAction.controls["applicationDate"].setValue(this.proposal.date)
                }else{
                    this.proposalFormAction.controls["applicationDate"].setValue(this.today)
                }
                //this.proposalFormAction.controls["applicationDate"].setValue(this.proposal.date ? this.proposal.date : this.today);
                this.proposalFormAction.controls["proposalUniqueId"].setValue(this.proposal.proposal_unique_id);
                this.proposalFormAction.controls["open_status"].setValue(this.proposal.open_status);
                if (this.proposal.assign_user) {
                    this.proposalFormAction.controls["assigned_user"].setValue(this.proposal.assign_user.user_id);
                }
                this.proposalFormAction.controls["requested_grant"].setValue(this.proposal.requested_grant);
                this.proposalFormAction.controls["donation_grant"].setValue(this.proposal.donation_grant);
                this.proposalFormAction.controls["approved_grant"].setValue(this.proposal.approved_grant);

                if((this.proposal.requested_grant != this.proposal.approved_grant) && this.proposal.approved_grant>0){
                    this.administrativeExpenses = (this.proposal.approved_grant * 40 )/100;
                    this.directExpenses = (this.proposal.approved_grant * 60 )/100;
                }else{
                    this.administrativeExpenses = (this.proposal.requested_grant * 40 )/100;
                    this.directExpenses = (this.proposal.requested_grant * 60 )/100;
                }
        
                if(this.loggedUserType == 1 && this.proposal.approved_grant>0){
                    this.approveGrantStatus = true;
                }

            }
        }, (err) => { });
    }

    fileChangeEvent(event: any) {
        const files = event.target.files;
        const nameObj = event.target.name;
        //console.log('files=>', files);
        console.log('nameObj=>', nameObj);
        if (files) {
            for (let i = 0; i < files.length; i++) {
                const fileObj = {
                    name: '',
                    type: '',
                    url: ''
                }
                this.allfiles[nameObj].push(files[i]);
                fileObj.name = files[i].name;
                fileObj.type = files[i].type;

                const reader = new FileReader();
                reader.onload = (filedata) => {
                    fileObj.url = reader.result + '';
                    this.upFiles[nameObj].push(fileObj);
                }
                reader.readAsDataURL(files[i]);
            }
        }
        event.srcElement.value = null;

        //this.propFileObj[nameObj].push(this.allfiles);

        console.log('upFiles=>', this.upFiles);
        console.log('allfiles=>', this.allfiles);
        //console.log('propFileObj=>', this.propFileObj);
    }

    removefile(file: any, nameObj: any) {
        const index = this.upFiles[nameObj].indexOf(file);
        this.upFiles[nameObj].splice(index, 1);
        this.allfiles[nameObj].splice(index, 1);

        //this.propFileObj[nameObj].splice(index, 1);

        console.log('upFiles remove=>', this.upFiles);
        console.log('allfiles remove=>', this.allfiles);
        //console.log('propFileObj remove=>', this.propFileObj.necessity_statement_file);
    }

    onAgeSelect(value: string, isChecked: boolean) {
        const valueFormArray = <FormArray>this.proposalForm_1.controls.population;

        if (isChecked) {
            valueFormArray.push(new FormControl(value));
        } else {
            let index = valueFormArray.controls.findIndex(x => x.value == value)
            valueFormArray.removeAt(index);
        }
        console.log(valueFormArray);
    }

    onEvaluationSelect(value: string, isChecked: boolean) {
        if (isChecked == true) {
            for (var i = 0; i < this.typeEvaluationsArr.length; i++) {
                var evaluationValue = this.typeEvaluationsArr[i].value;
                if (evaluationValue == value) {
                    this.typeEvaluationsArr[i]['checked'] = true
                }
            }
        } else {
            for (var i = 0; i < this.typeEvaluationsArr.length; i++) {
                var evaluationValue = this.typeEvaluationsArr[i].value;
                if (evaluationValue == value) {
                    this.typeEvaluationsArr[i]['checked'] = false
                }
            }
        }
    }

    delFile(fileInfo: any) {
        this.proposalApi.delFile(fileInfo).subscribe((response: any) => {
            // console.log(response);
            if (response.success) {
                this.getProposal();
                this.toast.success('Removed Successfully');
            }
        }, (errorData: any) => { });
    }
    /**
     * First Step Submit
     */
    toggle1(value) {
        this.ngxService.start();

        this.show1 = false;
        this.show2 = true;
        this.show3 = false;
        this.showProp = false;
        this.showBudget = false;
        this.showLegal = false;
        this.showReq = false;
        this.showSubmit = false;

        this.active1 = false;
        this.active2 = true;
        this.active3 = false;
        this.activeProp = false;
        this.activeBudget = false;
        this.activeLegal = false;
        this.activeReq = false;
        this.activeSubmit = false;
        this.ngxService.stop();
    }

    toggle2() {
        this.ngxService.start();
        this.show1 = false;
        this.show2 = false;
        this.show3 = true;
        this.showProp = false;
        this.showBudget = false;
        this.showLegal = false;
        this.showReq = false;
        this.showSubmit = false;

        this.active1 = false;
        this.active2 = false;
        this.active3 = true;
        this.activeProp = false;
        this.activeBudget = false;
        this.activeLegal = false;
        this.activeReq = false;
        this.activeSubmit = false;0
        this.ngxService.stop();
    }

    toggle3() {
        this.ngxService.start();
        this.show1 = false;
        this.show2 = false;
        this.show3 = false;
        this.showProp = true;
        this.showBudget = false;
        this.showLegal = false;
        this.showReq = false;
        this.showSubmit = false;

        this.active1 = false;
        this.active2 = false;
        this.active3 = false;
        this.activeProp = true;
        this.activeBudget = false;
        this.activeLegal = false;
        this.activeReq = false;
        this.activeSubmit = false;
        this.ngxService.stop();
    }

    toggleProp() {
        this.ngxService.start();
        this.show1 = false;
        this.show2 = false;
        this.show3 = false;
        this.showProp = false;
        this.showBudget = true;
        this.showReq = false;
        this.showSubmit = false;

        this.active1 = false;
        this.active2 = false;
        this.active3 = false;
        this.activeProp = false;
        this.activeBudget = true;
        this.activeReq = false;
        this.activeSubmit = false;
        this.ngxService.stop();
    }

    toggleBudget(tight_budget = false) {
        this.ngxService.start();

        this.show1 = false;
        this.show2 = false;
        this.show3 = false;
        this.showProp = false;
        this.showBudget = false;
        this.showLegal = true;
        this.showReq = false;
        this.showSubmit = false;

        this.active1 = false;
        this.active2 = false;
        this.active3 = false;
        this.activeProp = false;
        this.activeBudget = false;
        this.activeLegal = true;
        this.activeReq = false;
        this.activeSubmit = false;
        this.ngxService.stop();
    }
    toggleLegal() {
        this.ngxService.start();
        this.show1 = false;
        this.show2 = false;
        this.show3 = false;
        this.showProp = false;
        this.showBudget = false;
        this.showLegal = false;
        this.showReq = true;
        this.showSubmit = false;

        this.active1 = false;
        this.active2 = false;
        this.active3 = false;
        this.activeProp = false;
        this.activeBudget = false;
        this.activeLegal = false;
        this.activeReq = true;
        this.activeSubmit = false;
        this.ngxService.stop();
    }

    toggleReq() {
        this.ngxService.start();
        this.show1 = false;
        this.show2 = false;
        this.show3 = false;
        this.showProp = false;
        this.showBudget = false;
        this.showLegal = false;
        this.showReq = false;
        this.showEvidence = true;
        this.showSubmit = false;

        this.active1 = false;
        this.active2 = false;
        this.active3 = false;
        this.activeProp = false;
        this.activeBudget = false;
        this.activeLegal = false;
        this.activeReq = false;
        this.activeEvidence = true;
        this.activeSubmit = false;

        this.ngxService.stop();
    }

    toggleEvidence() { 
        this.ngxService.start();
        this.show1 = false;
        this.show2 = false;
        this.show3 = false;
        this.showProp = false;
        this.showBudget = false;
        this.showLegal = false;
        this.showReq = false;
        this.showEvidence = false;
        this.showSubmit = true;

        this.active1 = false;
        this.active2 = false;
        this.active3 = false;
        this.activeProp = false;
        this.activeBudget = false;
        this.activeLegal = false;
        this.activeReq = false;
        this.activeReq = false;
        this.activeEvidence = false;
        this.activeSubmit = true;

        this.ngxService.stop();
    }

    toggle7() {
        
    }
    
    actionSubmit() {
        this.fActionsubmitted = true;
        if (this.proposalFormAction.invalid) { this.toast.error('Please enter the required fields !!'); return; }

        const formData: any = new FormData();

        formData.append('requested_grant', this.proposalFormAction.value.requested_grant);
        formData.append('donation_grant', this.proposalFormAction.value.donation_grant);
        formData.append('approved_grant', this.proposalFormAction.value.approved_grant);
        formData.append('open_status', this.proposalFormAction.value.open_status);
        formData.append('assigned_user', this.proposalFormAction.value.assigned_user);

        //this.ngxService.start();
        this.proposalApi.updateGrant(formData).subscribe((response: any) => {
            console.log(response);
            if (response.success) {
                this.toast.success(response.message);
                //this.ngxService.stop();
            }
        }, error => {
            this.ngxService.stop();
            this.fActionsubmitted = false;
            console.log('Grant update error: ', error);
        }
        );
    }

    gotoTop() {
        document.querySelector('mat-sidenav-content').scrollTop = 0;
      }

    back1() {
    this.ngxService.start();
    this.show1 = true;
    this.show2 = false;
    this.show3 = false;
    this.showProp = false;
    this.showBudget = false;
    this.showLegal = false;
    this.showReq = false;
    this.showEvidence = false;
    this.showSubmit = false;

    this.active1 = true;
    this.active2 = false;
    this.active3 = false;
    this.activeProp = false;
    this.activeBudget = false;
    this.activeLegal = false;
    this.activeReq = false;
    this.activeEvidence = false;
    this.activeSubmit = false;
    this.ngxService.stop();
    this.gotoTop();
  }

  back2() {
    this.ngxService.start();
    this.show1 = false;
    this.show2 = true;
    this.show3 = false;
    this.showProp = false;
    this.showBudget = false;
    this.showLegal = false;
    this.showReq = false;
    this.showEvidence = false;
    this.showSubmit = false;

    this.active1 = false;
    this.active2 = true;
    this.active3 = false;
    this.activeProp = false;
    this.activeBudget = false;
    this.activeLegal = false;
    this.activeReq = false;
    this.activeEvidence = false;
    this.activeSubmit = false;
    this.ngxService.stop();
    this.gotoTop();
  }

  back3() {
    this.ngxService.start();
    this.show1 = false;
    this.show2 = false;
    this.show3 = true;
    this.showProp = false;
    this.showBudget = false;
    this.showLegal = false;
    this.showReq = false;
    this.showEvidence = false;
    this.showSubmit = false;

    this.active1 = false;
    this.active2 = false;
    this.active3 = true;
    this.activeProp = false;
    this.activeBudget = false;
    this.activeLegal = false;
    this.activeReq = false;
    this.activeEvidence = false;
    this.activeSubmit = false;
    this.ngxService.stop();
    this.gotoTop();
  }

  backReq() {
    this.ngxService.start();
    this.show1 = false;
    this.show2 = false;
    this.show3 = false;
    this.showProp = false;
    this.showBudget = false;
    this.showLegal = false;
    this.showReq = true;
    this.showEvidence = false;
    this.showSubmit = false;

    this.active1 = false;
    this.active2 = false;
    this.active3 = false;
    this.activeProp = false;
    this.activeBudget = false;
    this.activeLegal = false;
    this.activeReq = true;
    this.activeEvidence = false;
    this.activeSubmit = false;
    this.ngxService.stop();
    this.gotoTop();
  }

  backProp() {
    this.ngxService.start();
    this.getProposal();
    this.show1 = false;
    this.show2 = false;
    this.show3 = false;
    this.showProp = true;
    this.showBudget = false;
    this.showLegal = false;
    this.showReq = false;
    this.showEvidence = false;
    this.showSubmit = false;

    this.active1 = false;
    this.active2 = false;
    this.active3 = false;
    this.activeProp = true;
    this.activeBudget = false;
    this.activeLegal = false;
    this.activeReq = false;
    this.activeEvidence = false;
    this.activeSubmit = false;
    this.ngxService.stop();
    this.gotoTop();
  }

  backBudget() {
    this.ngxService.start();
    this.show1 = false;
    this.show2 = false;
    this.show3 = false;
    this.showProp = false;
    this.showBudget = true;
    this.showLegal = false;
    this.showReq = false;
    this.showEvidence = false;
    this.showSubmit = false;

    this.active1 = false;
    this.active2 = false;
    this.active3 = false;
    this.activeProp = false;
    this.activeBudget = true;
    this.activeLegal = false;
    this.activeReq = false;
    this.activeEvidence = false;
    this.activeSubmit = false;
    this.ngxService.stop();
    this.gotoTop();
  }

  backLegal() {
    this.ngxService.start();
    this.show1 = false;
    this.show2 = false;
    this.show3 = false;
    this.showProp = false;
    this.showBudget = false;
    this.showLegal = true;
    this.showReq = false;
    this.showEvidence = false;
    this.showSubmit = false;

    this.active1 = false;
    this.active2 = false;
    this.active3 = false;
    this.activeProp = false;
    this.activeBudget = false;
    this.activeLegal = true;
    this.activeReq = false;
    this.activeEvidence = false;
    this.activeSubmit = false;
    this.ngxService.stop();
    this.gotoTop();
  }

  backEvidence() {
    this.ngxService.start();

    this.show1 = false;
    this.show2 = false;
    this.show3 = false;
    this.showProp = false;
    this.showBudget = false;
    this.showLegal = false;
    this.showReq = false;
    this.showEvidence = true;
    this.showSubmit = false;

    this.active1 = false;
    this.active2 = false;
    this.active3 = false;
    this.activeProp = false;
    this.activeBudget = false;
    this.activeLegal = false;
    this.activeReq = false;
    this.activeEvidence = true;
    this.activeSubmit = false;

    this.ngxService.stop();
    this.gotoTop();
  }

  backSubmit() {
    this.ngxService.start();
    this.show1 = false;
    this.show2 = false;
    this.show3 = false;
    this.showProp = false;
    this.showBudget = false;
    this.showLegal = false;
    this.showReq = false;
    this.showEvidence = false;
    this.showSubmit = true;

    this.active1 = false;
    this.active2 = false;
    this.active3 = false;
    this.activeProp = false;
    this.activeBudget = false;
    this.activeLegal = false;
    this.activeReq = false;
    this.activeEvidence = false;
    this.activeSubmit = true;
    this.ngxService.stop();
    this.gotoTop();
  }

    clearAll() {
        this.filesToUpload = [];
    }

    openProposalDialog() {
        const dialogRef = this.dialog.open(PopupModalsComponent, {
            panelClass: 'dialog-xs',
            disableClose: true,
            width: '600px'
        });
    }

    openProposalattachDialog() {
        const dialogRef = this.dialog.open(AttachPopupComponent, {
            panelClass: 'dialog-xs',
            disableClose: true,
            width: '600px'
        });
    }

    openMoreInfoDialog() {
        const dialogRef = this.dialog.open(MoreInfoComponent, {
            panelClass: 'dialog-xs',
            disableClose: true,
            width: '600px',
            data: { status: 'notView' },

        });
    }

    onSelectPAP(id) {
    //console.log(id);
      this.UserMasterApi.pareocategoryList(id).subscribe((res: any) => {
      if (res.success) {
        this.Pareocategorylisting = res.data;
        return this.Pareocategorylisting;
      }
    }, (err) => {});
  }


}
