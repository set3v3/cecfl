import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProposalViewComponent } from './proposal-view.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { ProposalViewRouteModule } from './proposal-view.routes.module';
import { UserMasterService } from 'src/app/services/apis/usermaster';
import { RoleMasterService } from 'src/app/services/apis/role.service';
import { ProposalService } from 'src/app/services/apis/proposal.service';
import { EcoFabSpeedDialModule } from '@ecodev/fab-speed-dial';
import { MatSortModule } from '@angular/material';
import { MoreInfoComponent } from '../proposal/more-info/more-info.component';
import { MoreinfoService } from 'src/app/services/apis/moreinfo.service';
import { TruncateModule } from '@yellowspot/ng-truncate';
import { NgxCurrencyModule } from "ngx-currency";
@NgModule({
  imports: [
    ProposalViewRouteModule,
    SharedModule,
    EcoFabSpeedDialModule,
    MatSortModule,
    TruncateModule,
    NgxCurrencyModule
  ],
  declarations: [
    ProposalViewComponent,
    //MoreInfoComponent
  ],
  entryComponents: [
    ProposalViewComponent,
    //MoreInfoComponent
  ],
  providers: [UserMasterService, RoleMasterService, ProposalService,MoreinfoService]
})
export class ProposalViewModule { }
