import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { AppSideNav } from '../../models/ui/app-side-nav';
import { AccordionSideNav } from '../../models/ui/app-side-nav';
import { AuthService } from '../../services/apis/auth.service';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { CryptoProvider } from '../../services/crypto/crypto.service';
import { UserStorageProvider } from '../../services/storage/user-storage.service';
import { AppConst } from '../../app.constants';
import { ChangepasswordComponent } from './change-password/change-password.component';
import { PermissionProvider } from 'src/app/services/permission/permission.service';

@Component({
  selector: 'app-post-auth',
  templateUrl: './post-auth.component.html',
  styleUrls: ['./post-auth.component.scss']
})
export class PostAuthComponent implements OnInit {

  // sideMenuList: Array<AppSideNav>;
  sideMenuList: Array<any>;
  AccordionMenuList: Array<AccordionSideNav>;
  userInfo: any;
  usertype: any;
  constructor(
    private translate: TranslateService,
    private authSrv: AuthService,
    private router: Router,
    private crypto: CryptoProvider,
    private userStorage: UserStorageProvider,
    private dialog: MatDialog,
    public Permission: PermissionProvider,
  ) {
    this.userInfo = this.userStorage.get();
    //this.usertype = this.userStorage.get().data.role.roleName;
    //console.log(this.usertype);

    this.sideMenuList = [
      { name: 'SIDE_NAV.TXT_DASHBOARD', matIcon: 'fa-th-large', canAccess: true, routeUrl: 'dashboard', childmenu: []
      },
      { name: 'SIDE_NAV.TXT_ROLE', matIcon: 'fa-tasks', routeUrl: 'role', childmenu: [], canAccess: this.Permission.sidePanelpermission('role')
      },
      { name: 'SIDE_NAV.TXT_PROPOSAL', matIcon: 'fa-sticky-note', routeUrl: 'proposal-add', childmenu: [], canAccess: this.Permission.sidePanelpermission('proposal-add')
      },
      { name: 'SIDE_NAV.TXT_PROPOSAL_LIST', matIcon: 'fa-list-alt', routeUrl: 'proposal-list', childmenu: [], canAccess: this.Permission.sidePanelpermission('proposal-list')
      },
      { name: 'SIDE_NAV.TXT_NEWS', matIcon: 'fa-newspaper-o', routeUrl: 'news-list', childmenu: [], canAccess: this.Permission.sidePanelpermission('news-list')
      },
      { name: 'SIDE_NAV.TXT_PROJECT', matIcon: 'fa-file-text', routeUrl: 'project-list', childmenu: [], canAccess: this.Permission.sidePanelpermission('project-list')
      },
      { name: 'SIDE_NAV.ADMIN_USER', matIcon: 'fa-user', routeUrl: 'admin-user', childmenu: [], canAccess: this.Permission.sidePanelpermission('admin-user')
      },
      { name: 'SIDE_NAV.USER', matIcon: 'fa-male', routeUrl: 'entity-list', childmenu: [], canAccess: this.Permission.sidePanelpermission('entity-list')
      },
      { name: 'SIDE_NAV.TXT_MEMBER_LIST', matIcon: 'fa-user', routeUrl: 'member-list', childmenu: [], canAccess: this.Permission.sidePanelpermission('member-list')
      },
      { name: 'SIDE_NAV.TXT_AUDIT', matIcon: 'fa-history', routeUrl: 'audittrail', childmenu: [], canAccess: this.Permission.sidePanelpermission('audittrail')
      },
      { name: 'SIDE_NAV.TXT_CMS', matIcon: 'fa-cogs', routeUrl: 'cms-list', childmenu: [], canAccess: this.Permission.sidePanelpermission('cms-list')
      },
      /* { name: 'SIDE_NAV.TXT_REPORT', matIcon: 'report-icon.png', routeUrl: '', canAccess: (
        this.Permission.sidePanelpermission('quaterly-reports')
        ), childmenu: [
        {
          name: 'SIDE_NAV.TXT_QUATERLY_REPORTS', matIcon: 'tb-report-icon.png',
          routeUrl: 'quaterly-reports', canAccess: this.Permission.sidePanelpermission('quaterly-reports')
        }
      ]
      }, */
      ];
    
  }
  
  ngOnInit() { }

  
  /*
    * function use to logOut
    *
    * @memberof PostAuthComponent
    */
  logOut() {
    this.userStorage.clear();
    this.router.navigate(['/']);
  }
  goToLink(link: string) {
    const url = this.Permission.sidePanelURL(link);
    window.open(url, 'blank');
  }
  isLargeScreen() {
    const width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
    if (width > 1024) {
      return true;
    } else {
      return false;
    }
  }

  getImgUrl(imagename) {
    return 'assets/images/icon/' + imagename;
  }
}
