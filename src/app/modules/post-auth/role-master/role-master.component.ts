
import { AppConst } from 'src/app/app.constants';
import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatTableDataSource, PageEvent, MatSort } from '@angular/material';
import { ConsoleProvider } from 'src/app/shared/modules/console/console.provider';
import { AddRoleComponent } from './add-role/add-role.component';
import { EditRoleComponent } from './edit-role/edit-role.component';
import { DeleteRoleComponent } from './delete-role/delete-role.component';
import { RoleMasterService } from 'src/app/services/apis/role.service';
import { PermissionProvider } from 'src/app/services/permission/permission.service';
import { RoleListingApiResponce } from 'src/app/models/role';
import { UserStorageProvider } from 'src/app/services/storage/user-storage.service';
import * as _ from 'lodash';

import { NgxUiLoaderService,SPINNER } from 'ngx-ui-loader';

@Component({
  selector: 'app-role-master',
  templateUrl: './role-master.component.html',
  styleUrls: ['./role-master.component.scss'],
  providers: []
})
export class RoleMasterComponent implements OnInit {
  spinnerType = SPINNER.rectangleBounce;
  fixVar: boolean;
  open: boolean;
  spin: boolean;
  direction: string;
  animationMode: string;
  isLoadingResults: boolean;
  showMessage: string;
  RoleListing: Array<any>;
  createtatus: boolean;
  editstatus: boolean;
  deletestatus: boolean;
  constructor(
    private consoleProvider: ConsoleProvider,
    private RoleApi: RoleMasterService,
    public dialog: MatDialog,
    private Permission: PermissionProvider,
    private userStorage: UserStorageProvider,
    private ngxService:NgxUiLoaderService,
  ) {
    this.fixed = false;
    this.open = false;
    this.spin = false;
    this.direction = 'up';
    this.animationMode = 'fling';
    this.isLoadingResults = false;
    this.RoleListing = [];
    //if(this.userStorage.get().data.role.roleName == 'Super-admin'){
      //this.editstatus = true;
    //}else{
    //}
    this.editstatus = Permission.permission('role', 'edit');
    this.createtatus = Permission.permission('role', 'create');
    this.deletestatus = Permission.permission('role', 'delete');
    this.showMessage = '';
  }

  ngOnInit() {
    this.fetchRoleListing();
  }
  /*
  * function use to  fetch RoleListing api
  * @param
  * @memberof RoleMasterComponent
  */
  fetchRoleListing() {
    this.isLoadingResults = true;
    this.ngxService.start();
    this.RoleApi.rolelisting().subscribe((response: RoleListingApiResponce) => {
      //console.log('fetchRoleListing', response);
      this.isLoadingResults = false;
      this.ngxService.stop();
      if (response.resCode === 200) {
        this.RoleListing = response.data;
      }
      if (this.RoleListing.length <= 0) {
        this.showMessage = 'No Role Avaliable';
      }
    }, (errorData: any) => {
      this.isLoadingResults = false;
      this.ngxService.stop();
      if (this.RoleListing.length <= 0) {
        this.showMessage = 'No Role Avaliable';
      }
    });
  }
  /*
  * function use to  open editRole type modal
  * @param
  * @memberof RoleMasterComponent
  */
  editRoletype(object) {
    const dialogRef = this.dialog.open(EditRoleComponent, {
      panelClass: 'dialog-sm',
      disableClose: true,
      data: object,
      width: '800px'
    });
    dialogRef.componentInstance.OnEditRole.subscribe((data: any) => {
      this.fetchRoleListing();
    });
  }
  /*
  * function use to  open editRole type modal
  * @param
  * @memberof RoleMasterComponent
  */
  deleteRoletype(object) {
    const dialogRef = this.dialog.open(DeleteRoleComponent, {
      panelClass: 'dialog-sm',
      disableClose: true,
      data: object,
      width: '800px'
    });
    dialogRef.componentInstance.OnDeleteRole.subscribe((data: any) => {
      this.fetchRoleListing();
    });
  }

  /*
  * function use add new admin user open modal
  * @param
  * @memberof RoleMasterComponent
  */
  AddRoletype() {
    const dialogRef = this.dialog.open(AddRoleComponent, {
      panelClass: 'dialog-sm',
      disableClose: true,
      width: '800px'
    });
    dialogRef.componentInstance.OnAddRole.subscribe((data: any) => {
      this.fetchRoleListing();
    });
  }

  get fixed(): boolean {
    return this.fixVar;
  }

  set fixed(fixed: boolean) {
    this.fixVar = fixed;
    if (this.fixVar) {
      this.open = true;
    }
  }

  public doAction(event: any) {
    // console.log(event);
  }

}

