
import { Component, OnInit, Inject, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder, FormArray } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ToastProvider } from 'src/app/shared/modules/toast/toast.provider';
import { ConsoleProvider } from 'src/app/shared/modules/console/console.provider';
import { RoleMasterService } from 'src/app/services/apis/role.service';
import { EDITRoleApiResponce } from 'src/app/models/role';
import { UserStorageProvider } from 'src/app/services/storage/user-storage.service';


@Component({
  selector: 'app-edit-role',
  templateUrl: './edit-role.component.html',
  styleUrls: ['./edit-role.component.scss'],
  providers: []
})
export class EditRoleComponent implements OnInit {
  editRoleform: FormGroup;
  showLoader: boolean;
  loggedUserType: any;
  thisrolekey: any;
  @Output() OnEditRole = new EventEmitter<EDITRoleApiResponce>(true);
  constructor(
    private formBulder: FormBuilder,
    private toast: ToastProvider,
    private RoleApi: RoleMasterService,
    private dialogRef: MatDialogRef<EditRoleComponent>,
    private consoleProvider: ConsoleProvider,
    private userStorage: UserStorageProvider,
    @Inject(MAT_DIALOG_DATA) public data: any

  ) {
    this.loggedUserType = this.userStorage.get().user.role.roleKey;
    this.thisrolekey = this.data.roleKey;
    // console.log(this.data);
  }
  /*
  * function use to build edit role from
  *
  * @memberof EditRoleComponent
  */
  EditRoleFromBuild() {
    this.editRoleform = new FormGroup({
      id: new FormControl(this.data.id, [
        Validators.required,
      ]),
      roleName: new FormControl(this.data.roleName, [
        Validators.required,
      ]),
      roleDescription: new FormControl(this.data.roleDescription, [
        Validators.required,
      ]),
      facility: this.formBulder.array([])
    });
    for (let i = 0; i < this.data.facility.length; i++) {
    //console.log('name=>',this.data.facility[i].name);
      // console.log(this.data.facility[i]);
      this.facility.push(this.addPackagefacility(this.data.facility[i]));
      // tslint:disable-next-line:prefer-for-of
      
      for (let j = 0; j < this.data.facility[i].permission.length; j++) {
        //console.log('name=>',this.data.facility[i].name);

        this.editRoleform.get('facility').updateValueAndValidity();
        this.permission(i).push(this.initPermisson(this.data.facility[i].permission[j]));
      }
    }
  }
  ngOnInit() {
    this.EditRoleFromBuild();
  }
  /*
  * function use to dynamicaly  init facility
  *
  * @memberof EditRoleComponent
  */
  addPackagefacility(data) {
    return this.formBulder.group({
      id: [data.id],
      name: [data.name, [Validators.required]],
      status: [data.status, [Validators.required]],
      url: [data.url, ''],
      name_key: [data.name_key, ''],
      permission: this.formBulder.array([])
    });
  }
  get facility(): FormArray {
    return this.editRoleform.get('facility') as FormArray;
  }
  permission(index): FormArray {
    return this.editRoleform.get('facility')['controls'][index]['controls'].permission as FormArray;
  }
  /*
  * function use to dynamicaly  init Permisson
  *
  * @memberof EditRoleComponent
  */
  initPermisson(data) {
    return this.formBulder.group({
      id: [data.id],
      permissionName: [data.permissionName, [Validators.required]],
      status: [data.status, [Validators.required]],
    });
  }
  /*
  * function use to api for update role
  *
  * @memberof EditRoleComponent
  */
  editRole(roleInfo) {
    if (this.editRoleform.valid) {
      this.showLoader = true;
      this.RoleApi.editrole(roleInfo).subscribe((response: EDITRoleApiResponce) => {
        this.showLoader = false;
        if (response.success) {
          this.OnEditRole.emit(response);
          this.dialogRef.close();
          this.toast.success('Role Updated Successfully');
        }
      }, (errorData: any) => {
        this.showLoader = false;
      });
    }else{
      alert('validation failed')
    }

  }
  /*
  * function use to reset role if seleted inactive
  *
  * @memberof EditRoleComponent
  */
  radioChangestatus(param, item) {
    if (param === '0') {
      // tslint:disable-next-line:prefer-for-of
      for (let j = 0; j < item.controls.permission.controls.length; j++) {
        // tslint:disable-next-line:no-string-literal
        item.controls.permission.controls[j].controls['status'].setValue(false);
        this.editRoleform.updateValueAndValidity();
      }
    }

  }

}
