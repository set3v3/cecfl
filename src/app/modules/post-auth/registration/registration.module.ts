import { NgModule } from '@angular/core';
import { RegistrationRouteModule } from './registration.routes.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { AuthService } from '../../../services/apis/auth.service';
import { RegistrationComponent } from './registration.component';
import { MatSortModule } from '@angular/material';
import { UserMasterService } from 'src/app/services/apis/usermaster';
import { CKEditorModule } from 'ngx-ckeditor';
import { RegistrationService } from '../../../services/apis/registration.service';


@NgModule({
  imports: [
    RegistrationRouteModule,
    SharedModule,
    MatSortModule,
    CKEditorModule

  ],
  declarations: [
    RegistrationComponent,
  ],
  entryComponents: [
    RegistrationComponent,
  ],
  providers: [AuthService, UserMasterService,RegistrationService]
})
export class RegistrationModule{ }
