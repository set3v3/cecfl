import { NgModule } from '@angular/core';
import { QuaterlyReportsRouteModule } from './quaterly-reports.routes.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { AuthService } from '../../../services/apis/auth.service';
import { QuaterlyReportsComponent } from './quaterly-reports.component';
import { MatSortModule } from '@angular/material';
import { UserMasterService } from 'src/app/services/apis/usermaster';
import { QualityService } from 'src/app/services/apis/qualityreports.service';
import { UserEditService } from 'src/app/services/apis/useredit.service';


//import { RegistrationService } from '../../../services/apis/registration.service';
//import { UserEditService } from '../../../services/apis/useredit.service';

//UserEditService
@NgModule({
  imports: [
    QuaterlyReportsRouteModule,
    SharedModule,
    MatSortModule
  ],
  
  declarations: [
    QuaterlyReportsComponent,
  ],
  entryComponents: [
    QuaterlyReportsComponent,
  ],
  providers: [AuthService, UserMasterService, QualityService, UserEditService]
})
export class QuaterlyReportsModule{

 
 }
