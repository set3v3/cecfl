import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastProvider } from '../../../shared/modules/toast/toast.provider';
import { QualityService } from '../../../services/apis/qualityreports.service';
import { CommonService } from 'src/app/services/apis/common.service';
import { DatePipe } from '@angular/common';
import { UserStorageProvider } from 'src/app/services/storage/user-storage.service';
import { error } from 'util';
import { NgxUiLoaderService, SPINNER } from 'ngx-ui-loader';
import { DomSanitizer } from '@angular/platform-browser';
import { environment } from 'src/environments/environment';
import { TranslateService } from '@ngx-translate/core';

@Component({
    selector: 'app-quaterly-reports',
    templateUrl: './quaterly-reports.component.html',
    styleUrls: ['./quaterly-reports.component.scss']
})
export class QuaterlyReportsComponent implements OnInit {
    spinnerType = SPINNER.rectangleBounce;
	filevaidation: boolean;
	showLoader: boolean;
    DocMessage: string;
    newslistid: number;
    quaterlyReportform: FormGroup;
    f1submitted: boolean;
    p: number = 1;
    language: string;
    newsParam: any = {};
    msg;
    created_at = new Date(1990, 0, 1);
    successMsg: boolean = false;
    errorMsg: boolean = false;
    userid: number;
    filesToUpload: Array<File> = [];
    imageUrl: Array<any> = [];
    upFiles: any = [];
    allfiles: any = [];
    proposalid: any;
    loggedUserType: any;
    classificationData:any = [];
    evidenceData:any =[]
	fileUrl:string = environment.file_base_url
    loadingFile:boolean = false;
    //calculate the administrive and direct
    public ab_c1: number = 0;
    public ab_c2: number = 0;
    public ab_c3: number = 0;
    //public ab_c4: number = 0;
    public ab_total: number = 0;
    public db_c1: number = 0;
    public db_c2: number = 0;
    public db_c3: number = 0;
    //public db_c4: number = 0;
    public db_total: number = 0;
    public q1total: number = 0;
    public q2total: number = 0;
    public q3total: number = 0;
    //public q4total: number = 0;
    public alltotal: number = 0;
    public administrative_budget: number = 0;
    public direct_budget: number = 0;
    public gross: number = 0;
    public net: number = 0;
    public expenditureData:any = [];
    public directExpenditureData:any = [];
    public admExpenceData:any = [];
    public directExpenceData:any = [];
    administativeExpenditureForm;
    directExpenditureForm;
    evidenceForm;
    constructor(
        private toast: ToastProvider,
        private quarterApi: QualityService,
        //private ExpensesApi: QualityService,
        private common: CommonService,
        public router: Router,
		private route: ActivatedRoute,
        private datePipe: DatePipe,
        private userStorage: UserStorageProvider,
        private ngxService: NgxUiLoaderService,
        private sanitizer: DomSanitizer,
        private translate: TranslateService,
    ) {
        this.newsformbuild();
        this.filevaidation = false;
        this.DocMessage = '';
		this.newslistid = 0;
        this.showLoader = false;
        this.loggedUserType = this.userStorage.get().user.user_type;
    }
    ngOnInit() {
        this.newsParam.page = this.p;

        this.common.currLang.subscribe(
            res => {
                this.language = res;
                // this.qualityreports();
            }
        )
        this.route.params.subscribe(params => {
            if (params) {
                //console.log(params);
                if (params['id']) {
                    //console.log(params['id']);
                    this.proposalid = +params['id'];
                    if (this.proposalid > 0) {
                        this.getuserdetaiils();
                    }
				}
            }
        });
        this.getExpenditureList();
        this.getAdmExpenceList();
        this.getDirectExpenditureList();
        this.getDirectExpenceList();
        this.getClassification();
        this.getEvidenceList();
    }
    getExpenditureList(){
        this.ngxService.start()
        this.quarterApi.getExpenditureList(this.proposalid).subscribe(
            res=>{
                //console.log(res)
                this.ngxService.stop()
                this.expenditureData = res['data']
            }
        )
    }
    getDirectExpenditureList(){
        this.ngxService.start()
        this.quarterApi.getDirectExpenditureList(this.proposalid).subscribe(
            res=>{
                //console.log(res)
                this.ngxService.stop()
                this.directExpenditureData = res['data']
            }
        )
    }

    getAdmExpenceList(){
        this.ngxService.start()
        this.quarterApi.getAdmExpenceList(this.proposalid).subscribe(
            res=>{
                //console.log(res)
                this.ngxService.stop()
                this.admExpenceData = res['data']
                this.getuserdetaiils()
            }
        )
    }

    getDirectExpenceList(){
        this.ngxService.start()
        this.quarterApi.getDirectExpenceList(this.proposalid).subscribe(
            res=>{
                //console.log(res)
                this.ngxService.stop()
                this.directExpenceData = res['data']
                this.getuserdetaiils()
            }
        )
    }

    calculate(event) {
        this.ab_total = (this.ab_c1) + (this.ab_c2) + (this.ab_c3);
        this.db_total = (this.db_c1) + (this.db_c2) + (this.db_c3);
        this.q1total = (this.ab_c1) + (this.db_c1);
        this.q2total = (this.ab_c2) + (this.db_c2);
        this.q3total = (this.ab_c3) + (this.db_c3);
        //this.q4total = (this.ab_c4) + (this.db_c4);
        this.alltotal = (this.ab_total) + (this.db_total);
        this.gross = (this.administrative_budget) + (this.direct_budget);
        this.net = (this.gross) - (this.alltotal);
    }
    
    newsformbuild() {
        this.administativeExpenditureForm = new FormGroup({
            quarter: new FormControl('', [Validators.required]),
            expenditure: new FormControl('', [Validators.required]),
            dateofincorporationAdmintrator: new FormControl('', [Validators.required]),
            chequeNo: new FormControl('', [Validators.required]),
            inFavourOf: new FormControl('', [Validators.required]),
            amount: new FormControl('', [Validators.required]),
        })

        this.directExpenditureForm = new FormGroup({
            quarter: new FormControl('', [Validators.required]),
            expenditure: new FormControl('', [Validators.required]),
            dateofincorporationDirect: new FormControl('', [Validators.required]),
            chequeNo: new FormControl('', [Validators.required]),
            inFavourOf: new FormControl('', [Validators.required]),
            amount: new FormControl('', [Validators.required]),
        })

        this.evidenceForm = new FormGroup({
            classification: new FormControl('', [Validators.required]),
        })
        this.quaterlyReportform = new FormGroup({
            entity_name: new FormControl(''),
            entity_code: new FormControl(''),
            created_at: new FormControl(''),
            proposal_unique_id: new FormControl(''),
            user_name: new FormControl(''),
          

            administrative_budget: new FormControl('', [Validators.required]),
            ab_c1: new FormControl(''),
            ab_c2: new FormControl(''),
            ab_c3: new FormControl(''),
            direct_budget: new FormControl('', [Validators.required]),
            db_c1: new FormControl(''),
            db_c2: new FormControl(''),
            db_c3: new FormControl(''),
			ab_total: new FormControl(''),
			db_total: new FormControl(''),
            q1total: new FormControl(''),
            q2total: new FormControl(''),
            q3total: new FormControl(''),
            alltotal: new FormControl(''),
            gross: new FormControl(''),
			net: new FormControl(''),
			name: new FormControl('', [Validators.required]),
			currentdate: new FormControl('', [Validators.required]),
			
        });
    }
    get f1() { return this.quaterlyReportform.controls; };

    addReport(value) {
        this.f1submitted = true;

        if (this.quaterlyReportform.invalid) { this.toast.error('Please enter the required fields !!'); return; }
        const formData = new FormData();
        var qualityValue = {
            ab_c1: value.ab_c1,
            ab_c2: value.ab_c2,
            ab_c3: value.ab_c3,
            db_c1: value.db_c1,
            db_c2: value.db_c2,
            db_c3: value.db_c3,
            proposal_id:this.proposalid,
            administrative_budget:value.administrative_budget,
			direct_budget:value.direct_budget
			
        }
        //console.log(qualityValue)

        this.quarterApi.expensesummary(qualityValue).subscribe
            (
            (res: any) => {
                this.getuserdetaiils();
				this.msg = res['message'];
				this.showLoader = false;
                this.router.navigate(['post-auth/proposal-list']);
                this.toast.success(this.msg)
                this.quaterlyReportform.reset();
                setTimeout(() => {
                    this.errorMsg = false;

                }, 3000);
            },
            (err) => {
                this.f1submitted = false;
                this.msg = err['message'];
                this.toast.error(this.msg)
            }
        );

    }

    getuserdetaiils = () => {
        this.quarterApi.qualityreports(this.proposalid).subscribe(
            res => {
                this.quaterlyReportform.patchValue({
                    id: this.proposalid,
                    entity_name: res.data[0].entity_name,
                    entity_code: res.data[0].entity_code,
                    created_at: this.datePipe.transform(res.data[0].created_at,"MM-dd-yyyy"),
					proposal_unique_id: res.data[0].proposal_unique_id,
					administrative_budget: res.data[0].administrative_budget,
					direct_budget: res.data[0].direct_budget,
					ab_c1: res.data[0].ab_c1,
					ab_c2: res.data[0].ab_c2,
					ab_c3: res.data[0].ab_c3,
					db_c1: res.data[0].db_c1,
					db_c2: res.data[0].db_c2,
					db_c3: res.data[0].db_c3,
					gross: res.data[0].gross,
					q1total: res.data[0].q1total,
					q2total: res.data[0].q2total,
					q3total: res.data[0].q3total,
					alltotal: res.data[0].alltotal,
					net: res.data[0].net,
					ab_total: res.data[0].ab_total,
					db_total: res.data[0].db_total,
                    //projectNameEn: res.data[0].projectNameEn,
                    //endDate: res.data[0].endDate,
					
				});
				
				if(res.data[0].db_c2 === null && res.data[0].db_c3 === null) {
					this.quaterlyReportform.patchValue({
						db_c2:0,
						db_c3:0
					});
				}
            }
        )
    }

    onSubmit(value){
        var addParam={
            proposal_id:this.proposalid, 
            bb_id:value.expenditure, 
            quarter:value.quarter, 
            date:this.datePipe.transform(value.dateofincorporationAdmintrator,"yyyy-MM-dd"), 
            expense_amount:value.amount, 
            no_check:value.chequeNo, 
            infavourof:value.inFavourOf
        }
        if (this.administativeExpenditureForm.invalid) { this.toast.error('Please enter the required fields !!'); return; }
        this.ngxService.start()
        this.quarterApi.saveAdministrativeExpenditure(addParam).subscribe(
            res=>{
                if(res.success){
                    this.ngxService.stop()
                    this.getAdmExpenceList()
                    this.administativeExpenditureForm.reset()
                    this.msg = res['message'];
                    this.toast.success(this.msg) 
                }
            },
            error=>{
                this.ngxService.stop()
                this.msg = error['message'];
                this.toast.error(this.msg) 
            }
        )
    }
    onClickDeleteAdminExpance(event){
        this.quarterApi.deleteAdministratorDirectExpance(event).subscribe(
            res=>{
                if(res.success){
                    this.getAdmExpenceList()
                    this.getuserdetaiils()
                    this.msg = res['message'];
                    this.toast.success(this.msg) 
                }
            },
            error=>{
                this.msg = error['message'];
                this.toast.error(this.msg) 
            }
        )
    }

    onSubmitDirect(value){
        //console.log('Direct=========>'+value)
        var addParam={
            proposal_id:this.proposalid, 
            bb_id:value.expenditure, 
            quarter:value.quarter, 
            date:this.datePipe.transform(value.dateofincorporationDirect,"yyyy-MM-dd"), 
            expense_amount:value.amount, 
            no_check:value.chequeNo, 
            infavourof:value.inFavourOf
        }
        if (this.directExpenditureForm.invalid) { this.toast.error('Please enter the required fields !!'); return; }
        this.ngxService.start()
        this.quarterApi.saveAdministrativeExpenditure(addParam).subscribe(
            res=>{
                if(res.success){
                    this.ngxService.stop()
                    this.getDirectExpenceList()
                    this.directExpenditureForm.reset()
                    this.msg = res['message'];
                    this.toast.success(this.msg) 
                }
            },
            error=>{
                this.ngxService.stop()
                this.msg = error['message'];
                this.toast.error(this.msg) 
            }
        )
    }
    onClickDeleteDirectExpance(event){
        this.quarterApi.deleteAdministratorDirectExpance(event).subscribe(
            res=>{
                if(res.success){
                    this.getDirectExpenceList()
                    this.getuserdetaiils()
                    this.msg = res['message'];
                    this.toast.success(this.msg) 
                }
            },
            error=>{
                this.msg = error['message'];
                this.toast.error(this.msg) 
            }
        )
    }
    getClassification(){
        this.ngxService.start()
        this.quarterApi.getClassification().subscribe(
            res=>{
                this.ngxService.stop()
                this.classificationData = res['data']
            }
        )
    }
    onChangeClassification(event){
        console.log(event)
    }
    fileChangeEvent(fileInput: any) {
        this.filesToUpload = < Array < File >> fileInput.target.files;
        let files = fileInput.target.files;
        if (files) {
            for (let i = 0; i < files.length; i++) {
                const fileObj = {
                    name: '',
                    type: '',
                    url: ''
                }
                this.allfiles.push(files[i]);
                fileObj.name = files[i].name;
                fileObj.type = files[i].type;
        
                const reader = new FileReader();
                reader.onload = (filedata) => {
                    fileObj.url = reader.result + '';
                    this.upFiles.push(fileObj);
                }
                reader.readAsDataURL(files[i]);
            }
        }
    }
    removeFile(file: any) {
        const index = this.upFiles.indexOf(file);
        this.upFiles.splice(index, 1);
        this.allfiles.splice(index, 1);
    }
    onSubmitEvidence(value){
        this.loadingFile = true;
        if (this.evidenceForm.invalid) { 
            this.loadingFile = false;
            this.toast.error(this.translate.instant('PROPOSAL.SELECT_EVIDENCE_ERR')); 
            return; 
        }
        const formData: any = new FormData();

        if(this.allfiles.length > 0){
            const files: Array<File> = this.allfiles;
            
            if(files.length > 0){
                for(let i =0; i < files.length; i++){
                    formData.append("programmatic_report[]", files[i], files[i]['name']);
                }
            }
        }
        else{
          this.loadingFile = false;
          this.toast.error(this.translate.instant('MESSAGE.FILE_ATTACH')); return; 
        }

        formData.append('proposal_id', this.proposalid);
        formData.append('evidence_id', this.evidenceForm.value.classification);

        this.ngxService.start()
        this.quarterApi.saveEvidence(formData).subscribe(
            res=>{
                if(res.success){
                    this.evidenceForm.controls["classification"].setValue('');
                    this.upFiles = [];
                    this.allfiles = [];
                    this.loadingFile = false;
                    this.ngxService.stop()
                    this.getEvidenceList()
                    this.msg = res['message'];
                    this.toast.success(this.msg) 
                }
            },
            error=>{
                this.loadingFile = false;
                this.ngxService.stop()
                this.msg = error['message'];
                this.toast.error(this.msg) 
            }
        )
    }

    getEvidenceList(){
        this.ngxService.start()
        this.quarterApi.getEvidenceList(this.proposalid).subscribe(
            res=>{
                this.ngxService.stop()
                this.evidenceData = res['data']
            }
        )
    }
    onClickDeleteEvidence(event){
        this.quarterApi.deleteEvidence(event).subscribe(
            res=>{
                if(res.success){
                    this.getEvidenceList()
                    this.msg = res['message'];
                    this.toast.success(this.msg) 
                }
            },
            error=>{
                this.msg = error['message'];
                this.toast.error(this.msg) 
            }
        )
    }
    
    onActivate(event) {
        window.scroll(0, 0);
    }



}
