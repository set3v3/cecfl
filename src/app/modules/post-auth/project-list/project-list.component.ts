import { Component, OnInit, Inject } from '@angular/core';
import { ProjectService } from '../../../services/apis/project.service';
import { Router, ActivatedRoute } from '@angular/router';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { ToastProvider } from 'src/app/shared/modules/toast/toast.provider';
import { NgxUiLoaderService,SPINNER } from 'ngx-ui-loader';
import { CommonService } from 'src/app/services/apis/common.service';
import { PermissionProvider } from 'src/app/services/permission/permission.service';

@Component({
  selector: 'app-project-list',
  templateUrl: './project-list.component.html',
  styleUrls: ['./project-list.component.scss']
})
export class ProjectListComponent implements OnInit {
	editstatus: boolean;
    createtatus: boolean;
    projectData: Array<any>;
	length;
    msg;
    p: number = 1;
    limit:number =10
	itemsPerPage:number = 10;
	ProjectParam: any = {};
	spinnerType = SPINNER.rectangleBounce;
	language;
	searchString;
	constructor(
		private Permission: PermissionProvider,
		private projectApi: ProjectService,
		public router: Router,
		public dialog: MatDialog,
		private toast: ToastProvider,
		private ngxService:NgxUiLoaderService,
		private common :CommonService
	) {
		this.projectData = [];
		this.editstatus = Permission.AuthGuardPermission('project-edit');
		this.createtatus = Permission.AuthGuardPermission('project-add');
	}

	ngOnInit() {
		this.common.currLang.subscribe(
			res=>{
				this.language = res;
			}
		)
		this.ProjectParam.page = this.p;
		this.getProjectList();
	}

	getProjectList = () => {
		this.ngxService.start();
		this.projectApi.projectList(this.ProjectParam).subscribe((res: any) => {
			this.ngxService.stop();
			this.projectData = res.data;
			this.length = res['total'];

		}, (err) => {
			this.ngxService.stop()
		});
	}
	paginationFeed(page){
        this.p = page;
        let start = (page - 1) * this.itemsPerPage;
        this.ProjectParam.page = page;
        this.getProjectList()
        
	}

	projectSearch(){
		this.ProjectParam.search = this.searchString;
		this.getProjectList();
	}
}
