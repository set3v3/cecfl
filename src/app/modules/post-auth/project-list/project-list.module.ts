import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { AuthService } from '../../../services/apis/auth.service';
import { MatSortModule } from '@angular/material';
import { UserMasterService } from 'src/app/services/apis/usermaster';
import { ProjectService } from '../../../services/apis/project.service';
import { NgxPaginationModule } from 'ngx-pagination';
import { ProjectListComponent } from './project-list.component';
import { ProjectListRouteModule } from './project-list.route.module';
@NgModule({
  imports: [
    CommonModule,
    ProjectListRouteModule,
    SharedModule,
    MatSortModule,
    NgxPaginationModule
  ],
  declarations: [
    ProjectListComponent,
  ],
  entryComponents: [
    ProjectListComponent,
  ],
  providers: [AuthService, UserMasterService, ProjectService]
})
export class ProjectListModule { }
