import { Component, OnInit, Inject, Output, EventEmitter } from '@angular/core';
import { FormBuilder, Validators, ControlContainer, FormGroup, FormControl, AsyncValidatorFn, AbstractControl, ValidationErrors, FormArray } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { AuthService } from '../../../services/apis/auth.service';
import { ToastProvider } from '../../../shared/modules/toast/toast.provider';
import { equalvalidator } from '../../../directives/validators/equal-validator';

import { ConsoleProvider } from 'src/app/shared/modules/console/console.provider';
import { ResetPasswordApiResponce } from 'src/app/models/auth';
import { MustMatch } from 'src/app/directives/validators/must-match.validator';

@Component({
    selector: 'app-change-password',
    templateUrl: './change-password.html',
    styleUrls: ['./change-password.scss']
})
export class ChangepasswordComponent implements OnInit {
    changePasswordform: FormGroup;
    @Output() mealOnadd = new EventEmitter<any>(true);
    constructor(
        private fb: FormBuilder,
        private authApi: AuthService,
        private dialogRef: MatDialogRef<ChangepasswordComponent>,
        private toast: ToastProvider,
        private consoleProvider: ConsoleProvider,

        @Inject(MAT_DIALOG_DATA) public data: any
    ) {
    }

    ngOnInit() {
        this.changePasswordform = this.fb.group({
            oldPassword: ['', Validators.required],
            password: ['', [Validators.required, Validators.minLength(6)]],
            // password: ['', [Validators.required, Validators.minLength(6), Validators.pattern("^[a-zA-Z0-9]*$")]],
            confirmPassword: ['', Validators.required]
        }, {
            validator: MustMatch('password', 'confirmPassword')
        });
      }
    /*
    * function use api hit for changePassword
    * @param
    * @memberof ChangepasswordComponent
    */
    changePassword(passwordinfo) {
        // console.log(passwordinfo);
        if (this.changePasswordform.valid) {
            this.authApi.resetPassword(passwordinfo).subscribe((responseData: ResetPasswordApiResponce) => {
                if (responseData.success) {
                    this.dialogRef.close();
                    this.toast.success(responseData.message);
                }
            }, error => {
            });
        }

    }

}
