import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CmsModalComponent } from './cms-modal.component';

describe('CmsModalComponent', () => {
  let component: CmsModalComponent;
  let fixture: ComponentFixture<CmsModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CmsModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CmsModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
