import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuditTrailRouteModule } from './audittrail.routes.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { AuthService } from 'src/app/services/apis/auth.service';
import { MatSortModule } from '@angular/material';
import { UserMasterService } from 'src/app/services/apis/usermaster';
import { AuditTrailService } from 'src/app/services/apis/audittrail.service';
import { AuditTrailComponent} from './audittrail.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { OwlDateTimeModule, OwlNativeDateTimeModule,OWL_DATE_TIME_LOCALE } from 'ng-pick-datetime';
@NgModule({
  imports: [
    AuditTrailRouteModule,
    SharedModule,
    MatSortModule,
    NgxPaginationModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
  ],
  declarations: [
    AuditTrailComponent
  ],
  entryComponents: [
    AuditTrailComponent
  ],
  providers: [
    AuthService, UserMasterService, AuditTrailService
    , { provide: OWL_DATE_TIME_LOCALE, useValue: 'en-SG'}
  ]
})
export class AuditTrailModule { }





