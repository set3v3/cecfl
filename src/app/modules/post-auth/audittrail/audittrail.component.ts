import { Component, OnInit, Inject } from '@angular/core';
import { AuditTrailService } from 'src/app/services/apis/audittrail.service';
import { Router, ActivatedRoute } from '@angular/router';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { ToastProvider } from 'src/app/shared/modules/toast/toast.provider';
import { PermissionProvider } from 'src/app/services/permission/permission.service';
import { NgxUiLoaderService,SPINNER } from 'ngx-ui-loader';
import { DatePipe } from '@angular/common';


@Component({
  selector: 'app-audittrail',
  templateUrl: './audittrail.component.html',
  styleUrls: ['./audittrail.component.scss']
 
})

export class AuditTrailComponent implements OnInit {
	loglisting: Array<any>;
	length;
    msg;
    p: number = 1;
    limit:number =10
	itemsPerPage:number = 10;
	auditTrailParam: any = {};
	spinnerType = SPINNER.rectangleBounce;
	date = new Date();
	adminUserlisting;
	roleListing;
	startDate = this.datePipe.transform(new Date(this.date.getFullYear(), this.date.getMonth(), 1),"yyyy-MM-dd");
	endDate = this.datePipe.transform(new Date(),"yyyy-MM-dd");
	dateTimeRange: Date[];
	constructor(
		private AuditTrailApi: AuditTrailService,
		public router: Router,
		public dialog: MatDialog,
		private toast: ToastProvider,
		private Permission: PermissionProvider,
		private ngxService:NgxUiLoaderService,
		private datePipe: DatePipe,
	) {
		this.loglisting = [];
	}

	ngOnInit() {
		this.auditTrailParam.page = this.p;
		//this.getloglist();
		this.getUserList()
	}

	getUserList(){
		this.AuditTrailApi.getUser().subscribe((res) => {
				this.adminUserlisting = res.data
			}
		)
	}
	onChangeUser(value){
		this.auditTrailParam.userid = value;
		this.auditTrailParam.startDate = this.startDate
        this.auditTrailParam.endDate = this.endDate
		this.getloglist()
	}

	onDateChange(event){
        this.startDate = this.datePipe.transform(event.value[0],"yyyy-MM-dd");
        this.endDate = this.datePipe.transform(event.value[1],"yyyy-MM-dd");
        this.auditTrailParam.startDate = this.startDate
        this.auditTrailParam.endDate = this.endDate
        this.getloglist()
        
	}
	clearValue(event) {
        this.dateTimeRange = null
        this.auditTrailParam.startDate = ''
        this.auditTrailParam.endDate = ''
        this.auditTrailParam.startDate = this.datePipe.transform(new Date(this.date.getFullYear(), this.date.getMonth(), 1),"yyyy-MM-dd");
        this.auditTrailParam.endDate = this.datePipe.transform(new Date(),"yyyy-MM-dd");
        this.getloglist()
    }

	paginationFeed(page){
        this.p = page;
        let start = (page - 1) * this.itemsPerPage;
        this.auditTrailParam.page = page;
        this.AuditTrailApi.LOGLIST(this.auditTrailParam).subscribe(data=> {
            this.loglisting = data.data;
        }); 
	}

	getloglist = () => {
		this.ngxService.start();
		this.AuditTrailApi.LOGLIST(this.auditTrailParam).subscribe((res: any) => {
			this.ngxService.stop();
			this.loglisting = res.data;
			this.length = res['total'];
			console.log(this.loglisting);
		}, (err) => {
			this.ngxService.stop()
		});
	}
}


