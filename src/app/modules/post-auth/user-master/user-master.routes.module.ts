import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '../../../services/guards/auth-guard.service';
import { UserMasterComponent } from './user-master.component';

const routerConfig: Routes = [
  { path: '', component: UserMasterComponent, canActivate: [AuthGuard], pathMatch: 'full' },
];

@NgModule({
  imports: [
    RouterModule.forChild(routerConfig)
  ],
  exports: [RouterModule],
  declarations: [],
  providers: [AuthGuard]
})
export class UserMasterRouteModule { }
