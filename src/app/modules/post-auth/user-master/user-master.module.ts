import { NgModule } from '@angular/core';
import { SharedModule } from '../../../shared/shared.module';
import { UserMasterComponent } from './user-master.component';
import { UserMasterRouteModule } from './user-master.routes.module';
import { ViewUserComponent } from './view-user/view-user.component';
import { EditUserComponent } from './edit-user/edit-user.component';
import { UserMasterService } from 'src/app/services/apis/usermaster';
import { PermissionProvider } from 'src/app/services/permission/permission.service';
import { MatSortModule } from '@angular/material';
import { DatePipe } from '@angular/common';
@NgModule({
  imports: [
    UserMasterRouteModule,
    SharedModule,
    MatSortModule
  ],
  declarations: [
    UserMasterComponent,
    ViewUserComponent,
    EditUserComponent
  ],
  entryComponents: [
    UserMasterComponent,
    ViewUserComponent,
    EditUserComponent
  ],
  providers: [UserMasterService, PermissionProvider, DatePipe]
})
export class UserMasterModule { }
