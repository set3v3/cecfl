
import { AppConst } from '../../../app.constants';
import { Component, OnInit, ViewChild } from '@angular/core';

import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatTableDataSource, MatPaginator, PageEvent, MatSort } from '@angular/material';
import { ViewUserComponent } from './view-user/view-user.component';
import { EditUserComponent } from './edit-user/edit-user.component';
import { ConsoleProvider } from 'src/app/shared/modules/console/console.provider';
import { UserMasterService } from 'src/app/services/apis/usermaster';
import { UserlistApiResponse, UserListInfo, AppUserReportExportApiResponse, AddressDetailsApiResponse } from 'src/app/models/usermaster';
import { UserStorageProvider } from 'src/app/services/storage/user-storage.service';
import { PermissionProvider } from 'src/app/services/permission/permission.service';
import * as _ from 'lodash';
import { NgxUiLoaderService,SPINNER } from 'ngx-ui-loader';

@Component({
  selector: 'app-user-master',
  templateUrl: './user-master.component.html',
  styleUrls: ['./user-master.component.scss'],
  providers: []
})
export class UserMasterComponent implements OnInit {
  spinnerType = SPINNER.rectangleBounce;
  isLoadingResults: boolean;
  showMessage: string;
  userlisting: Array<UserListInfo>;
  totalItem: number;
  page: number;
  viewstatus: boolean;
  userType: Array<any>;
  editstatus: boolean;
  exportstatus: boolean;
  pageindex: number;
  searchParam: any;
  LgaListing: Array<any>;
  SelectedState: any;
  SelectedLga: any;
  bydefatseletedlga: any;
  bydefatseletedState: any;
  bydefatseleteduserType: any;
  StateList: Array<any>;
  displayedColumns = ['name_of_director', 'entity_name', 'phone',  'created_at', 'activeStatus'];
  dataSource: MatTableDataSource<UserListInfo>;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(
    private consoleProvider: ConsoleProvider,
    private UserMasterApi: UserMasterService,
    public dialog: MatDialog,
    private Permission: PermissionProvider,
    private ngxService:NgxUiLoaderService,
  ) {
    this.StateList = [];
    this.bydefatseletedlga = 0;
    this.bydefatseletedState = 0;
    this.bydefatseleteduserType = 0;
    this.LgaListing = [];
    this.userType = AppConst.MOBILE_USER_TYPE;
    this.SelectedState = '';
    this.SelectedLga = '';
    this.isLoadingResults = false;
    this.userlisting = [];
    this.editstatus = Permission.permission('user', 'edit');
    this.viewstatus = Permission.permission('user', 'view');
    this.exportstatus = Permission.permission('user', 'export');
    //this.getLgaState();
    if (this.editstatus) {
      this.displayedColumns.push('edit');
    }
    if (this.viewstatus) {

      this.displayedColumns.push('view');
    }
    this.showMessage = '';

  }

  ngOnInit() {
    this.fetchUserListing(0);
  }
  /*
  * function use to open view user details modal
  * @param userdetails
  * @memberof UserMasterComponent
  */
  viewUser(object) {
    const dialogRef = this.dialog.open(ViewUserComponent, {
      panelClass: 'dialog-sm',
      disableClose: true,
      data: object,
      width: '600px'
    });
  }
  /*
  * function use to  edit status active or deactive
  * @param userdetails
  * @memberof UserMasterComponent
  */
  EditUser(object) {
    const dialogRef = this.dialog.open(EditUserComponent, {
      panelClass: 'dialog-sm',
      disableClose: true,
      data: object,

    });
    dialogRef.componentInstance.UserOnEdit.subscribe((data: any) => {
      this.fetchUserListing(this.pageindex, this.page);
    });
  }
  /*
  * function use to  pagenatition
  * @param
  * @memberof UserMasterComponent
  */
  getNext(event: PageEvent) {
    this.consoleProvider.log('getNext event', event);
    this.page = event.pageSize;
    this.pageindex = event.pageIndex;
    this.fetchUserListing(event.pageIndex, event.pageSize);
  }
  /*
  * function use for search user
  * @param
  * @memberof UserMasterComponent
  */
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    // filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    // this.userlisting = [];
    this.fetchUserListing(0, '', filterValue);
  }

  fetchUserListing(index?, limit?, searchTxt?) {
    this.ngxService.start();
    this.showMessage = '';
    this.isLoadingResults = true;
    this.searchParam = {
      userType: (searchTxt) ? searchTxt : '',
      page: (limit) ? limit : 10,
      pageIndex: (index) ? (index) : 0,
      state: this.SelectedState ? this.SelectedState : '',
      lga: this.SelectedLga ? this.SelectedLga : ''
    };
    // console.log(this.searchParam);
    this.UserMasterApi.getUserList(this.searchParam).subscribe((res: UserlistApiResponse) => {
      this.ngxService.stop();
      this.isLoadingResults = false;
      //this.consoleProvider.log('getUserList', res);
      if (res.success) {
        this.userlisting = res.data;
        this.totalItem = res.totalCount;

        this.dataSource = new MatTableDataSource(this.userlisting);
        // this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      }
      if (this.userlisting.length <= 0) {
        this.showMessage = 'No Record Found.';
      }

    }, (err) => {
      this.ngxService.stop();
      this.isLoadingResults = false;
      console.log('api_http-------errr', err);
    });
  }
  downloadExport() {
    this.isLoadingResults = true;
    const search = {
      search: (this.searchParam.userType) ? this.searchParam.userType : ''
    };
    this.UserMasterApi.downloadAppUserReportExport(search).subscribe((res: AppUserReportExportApiResponse) => {
      this.isLoadingResults = false;
      //  console.log(res);
      if (res.success) {
        window.location.href = res.data;
      }
    }, (err) => {
      this.isLoadingResults = false;
    });
  }
  getLgaState() {
    this.UserMasterApi.getlgaState().subscribe((res: any) => {
      console.log(res);
      if (res.success) {
        this.StateList = res.data.addressDetails;
      }

      // this.LgaListing = AppConst.LGA_NAME;
    }, (err) => {
    });
  }
  StateChange(event) {
    // console.log(event);
    if (event.value === '0') {
      this.LgaListing = [];
      this.SelectedState = '';
      this.SelectedLga = '';
      this.userlisting = [];
      this.fetchUserListing(0);

    } else {
      this.LgaListing = event.value.LGA;
      this.SelectedState = event.value.State;
      this.SelectedLga = '';
      this.userlisting = [];
      this.fetchUserListing(0);
    }

  }
  LgaChange(event) {
    if (event.value === '0') {
      this.SelectedLga = '';
      // console.log(this.SelectedState);
      ///  console.log(this.SelectedLga);
      this.userlisting = [];
      this.fetchUserListing(0);

    } else {
      this.SelectedLga = event.value;
      // console.log(this.SelectedState);
      // console.log(this.SelectedLga);
      this.userlisting = [];
      this.fetchUserListing(0);
    }
  }
  sortData(e) {
    // console.log(e);
  }
  UserTypechange(event) {

  }
}

