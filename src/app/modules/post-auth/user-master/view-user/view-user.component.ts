import { Component, OnInit, Inject, ViewChild, ElementRef } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ToastProvider } from '../../../../shared/modules/toast/toast.provider';
import { ConsoleProvider } from 'src/app/shared/modules/console/console.provider';
import { UserMasterService } from 'src/app/services/apis/usermaster';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ImeiApiResponse } from 'src/app/models/usermaster';
import { DatePipe } from '@angular/common';



@Component({
  selector: 'app-view-user',
  templateUrl: './view-user.component.html',
  styleUrls: ['./view-user.component.scss'],
  providers: []
})
export class ViewUserComponent implements OnInit {
  userInfo: any;
  showLoader: boolean;
  imeiview: boolean;
  ieminumber: string;
  showbtn: boolean;
  passFrom: FormGroup;
  constructor(
    private toast: ToastProvider,
    private dialogRef: MatDialogRef<ViewUserComponent>,
    private consoleProvider: ConsoleProvider,
    private UserMasterApi: UserMasterService,
    private datePipe: DatePipe,
    @Inject(MAT_DIALOG_DATA) public data: any

  ) {
    this.userInfo = this.data;
    this.showLoader = false;
    this.imeiview = false;
    this.showbtn = true;
    this.consoleProvider.log('userInfo', this.userInfo);
    this.ieminumber = this.userInfo.hasOwnProperty('devieId') ? this.userInfo.devieId : 'NA';
    this.userInfo.createDate = this.datePipe.transform((this.userInfo.createDate)).toString();

  }
  ngOnInit() {
    this.passFrom = new FormGroup({
      id: new FormControl(this.userInfo.user_id, [
        Validators.required,
      ]),
      password: new FormControl('', [
        Validators.required,
      ])

    });
  }
  imeiViewClick() {
    this.imeiview = !this.imeiview;
    // this.scrollToBottom();
  }
  getimei(param) {

    this.showLoader = true;
    this.UserMasterApi.ShowImeiNo(param).subscribe((response: ImeiApiResponse) => {
      // console.log();
      this.showLoader = false;
      this.imeiview = !this.imeiview;
      this.showbtn = false;
      if (response.resCode === 200) {
        this.ieminumber = response.data.deviceToken ? response.data.deviceToken : 'NA';
      }
    }, (errorData: any) => {
      this.showLoader = false;
    });
  }
}
