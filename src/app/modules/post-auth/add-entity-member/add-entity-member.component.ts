import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastProvider } from 'src/app/shared/modules/toast/toast.provider';
import { NgxUiLoaderService, SPINNER } from 'ngx-ui-loader';
import { CommonService } from 'src/app/services/apis/common.service';
import { UserMasterService } from 'src/app/services/apis/usermaster';
import { UserStorageProvider } from 'src/app/services/storage/user-storage.service';

@Component({
  selector: 'app-add-entity-member',
  templateUrl: './add-entity-member.component.html',
  styleUrls: ['./add-entity-member.component.scss']
})
export class AddEntityMemberComponent implements OnInit {

  language;
  public editorValue: string = '';
  addMemberform: FormGroup;
  msg;
  successMsg: boolean = false;
  errorMsg: boolean = false;
  memberId: number = 0;
  public formSubmit: boolean = false;
  spinnerType = SPINNER.rectangleBounce;
  public edittitle: boolean = false;
  public searchParam: any;
  public Citylisting: Array <any>;
  constructor(
    private addmemberApi: UserMasterService,
    public router: Router,
    public route: ActivatedRoute,
    private toast: ToastProvider,
    private ngxService: NgxUiLoaderService,
    private common: CommonService,
    private userStorage: UserStorageProvider,
    
  ) {
    this.addMemberFormBuild();
    this.getCityList();
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      if (params) {
        //console.log(params);
        if (params['id']) {
          //console.log(params['id']);
          this.memberId = +params['id'];
          if (this.memberId > 0) {
            this.edittitle =true;
            this.view();
          }
        }
      }
    });
    
  }

  addMemberFormBuild() {
    const emailRegx = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    this.addMemberform = new FormGroup({ 
      member_name: new FormControl('', [Validators.required, Validators.pattern("^[a-zA-Z0-9]+(?: [a-zA-Z0-9]+)*$") ]),
      designation: new FormControl('', [Validators.required]),
      telephone: new FormControl('', [ Validators.required, Validators.pattern(/^[6-9]\d{9}$/) ]),
      email: new FormControl(''),
      address:  new FormControl('', [Validators.required, Validators.pattern("^[a-zA-Z0-9\.\/\,\d./-]+(?: [a-zA-Z0-9\.\/\,\d./-]+)*$") ]),
      city: new FormControl('', [Validators.required]),
      state: new FormControl('', [Validators.required]),
      zip: new FormControl('', [Validators.required]),
    });
  }


  addMember(value) {
    this.formSubmit = true;
    var memberObj = {
      member_name: value.member_name,
      designation: value.designation,
      telephone: value.telephone,
      email: value.email,
      address: value.address,
      city: value.city,
      state: value.state,
      zip: value.zip,
    }

    if (this.addMemberform.valid) {
      if (this.memberId > 0) {
        this.ngxService.start()
        this.addmemberApi.editMember(memberObj, this.memberId).subscribe(
          (res: any) => {
            this.ngxService.stop()
            this.msg = res['message'];
            this.router.navigate(['post-auth/member-list']);
            this.toast.success(this.msg)
          },
          (err) => {
            this.ngxService.stop()
            this.formSubmit = false;
          });
      } else{
        this.ngxService.start()
        this.addmemberApi.addMember(memberObj).subscribe(
          (res: any) => {
            this.ngxService.stop()
            this.msg = res['message'];
            this.router.navigate(['post-auth/member-list']);
            this.toast.success(this.msg)
            this.addMemberform.reset();
          },
          (err) => {
            this.ngxService.stop()
            this.formSubmit = false;
            this.msg = err['message'];
            this.toast.error(this.msg)
          }
        );
      }
    }
  }

  view() {
    this.ngxService.start()
    this.addmemberApi.singleViewMember(this.memberId).subscribe(
        data => {
            this.ngxService.stop()
            console.log(data)
            if (this.memberId) {
                this.addMemberform.setValue({
                  member_name : data['member'].member_name,
                  designation : data['member'].designation,
                  telephone : data['member'].telephone,
                  email : data['member'].email,
                  address : data['member'].address,
                  city :data['member'].city,
                  state :data['member'].state,
                  zip :data['member'].zip,
                   // status :data.status.toString(),
                });
            } 
            this.addMemberform.updateValueAndValidity();
            console.log(this.addMemberform);
        }
    )
}


getCityList() {
  this.searchParam = {
    search: 'PR',
  };
  this.addmemberApi.citySearch(this.searchParam).subscribe((res: any) => {
    if (res.success) {
    this.Citylisting = res.data;
    }
  }, (err) => {});
  }

  get member_name() {
    return this.addMemberform.get('member_name');
  }

  get designation() {
    return this.addMemberform.get('designation');
  }

  get telephone() {
    return this.addMemberform.get('telephone');
  }
  get email() {
    return this.addMemberform.get('email');
  }
  get address() {
    return this.addMemberform.get('address');
  }

  get city() {
    return this.addMemberform.get('city');
  }

  get state() {
    return this.addMemberform.get('state');
  }

  get zip() {
    return this.addMemberform.get('zip');
  }


}
