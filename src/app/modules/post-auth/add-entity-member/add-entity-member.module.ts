import { NgModule } from '@angular/core';
import { SharedModule } from 'src/app/shared/shared.module';
import { AuthService } from '../../../services/apis/auth.service';
import { MatSortModule } from '@angular/material';
import { UserMasterService } from 'src/app/services/apis/usermaster';
import { RegistrationService } from '../../../services/apis/registration.service';
import { AddEntityMemberComponent } from './add-entity-member.component';
import { AddEntityMemberRoutesModule } from './add-entity-member-routes.module';
import { UserEditService } from 'src/app/services/apis/useredit.service';


@NgModule({
  imports: [
    AddEntityMemberRoutesModule,
    SharedModule,
    MatSortModule,
   

  ],
  declarations: [
    AddEntityMemberComponent,
  ],
  entryComponents: [
    AddEntityMemberComponent,
  ],
  providers: [AuthService, UserMasterService,RegistrationService,UserEditService]
})
export class AddEntityMemberModule { }
