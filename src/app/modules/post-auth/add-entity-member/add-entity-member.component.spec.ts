import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddEntityMemberComponent } from './add-entity-member.component';

describe('AddEntityMemberComponent', () => {
  let component: AddEntityMemberComponent;
  let fixture: ComponentFixture<AddEntityMemberComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddEntityMemberComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEntityMemberComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
