import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { MoreinfoService } from 'src/app/services/apis/moreinfo.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastProvider } from 'src/app/shared/modules/toast/toast.provider';
import { UserStorageProvider } from 'src/app/services/storage/user-storage.service';

@Component({
  selector: 'app-more-info',
  templateUrl: './more-info.component.html',
  styleUrls: ['./more-info.component.scss']
})
export class MoreInfoComponent implements OnInit {
  moreinfolisting: Array<any>;
  addMoreinfoform: FormGroup;
  proposalid: number;
  public f1submitted: boolean = false;
  constructor(
    
    private dialogRef: MatDialogRef<MoreInfoComponent>,
    @Inject(MAT_DIALOG_DATA) public showData:any,
    private moreinfoApi: MoreinfoService,
    public router: Router,
    public route: ActivatedRoute,
    private userStorage: UserStorageProvider,
    private toast: ToastProvider,
  ) 
  {
    this.moreinfoformbuild();
    this.moreinfolisting = [];
   }
 
  ngOnInit() {
    //console.log(this.userStorage.get());
    this.getmoreinfolist(localStorage.getItem('proposal_id'));
  }

  get f1() { return this.addMoreinfoform.controls; };
  moreinfoformbuild() {
    this.addMoreinfoform = new FormGroup({

      MoreInfoBody: new FormControl('', [
        Validators.required,
      ]),

      ProposalId: new FormControl(localStorage.getItem('proposal_id') ? localStorage.getItem('proposal_id') : '', [
      ]),

      CreatedBy: new FormControl(this.userStorage.get().user.id ? this.userStorage.get().user.id : '', [
      ]),

    });
  }//


  addMoreinfo(value) {
    this.f1submitted = true;
    if (this.addMoreinfoform.invalid) { this.toast.error('Please enter the required fields !!'); return; }
      //console.log( value); return false;
      this.moreinfoApi.addmoreinfo(value).subscribe(
        (res: any) => {
          if(res.success){
            this.dialogRef.close();
            this.toast.success('More Info added successfully'); return;
          }else{
            this.toast.error('Unable to add note'); return;
          }
          //this.router.navigate(['post-auth/proposal']);
          
        },
        (err) => {
          this.f1submitted = false;
        });
  }

  getmoreinfolist = (proposal_id) => {
    console.log(proposal_id);
		this.moreinfoApi.listmoreinfo(proposal_id).subscribe((res: any) => {
			console.log(res);
			this.moreinfolisting = res.data;
		}, (err) => {

		});
  }
  
  moreinfodelete(id,proposal_id) {
		//console.log(id);
		this.moreinfoApi.moreinfodelete(id).subscribe((res: any) => {
			//console.log(res);
			this.getmoreinfolist(proposal_id);

	}, (err) => {

		});
	}

}
