import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { NotesService } from 'src/app/services/apis/notes.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastProvider } from 'src/app/shared/modules/toast/toast.provider';
import { UserStorageProvider } from 'src/app/services/storage/user-storage.service';
import { AppConst } from 'src/app/app.constants';

@Component({
  selector: 'app-popup-modals',
  templateUrl: './popup-modals.component.html',
  styleUrls: ['./popup-modals.component.scss']
})
export class PopupModalsComponent implements OnInit {
  filesToUpload: Array<File> = [];
  urls: Array<any>;
  noteslisting: Array<any>;
  addNotesform: FormGroup;
  proposalid: number;
  noteList:boolean = false;
  fileBaseUrl = AppConst.FILE_BASE_URL;
  public f1submitted: boolean = false;
  constructor(
    private dialogRef: MatDialogRef<PopupModalsComponent>,
    private notesApi: NotesService,
    public router: Router,
    public route: ActivatedRoute,
    private userStorage: UserStorageProvider,
    private toast: ToastProvider,
  ) 
  {
    this.notesformbuild();
    this.noteslisting = [];
   }
 
  ngOnInit() {
    //console.log(this.userStorage.get());
    this.getnoteslist(localStorage.getItem('proposal_id'));
  }

  get f1() { return this.addNotesform.controls; };
  notesformbuild() {
    this.addNotesform = new FormGroup({

      NotesBody: new FormControl('', [
        Validators.required,
      ]),

      ProposalId: new FormControl(localStorage.getItem('proposal_id') ? localStorage.getItem('proposal_id') : '', [
      ]),

      CreatedBy: new FormControl(this.userStorage.get().user.id ? this.userStorage.get().user.id : '', [
      ]),

    });
  }//

  fileChangeEvent(fileInput: any) {
    this.filesToUpload = <Array<File>>fileInput.target.files;
    this.urls = [];
    let files = fileInput.target.files;
    if (files) {
        for (let file of files) {
            let reader = new FileReader();
            reader.onload = (e: any) => {
                this.urls.push(file.name);
            }
            reader.readAsDataURL(file);
        }
    }
  }

  removefile(file: any, ) {
    const index = this.urls.indexOf(file);
    this.urls.splice(index, 1);
  }


  addNotes(value) {
    this.f1submitted = true;
    const formData = new FormData();
    if(this.filesToUpload[0]){
      formData.append("note_file_name", this.filesToUpload[0], this.filesToUpload[0].name);
    }
    
    formData.append('NotesBody', value.NotesBody);
    formData.append('ProposalId',localStorage.getItem('proposal_id'));
    formData.append('CreatedBy', this.userStorage.get().user.id);
    if (this.addNotesform.invalid) { this.toast.error('Please enter the required fields !!'); return; }
      //console.log( value); return false;
      this.notesApi.addnotes(formData).subscribe(
        (res: any) => {
          if(res.success){
            this.noteList = true
            //this.dialogRef.close();
            this.getnoteslist(localStorage.getItem('proposal_id'));
            this.toast.success('Note added successfully'); return;
          }else{
            this.toast.error('Unable to add note'); return;
          }
          //this.router.navigate(['post-auth/proposal']);
          
        },
        (err) => {
          this.f1submitted = false;
        });
  }

  getnoteslist = (proposal_id) => {
    console.log(proposal_id);
		this.notesApi.listnotes(proposal_id).subscribe((res: any) => {
      console.log(res);
			this.noteslisting = res.data;
		}, (err) => {

		});
  }
  
  notesdelete(id) {
		//console.log(id);
		this.notesApi.notedelete(id).subscribe((res: any) => {
      //console.log(res);
			this.getnoteslist(localStorage.getItem('proposal_id'));
      this.toast.success('Note deleted successfully'); return;
	}, (err) => {

		});
	}

}
