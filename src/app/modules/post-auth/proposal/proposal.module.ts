import { NgModule } from '@angular/core';
import { ProposalRouteModule } from './proposal.routes.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { AuthService } from 'src/app/services/apis/auth.service';
import { ProposalService } from 'src/app/services/apis/proposal.service';
import { ProposalComponent,RejectedModalComponent,ApprovedModalComponent } from './proposal.component';
import { MatSortModule } from '@angular/material';
import { UserMasterService } from 'src/app/services/apis/usermaster';
import { PopupModalsComponent } from './popup-modals/popup-modals.component';
import { AttachPopupComponent } from './attach-popup/attach-popup.component';
//import { MoreInfoComponent } from './more-info/more-info.component';
import { NotesService } from 'src/app/services/apis/notes.service';
import { MoreinfoService } from 'src/app/services/apis/moreinfo.service';
//import { ApproveModalComponent } from './approve-modal/approve-modal.component';
import { TruncateModule } from '@yellowspot/ng-truncate';
import { NgxCurrencyModule } from "ngx-currency";
import { AuditTrailService } from 'src/app/services/apis/audittrail.service';

@NgModule({
  imports: [
    ProposalRouteModule,
    SharedModule,
    MatSortModule,
    TruncateModule,
    NgxCurrencyModule
  ],
  declarations: [
    ProposalComponent,
    PopupModalsComponent,
    AttachPopupComponent,
    ApprovedModalComponent,
    RejectedModalComponent,
    //MoreInfoComponent
  ],
  entryComponents: [
    PopupModalsComponent,
    AttachPopupComponent,
    ApprovedModalComponent,
    RejectedModalComponent,
    //MoreInfoComponent
    
  ],
  providers: [AuthService, UserMasterService, ProposalService, NotesService, MoreinfoService, AuditTrailService]
})
export class ProposalModule { }
