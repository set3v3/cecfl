import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AttachPopupComponent } from './attach-popup.component';

describe('AttachPopupComponent', () => {
  let component: AttachPopupComponent;
  let fixture: ComponentFixture<AttachPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AttachPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AttachPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
