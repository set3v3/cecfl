import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { ToastProvider } from 'src/app/shared/modules/toast/toast.provider';
import { ProposalService } from 'src/app/services/apis/proposal.service';
import { UserStorageProvider } from 'src/app/services/storage/user-storage.service';
import { AppConst } from 'src/app/app.constants';
@Component({
    selector: 'app-attach-popup',
    templateUrl: './attach-popup.component.html',
    styleUrls: ['./attach-popup.component.scss']
})
export class AttachPopupComponent implements OnInit {
    filesToUpload: Array<File> = [];
    urls: Array<any>;
    attachForm: FormGroup;
    public formSubmit: boolean = false;
    msg;
    proposal_id;
    user_id;
    attachListing:any =[];
    attachList:boolean = false;
    fileBaseUrl = AppConst.FILE_BASE_URL;
    constructor(
        private dialogRef: MatDialogRef<AttachPopupComponent>,
        private ngxService:NgxUiLoaderService,
        private toast: ToastProvider,
        private proposalApi: ProposalService,
        private userStorage: UserStorageProvider,
    ) { }

    ngOnInit() {
        this.proposal_id = window.location.href.substr(window.location.href.lastIndexOf('/') + 1);
        this.user_id =  this.userStorage.get().user.id;
        this.attachForm = new FormGroup({
            desc: new FormControl('', [Validators.required]),
        });
        this.getAttachDoc()
    }

    fileChangeEvent(fileInput: any) {
        this.filesToUpload = <Array<File>>fileInput.target.files;
        this.urls = [];
        let files = fileInput.target.files;
        if (files) {
            for (let file of files) {
                let reader = new FileReader();
                reader.onload = (e: any) => {
                    this.urls.push(file.name);
                }
                reader.readAsDataURL(file);
            }
        }
    }

    removefile(file: any, ) {
        const index = this.urls.indexOf(file);
        this.urls.splice(index, 1);
    }

    addAttach(value) {
        this.formSubmit = true;
        const formData = new FormData();
        formData.append("admin_attach_file[]", this.filesToUpload[0], this.filesToUpload[0].name);
        formData.append('desc', value.desc);
        formData.append('proposal_id',this.proposal_id);
        formData.append('user_id', this.user_id);

        if (this.attachForm.valid) {
            
            this.ngxService.start()
            this.proposalApi.addAttach(formData).subscribe(
                (res: any) => {
                    this.ngxService.stop()
                    this.msg = res['message'];
                    this.attachList = true;
                    this.urls = [];
                    this.getAttachDoc()
                    this.toast.success(this.msg)
                    this.attachForm.reset();
                },
                (err) => {
                    this.ngxService.stop()
                    this.formSubmit = false;
                    this.msg = err['message'];
                    this.toast.error(this.msg)
                }
            );
        }
    }
    getAttachDoc(){
        this.ngxService.start();
		this.proposalApi.getAttachDoc(this.proposal_id).subscribe((res: any) => {
			this.ngxService.stop();
			this.attachListing = res.data;

		}, (err) => {
            this.ngxService.stop();
            this.toast.error(this.msg)
		});
    }

    get desc() {
        return this.attachForm.get('desc');
    }
}
