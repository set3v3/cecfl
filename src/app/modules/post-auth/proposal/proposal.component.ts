import { Component, OnInit, ViewChild, TemplateRef, ElementRef, Inject } from '@angular/core';
import { FormBuilder, Validators, ControlContainer, FormGroup, FormControl, AsyncValidatorFn, AbstractControl, ValidationErrors, FormArray } from '@angular/forms';
import { AuthService } from 'src/app/services/apis/auth.service';
import { ProposalService } from 'src/app/services/apis/proposal.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastProvider } from 'src/app/shared/modules/toast/toast.provider';
import { ConsoleProvider } from 'src/app/shared/modules/console/console.provider';
import { UserMasterService } from 'src/app/services/apis/usermaster';
import { UserStorageProvider } from 'src/app/services/storage/user-storage.service';
import { requireCheckboxesToBeCheckedValidator } from "src/app/directives/validators/require-checkboxes-to-be-checked.validator";
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { PopupModalsComponent } from './popup-modals/popup-modals.component';
import { AttachPopupComponent } from './attach-popup/attach-popup.component';
import { MoreInfoComponent } from './more-info/more-info.component';
import { AppConst } from 'src/app/app.constants';
import { DatePipe } from '@angular/common';
import { NgxUiLoaderService,SPINNER } from 'ngx-ui-loader';
import { CommonService } from 'src/app/services/apis/common.service';
import { createOfflineCompileUrlResolver } from '@angular/compiler';
import { TranslateService } from '@ngx-translate/core';
import { AuditTrailService } from 'src/app/services/apis/audittrail.service';

declare var $: any;

@Component({
  selector: 'app-proposal',
  templateUrl: './proposal.component.html',
  styleUrls: ['./proposal.component.scss']
})

export class ProposalComponent implements OnInit {
  
  spinnerType = SPINNER.rectangleBounce;
  public showLoader: boolean;
  budgetBreakdown:any = []
  loader:boolean = false;
  loadingFile:boolean = false;
 
  
  Deptlisting = [
    {
      key: 'puerto_rico_state_department',
      value: 'DEPARTAMENTO_DE_ESTADO_DE_PUERTO_RICO'
    },
    {
      key: 'department_of_finance',
      value: 'DEPARTAMENTO_DE_HACIENDA'
    },
    {
      key: 'puerto_rico_state_insurance_fund_corporation',
      value: 'CORPORACIÓN_DEL_FONDO_DEL_SEGURO_DEL_ESTADO_DE_PUERTO_RICO'
    },
    {
      key: 'municipal_income_collection_center',
      value: 'CENTRO_DE_RECAUDACIONES_DE_INGRESOS_MUNICIPALES'
    },
    {
      key: 'osfl',
      value: 'OSFL'
    }
    ]
    /*{
      key: 'internal_revenue_services',
      value: 'INTERNAL REVENUE SERVICES (IRS)'
    },{
      key: 'department_of_labor_and_human_resources',
      value: 'DEPARTAMENTO DEL TRABAJO Y RECURSOS HUMANOS'
    },
    {
      key: 'health_department',
      value: 'DEPARTAMENTO DE SALUD'
    },
    {
      key: 'administration_of_regulations_and_permits',
      value: 'ADMINISTRACIÓN DE REGLAMENTOS Y PERMISOS'
    },
    {
      key: 'puerto_rico_fire_fighter_body',
      value: 'CUERPO DE BOMBEROS DE PUERTO RICO'
    },
    {
      key: 'custody_agency',
      value: 'AGENCIA CUSTODIO'
    },
    {
      key: 'additional_documents',
      value: 'DOCUMENTOS ADICIONALES'
    },
    {
      key: 'seguro_social',
      value: 'SEGURO SOCIAL'
    },
    {
      key: 'administration_for_child_support',
      value: 'ADMINISTRACION PARA EL SUSTENTO DE MENORES (ASUME)'
    },
    {
      key: 'puerto_rico_police',
      value: 'POLICIA DE PUERTO RICO'
    },
    {
      key: 'negotiated_conventions_of_san_jun',
      value: 'NEGOCIADO DE CONVENCIONES DE SAN JUN, PR'
    },
    {
      key: 'higher_education_council',
      value: 'CONSEJO SUPERIOR DE EDUCACION'
    }*/

  Evidencelist = [
    {
      key: 'EVIDENCE_OF_MATCHING_SPICE'
    },
    {
      key: 'FUNDING_SOURCES'
    },
    {
      key: 'COLLABORATIVE_AGREEMENT'
    },
    {
      key: 'CERTIFICATION_OF_MATCHING_FUNDS'
    },
    {
      key: 'OTHER_ANNEXES'
    },
    {
      key: 'EVIDENCE_OF_MATCHING_CASH_FUNDS'
    }
  ]

  
  startDate = new Date(1990, 0, 1);
  minDate = new Date(2000, 0, 1);
  maxDate = new Date();
  searchParam: any;
  selectedDepartment: any;
  selectedEvidence: any;
  selectedPA: any;
  selectedDA: any;
  Projectlisting:any =[] ;
  Citylisting: Array < any > ;
  Pareotypelisting:any =[] ;
  Pareocategorylisting:any[];
  adminUserlisting;
  memberlisting: Array < any > ;
  loggedUserType: any;
  loggedUserRole: any;
  fileBaseUrl: any;
  apiBaseUrl: any;
  templateUrl: any;
  today: any;
  adminExpanceId:any = []
  directExpanceId:any = []
  public proposal: any;
  urls: Array < any > ;
  adminExpance:any = []
  administrativeExpenses: any;
  pareoCalculation: any;
  directExpenses: any;
  upDocKey:any = []
  upDocTotal:any = 0;
  upEvidenceKey:any = []

  public total_adm_budget_amount: number = 0;
  public total_adm_tight_budget: number = 0;
  public total_direct_budget_amount: number = 0;
  public total_direct_tight_budget: number = 0;

  upFiles: any = {
    necessity_statement_file: [],
    work_plan_file: [],
    property_inventory_file: [],
    adm_expence_file: [],
    direct_expence_file: [],
    evidence_doc_file: [],

    puerto_rico_state_department: [],
    internal_revenue_services: [],
    puerto_rico_state_insurance_fund_corporation: [],
    municipal_income_collection_center: [],
    department_of_finance: [],
    department_of_labor_and_human_resources: [],
    health_department: [],
    administration_of_regulations_and_permits: [],
    puerto_rico_fire_fighter_body: [],
    custody_agency: [],
    additional_documents: [],
    seguro_social: [],
    administration_for_child_support: [],
    puerto_rico_police: [],
    negotiated_conventions_of_san_jun: [],
    higher_education_council: [],
    osfl: [],
  };
  allfiles: any = {
    necessity_statement_file: [],
    work_plan_file: [],
    property_inventory_file: [],
    adm_expence_file: [],
    direct_expence_file: [],
    evidence_doc_file: [],

    puerto_rico_state_department: [],
    internal_revenue_services: [],
    puerto_rico_state_insurance_fund_corporation: [],
    municipal_income_collection_center: [],
    department_of_finance: [],
    department_of_labor_and_human_resources: [],
    health_department: [],
    administration_of_regulations_and_permits: [],
    puerto_rico_fire_fighter_body: [],
    custody_agency: [],
    additional_documents: [],
    seguro_social: [],
    administration_for_child_support: [],
    puerto_rico_police: [],
    negotiated_conventions_of_san_jun: [],
    higher_education_council: [],
    osfl: [],
  };


  public f1submitted: boolean = false;
  public f2submitted: boolean = false;
  public f3submitted: boolean = false;
  public fPropsubmitted: boolean = false;
  public fBudgetsubmitted: boolean = false;
  public fLegalsubmitted: boolean = false;
  public fReqsubmitted: boolean = false;
  public fEvidencesubmitted: boolean = false;
  public f7submitted: boolean = false;
  public fActionsubmitted: boolean = false;

  public agreeChk: boolean = false;

  public show: any;
  public show1: boolean = true;
  public show2: boolean = false;
  public show3: boolean = false;
  public showProp: boolean = false;
  public showBudget: boolean = false;
  public showLegal: boolean = false;
  public showReq: boolean = false;
  public showEvidence: boolean = false;
  public showSubmit: boolean = false;

  public active: any;
  public active1: boolean = true;
  public active2: boolean = false;
  public active3: boolean = false;
  public activeProp: boolean = false;
  public activeBudget: boolean = false;
  public activeLegal: boolean = false;
  public activeReq: boolean = false;
  public activeEvidence: boolean = false;
  public activeSubmit: boolean = false;

  public proposalForm_1: FormGroup;
  public proposalForm_2: FormGroup;
  public proposalForm_3: FormGroup;
  public proposalFormProp: FormGroup;
  public proposalFormBudget: FormGroup;
  public proposalFormLegal: FormGroup;
  public proposalFormReq: FormGroup;
  public proposalFormEvidence: FormGroup;
  public proposalFormSubmit: FormGroup;

  public proposalFormAction: FormGroup;

  public admExpenceList: FormArray;
  public directExpenceList: FormArray;

  public tightBudgetStatus: boolean = false;
  public budgetPrevStatus: boolean = true;
  public approveGrantStatus: boolean = false;

  // returns all form groups under admExpences
  get admExpenceFormGroup() {
    return this.proposalFormBudget.get('admExpences') as FormArray;
  }

  // returns all form groups under directExpences
  get directExpenceFormGroup() {
    return this.proposalFormBudget.get('directExpences') as FormArray;
  }

  ageGroupArr: any = [];
  populationArr: any = [];
  filesToUpload: Array < File > = [];
  proposalId: boolean = false;
  typeEvaluationsArr: any = [];
  typeEvaluationsResArr: any = [];
  typeevaluationsArr: any = []
  legalArr: any = [];
  public grant: boolean = false;
  language: any;
  proposal_id = localStorage.getItem('proposal_id')
  constructor(
    elementRef: ElementRef,
    private fb: FormBuilder,
    private authProvider: AuthService,
    public router: Router,
    private route: ActivatedRoute,
    private toast: ToastProvider,
    private consoleProvider: ConsoleProvider,
    private UserMasterApi: UserMasterService,
    private userStorage: UserStorageProvider,
    private proposalApi: ProposalService,
    public dialog: MatDialog,
    private datePipe: DatePipe,
    private ngxService:NgxUiLoaderService,
    private common: CommonService,
    private translate: TranslateService,
    private AuditTrailApi: AuditTrailService,
  ) {
    this.ngxService.start();
    this.Projectlisting = [];
    this.Pareotypelisting = [] ;
    this.Pareocategorylisting = [];
    this.Citylisting = [];
    this.proposal = [];
    this.getProjectList();
    this.getCityList();
    this.getPareotypeList();
    this.getAdminUserList();
    this.getMemberList();
    if (localStorage.getItem('proposal_id')) {
      this.grant = true;
    }
    this.loggedUserType = this.userStorage.get().user.user_type;
    this.loggedUserRole = this.userStorage.get().user.role.roleKey;
    this.fileBaseUrl = AppConst.FILE_BASE_URL;
    this.apiBaseUrl = AppConst.API_BASE_URL;
    this.templateUrl = AppConst.TEMPLATE_DOWNLOAD_URL;
    this.today = this.datePipe.transform(new Date(),"MM/dd/yyyy");
  }

  ngOnInit() {
    this.common.currLang.subscribe(
      res=>{
        this.language = res
      }
    )
    
    const emailRegx = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    /** First Step form initialize */
    this.proposalForm_1 = this.fb.group({
      project_id: ['', Validators.required],
      city: ['', Validators.required],
      categories: ['', Validators.required],
      community: ['', Validators.required],
      necessity_statement: ['', Validators.required],
      programmatic_approach: ['', Validators.required],
      user_id: [this.userStorage.get().user.id, ''],
      population_status: ['', ''],
      necessity_status: ['', ''],
      programmatic_status: ['', ''],
      pareo_type: ['', Validators.required],
      pareo_origin: ['', Validators.required],
      pareo_category: ['', Validators.required],
      pareoCal: ['', ''],
      pareo_approved_grant: ['', '']
    });

    if (!localStorage.getItem('proposal_id')) {
      this.ageGroupArr = [{
          id: 1,
          value: '0-3 Infants',
          checked: false
        },
        {
          id: 2,
          value: '4-6 Preschool',
          checked: false
        },
        {
          id: 3,
          value: '7-10 Children',
          checked: false
        },
        {
          id: 4,
          value: '11-14 Teens',
          checked: false
        },
        {
          id: 5,
          value: '15-18 Young',
          checked: false
        },
        {
          id: 6,
          value: '19-21 Young Adult',
          checked: false
        },
        {
          id: 7,
          value: '22-59 Adults',
          checked: false
        },
        {
          id: 8,
          value: '60 Older Adult',
          checked: false
        }
      ]
      for (let item of this.ageGroupArr) {
        this.proposalForm_1.addControl('ageGroup_' + item.id, new FormControl());
      }

      for (var i = 0; i < this.ageGroupArr.length; i++) {
        //console.log('ageGroup_' + i);
        //population: this.fb.array([])
      }
    }

    this.proposalForm_2 = this.fb.group({
      goal_to_achieve: ['', Validators.required],
      changes_or_benefits: ['', Validators.required],
      specific_activities: ['', Validators.required],
      who_will_affected: ['', Validators.required],
      how_will_pay: ['', Validators.required],
      resources_required: ['', Validators.required],

      goal_status: ['', ''],
      changes_status: ['', ''],
      specific_status: ['', ''],
      affected_status: ['', ''],
      pay_status: ['', ''],
      resources_status: ['', ''],
      work_plan_status: ['', ''],
    });

    this.proposalForm_3 = this.fb.group({
      collecting_relevant_data: ['', Validators.required],
      how_often_organization_evaluates: ['', Validators.required],
      who_evaluates: ['', Validators.required],
      statements_describes_type_of_evaluations: this.fb.array([]),
      type_of_evaluation: ['', Validators.required],
      type_of_data: ['', Validators.required],
      type_of_evaluator: ['', Validators.required],
      step: [3, ''],

      collecting_status: ['', ''],
      who_evaluates_status: ['', ''],
      organization_evaluates_status: ['', ''],
      statements_describes_type_of_evaluations_status: ['', ''],
      type_of_evaluation_status: ['', ''],
      type_of_data_status: ['', ''],
      type_of_evaluator_status: ['', ''],
    });

    this.proposalFormReq = this.fb.group({
      department: ['', ''],
      desc: ['', ''],
      requirement_file_status: ['', ''],
    });

    this.proposalFormLegal = this.fb.group({
      legal: ['', []],
      legal_status: ['', ''],
    });

    this.proposalFormProp = this.fb.group({
      inventory_file_status: ['', ''],
      inventory_of_property: ['', ''],
    });

    /* this.proposalFormBudget = this.fb.group({
      budget: ['', ''],
      budget_file_status: ['', ''],
    }); */
    
    this.proposalFormBudget = this.fb.group({
      administrative_budget_file_status: ['', ''],
      direct_budget_file_status: ['', ''],
      admExpences: this.fb.array([]),
      directExpences: this.fb.array([])
    });

    this.proposalFormEvidence = this.fb.group({
      descEvidence: ['', ''],
      evidence_doc_status: ['', ''],
    });

    // set admExpencelist to this field
    //this.admExpenceList = this.proposalFormBudget.get('admExpences') as FormArray;

    // set directExpenceList to this field
    //this.directExpenceList = this.proposalFormBudget.get('directExpences') as FormArray;
    
    this.proposalFormSubmit = this.fb.group({
      name: ['', Validators.required],
      proposal_member: [''],
      date: [ new Date(), Validators.required],
      proposalAgree: ['', Validators.required],
      application_status: ['']
    });

    this.proposalFormAction = this.fb.group({
      open_status: [''],
      entityName: [''],
      proposalUniqueId: [''],
      applicationDate: [''],
      assigned_user: [''],
      application_status: [''],
      requested_grant: ['', Validators.required],
      donation_grant: [''],
      approved_grant: [''],
      notifyUser: ['']
    });
  }

  ngAfterViewInit(){
    if (localStorage.getItem('proposal_id')) {
      this.getProposal();
    }
    this.ngxService.stop();
  }

  // convenience getter for easy access to form fields
  get f1() { return this.proposalForm_1.controls; };
  get f2() { return this.proposalForm_2.controls; };
  get f3() { return this.proposalForm_3.controls; };
  get fProp() { return this.proposalFormProp.controls; };
  get fBudget() { return this.proposalFormBudget.controls; };
  get fReq() { return this.proposalFormReq.controls; };
  get fEvidence() { return this.proposalFormEvidence.controls; };
  get f7() { return this.proposalFormSubmit.controls; };

  get fAction() { return this.proposalFormAction.controls; };

/* ########################################################################################### */
  // Administrative budget formgroup
  createAdmExpance(): FormGroup {
    return this.fb.group({
      description: [null, Validators.compose([Validators.required])],
      budget_amount: [null, Validators.compose([Validators.required])],
      tight_budget: [null],
      budget_type:[null],
      id:[null],
      proposal_id:[null],
      created_at:[null],
      updated_at:[null]
    });
  }

  // Direct budget formgroup
  createDirectExpance(): FormGroup {
    return this.fb.group({
      description: [null, Validators.compose([Validators.required])],
      budget_amount: [null, Validators.compose([Validators.required])],
      tight_budget: [null],
      budget_type:[null],
      id:[null],
      proposal_id:[null],
      created_at:[null],
      updated_at:[null]
    });
  }

  // add an Administrative budget form group
  addAdmExpance() {
    this.admExpenceFormGroup.push(this.createAdmExpance());
  }

  // add an Direct budget form group
  addDirectExpance() {
    this.directExpenceFormGroup.push(this.createDirectExpance());
  }

  // remove Administrative budget from group
  removeAdmExpance(index,id?) {
    //console.log(id)
    var adminExpance = this.budgetBreakdown.filter(res=>res.id == id)
    if(id > 0){
      this.proposalApi.deleteExpance(id).subscribe(
        res=>{
          if(res.success){
            this.toast.success(res.message)
          }
  
        },
        error=>{
          this.toast.error(error.message)
        }
      )
    }
    this.admExpenceFormGroup.removeAt(index);
    this.calculateAdministrativeBudget();
  }

  // remove Administrative budget from group
  removeDirectExpance(index,id?) {
    var directExpance = this.budgetBreakdown.filter(res=>res.id == id)
    if(id > 0){
      this.proposalApi.deleteExpance(id).subscribe(
        res=>{
          if(res.success){
            this.toast.success(res.message)
          }
  
        },
        error=>{
          this.toast.error(error.message)
        }
      )
    }
    
    this.directExpenceFormGroup.removeAt(index);
    this.calculateDirectBudget();
  }

  // get the formgroup under admExpences form array
/*   getAdmExpenceFormGroup(index): FormGroup {
    const formGroup = this.admExpenceList.controls[index] as FormGroup;
    return formGroup;
  } */

/* ########################################################################################### */

  onSelectDepartment(v) {
    this.selectedDepartment = v;
  }

  onSelectEvidence(v) {
    this.selectedEvidence = v;
  }

  onSelectPA(v) {
    this.selectedPA = v;
  }

  onSelectDocumentType(v) {
    this.selectedDA = v;
  }

  getAdminUserList() {
    this.UserMasterApi.getAssignedUser().subscribe((res) => {
      this.adminUserlisting = res.data;
    }, (err) => {});
  }

  getMemberList() {
    this.UserMasterApi.memberList(this.userStorage.get().user.id).subscribe((res: any) => {
      this.memberlisting = res.members;
    }, (err) => {});
  }
  
  getProjectList(){
    this.UserMasterApi.projectList().subscribe((res: any) => {
      if (res.success) {
        this.Projectlisting = res.data;
      }
    }, (err) => {});
  }

  getCityList() {
    this.searchParam = {
      search: 'PR',
    };
    this.UserMasterApi.citySearch(this.searchParam).subscribe((res: any) => {
      if (res.success) {
        this.Citylisting = res.data;
      }
    }, (err) => {});
  }

  getPareotypeList(){
    this.UserMasterApi.pareotypeList().subscribe((res: any) => {
      if (res.success) {
        this.Pareotypelisting = res.data;
      }
    }, (err) => {});
  }

  
  getProposal() {
    this.proposalId = true
    this.proposalApi.viewProposal().subscribe((res: any) => {
      if (res.success) {
        var adminExpance = []
        var directExpences = []
        this.proposalFormBudget = this.fb.group({
          administrative_budget_file_status: ['', ''],
          direct_budget_file_status: ['', ''],
          admExpences: this.fb.array([]),
          directExpences: this.fb.array([])
        });
        this.proposal = res.data;
        this.ageGroupArr = JSON.parse(this.proposal.population)
        this.budgetBreakdown = this.proposal.budget_breakdown

        if(this.budgetBreakdown.length > 0){
          
          this.adminExpanceId = [];
          this.directExpanceId = [];
          this.total_adm_budget_amount = 0;
          this.total_direct_budget_amount = 0;
          this.total_adm_tight_budget = 0;
          this.total_direct_tight_budget = 0;
          for(let item of this.budgetBreakdown){
            if(item.budget_type == 'Administrative'){
              this.adminExpanceId.push(item.id)
              this.admExpenceFormGroup.push(this.createAdmExpance());
              adminExpance.push(item);
              this.total_adm_budget_amount = this.total_adm_budget_amount + item.budget_amount;
              this.total_adm_tight_budget = this.total_adm_tight_budget + item.tight_budget;
            }
            if(item.budget_type == 'Direct'){
              this.directExpanceId.push(item.id)
              this.directExpenceFormGroup.push(this.createDirectExpance());
              directExpences.push(item);
              this.total_direct_budget_amount = this.total_direct_budget_amount + item.budget_amount;
              this.total_direct_tight_budget = this.total_direct_tight_budget + item.tight_budget;
            }
    
          }
          this.proposalFormBudget.patchValue({
            admExpences:adminExpance,
            directExpences:directExpences
          })
          
        }

        // step 1
        this.proposalForm_1.controls["project_id"].setValue(this.proposal.project_id);
        this.proposalForm_1.controls["city"].setValue(this.proposal.city);
        this.proposalForm_1.controls["categories"].setValue(this.proposal.categories);
        this.proposalForm_1.controls["community"].setValue(this.proposal.community);
        this.proposalForm_1.controls["necessity_statement"].setValue(this.proposal.necessity_statement);
        this.proposalForm_1.controls["programmatic_approach"].setValue(this.proposal.programmatic_approach);

        for (let item of JSON.parse(this.proposal.population)) {
          this.proposalForm_1.addControl('ageGroup_' + item.id, new FormControl(item.population));
        }
        
        this.proposalForm_1.controls["population_status"].setValue(this.proposal.population_status);
        this.proposalForm_1.controls["necessity_status"].setValue(this.proposal.necessity_status);
        this.proposalForm_1.controls["programmatic_status"].setValue(this.proposal.programmatic_status);

        //pareo calculation
        this.pareoCalculation = (this.proposal.requested_grant * 20 )/100;
        this.onSelectPAP(this.proposal.pareo_type);
        this.proposalForm_1.controls["pareo_type"].setValue(this.proposal.pareo_type);
        this.proposalForm_1.controls["pareo_origin"].setValue(this.proposal.pareo_origin);
        this.proposalForm_1.controls["pareo_category"].setValue(this.proposal.pareo_category);

        this.proposalForm_1.controls["pareoCal"].setValue(this.pareoCalculation);
        this.proposalForm_1.controls["pareo_approved_grant"].setValue(this.proposal.approved_grant);
        
        // step 2
        this.proposalForm_2.controls["goal_to_achieve"].setValue(this.proposal.goal_to_achieve);
        this.proposalForm_2.controls["changes_or_benefits"].setValue(this.proposal.changes_or_benefits);
        this.proposalForm_2.controls["specific_activities"].setValue(this.proposal.specific_activities);
        this.proposalForm_2.controls["who_will_affected"].setValue(this.proposal.who_will_affected);
        this.proposalForm_2.controls["how_will_pay"].setValue(this.proposal.how_will_pay);
        this.proposalForm_2.controls["resources_required"].setValue(this.proposal.resources_required);

        this.proposalForm_2.controls["goal_status"].setValue(this.proposal.goal_status);
        this.proposalForm_2.controls["changes_status"].setValue(this.proposal.changes_status);
        this.proposalForm_2.controls["specific_status"].setValue(this.proposal.specific_status);
        this.proposalForm_2.controls["affected_status"].setValue(this.proposal.affected_status);
        this.proposalForm_2.controls["pay_status"].setValue(this.proposal.pay_status);
        this.proposalForm_2.controls["resources_status"].setValue(this.proposal.resources_status);
        this.proposalForm_2.controls["work_plan_status"].setValue(this.proposal.work_plan_status);

        // step 3
        this.proposalForm_3.controls["collecting_relevant_data"].setValue(this.proposal.collecting_relevant_data);
        this.proposalForm_3.controls["who_evaluates"].setValue(this.proposal.who_evaluates);
        this.proposalForm_3.controls["how_often_organization_evaluates"].setValue(this.proposal.how_often_organization_evaluates);
        this.proposalForm_3.controls["type_of_evaluation"].setValue(this.proposal.type_of_evaluation);
        this.proposalForm_3.controls["type_of_data"].setValue(this.proposal.type_of_data);
        this.proposalForm_3.controls["type_of_evaluator"].setValue(this.proposal.type_of_evaluator);

        this.typeEvaluationsArr = JSON.parse(this.proposal.statements_describes_type_of_evaluations);
        if (this.typeEvaluationsArr) {
          for (let item of this.typeEvaluationsArr) {
            this.proposalForm_3.addControl('typeEvaluation_' + item.id, new FormControl(item.typeevaluation));
          }
        } else {
          this.typeEvaluationsArr = [{
              id: 1,
              value: 'Rarely use the evaluation data',
              checked: false,
              label: 'evaluation1'
            },
            {
              id: 2,
              value: 'Changes are made to programs and services based on data evaluation',
              checked: false,
              label: 'evaluation2'
            },
            {
              id: 3,
              value: 'Share evaluation data with the media',
              checked: false,
              label: 'evaluation3'
            },
            {
              id: 4,
              value: 'Share evaluation data with policy designers',
              checked: false,
              label: 'evaluation4'
            },
            {
              id: 5,
              value: 'Share evaluation data with community representatives',
              checked: false,
              label: 'evaluation5'
            },
            {
              id: 6,
              value: 'Share evaluation data with financing sources',
              checked: false,
              label: 'evaluation6'
            },
            {
              id: 7,
              value: 'Share evaluation data with central office',
              checked: false,
              label: 'evaluation7'
            },
            {
              id: 8,
              value: 'Share evaluation data with others not listed above',
              checked: false,
              label: 'evaluation8'
            },
            {
              id: 9,
              value: 'Use evaluation data to measure performance of team / stakeholders',
              checked: false,
              label: 'evaluation9'
            }
          ]
          for (let item of this.typeEvaluationsArr) {
            this.proposalForm_3.addControl('typeEvaluation_' + item.id, new FormControl());
          }
        }
        this.proposalForm_3.controls["collecting_status"].setValue(this.proposal.collecting_status);
        this.proposalForm_3.controls["who_evaluates_status"].setValue(this.proposal.who_evaluates_status);
        this.proposalForm_3.controls["organization_evaluates_status"].setValue(this.proposal.organization_evaluates_status);
        this.proposalForm_3.controls["statements_describes_type_of_evaluations_status"].setValue(this.proposal.statements_describes_type_of_evaluations_status);
        this.proposalForm_3.controls["type_of_evaluation_status"].setValue(this.proposal.type_of_evaluation_status);
        this.proposalForm_3.controls["type_of_data_status"].setValue(this.proposal.type_of_data_status);
        this.proposalForm_3.controls["type_of_evaluator_status"].setValue(this.proposal.type_of_evaluator_status);

        

        // step 5
        this.proposalFormProp.controls["inventory_file_status"].setValue(this.proposal.inventory_file_status);

        // step 6
        this.proposalFormBudget.controls["administrative_budget_file_status"].setValue(this.proposal.administrative_budget_file_status);
        this.proposalFormBudget.controls["direct_budget_file_status"].setValue(this.proposal.direct_budget_file_status);

        // step legal
        this.proposalFormLegal.controls["legal_status"].setValue(this.proposal.legal_status);
        this.legalArr = JSON.parse(this.proposal.legal);
        if (this.legalArr) {
          for (let item of this.legalArr) {
            this.proposalFormLegal.addControl('legal_' + item.id, new FormControl(item.status));
          }
        } else {

          this.legalArr = [{
              id: 1,
              value: 'legal1',
              checked: false,
              label: 'legal1'
            },
            {
              id: 2,
              value: 'legal2',
              checked: false,
              label: 'legal2'
            },
            {
              id: 3,
              value: 'legal3',
              checked: false,
              label: 'legal3'
            },
            {
              id: 4,
              value: 'legal4',
              checked: false,
              label: 'legal4'
            },
            {
              id: 5,
              value: 'legal5',
              checked: false,
              label: 'legal5'
            },
            {
              id: 6,
              value: 'legal6',
              checked: false,
              label: 'legal6'
            },
            {
              id: 7,
              value: 'legal7',
              checked: false,
              label: 'legal7'
            },
            {
              id: 8,
              value: 'legal8',
              checked: false,
              label: 'legal8'
            },
            {
              id: 9,
              value: 'legal9',
              checked: false,
              label: 'legal9'
            }
          ]
          for (let item of this.legalArr) {
            this.proposalFormLegal.addControl('legal_' + item.id, new FormControl());
          }
        }

        // step Req
        this.proposalFormReq.controls["requirement_file_status"].setValue(this.proposal.requirement_file_status);

        // step Evidence
        this.proposalFormEvidence.controls["evidence_doc_status"].setValue(this.proposal.evidence_doc_status);

        // step 9
        this.proposalFormSubmit.controls["name"].setValue(this.proposal.name);
        this.proposalFormSubmit.controls["proposal_member"].setValue(this.proposal.proposal_member ? this.proposal.proposal_member.member_name + ' - ' + this.proposal.proposal_member.designation : '');
        this.proposalFormSubmit.controls["date"].setValue(this.proposal.date ? this.proposal.date : new Date());
        this.proposalFormSubmit.controls["proposalAgree"].setValue(this.proposal.proposalAgree);

        this.proposalFormAction.controls["entityName"].setValue(this.loggedUserType === 1 ? this.userStorage.get().user.entity_name : this.proposal.proposal_user.entity_name);
        this.proposalFormAction.controls["applicationDate"].setValue(this.proposal.date ? this.datePipe.transform(this.proposal.date,"MM/dd/yyyy") : this.today);
        this.proposalFormAction.controls["proposalUniqueId"].setValue(this.proposal.proposal_unique_id);
        this.proposalFormAction.controls["open_status"].setValue(this.proposal.open_status);
        if(this.proposal.assign_user){
            this.proposalFormAction.controls["assigned_user"].setValue(this.proposal.assign_user.user_id);
        }
        this.proposalFormAction.controls["requested_grant"].setValue(this.proposal.requested_grant);
        this.proposalFormAction.controls["donation_grant"].setValue(this.proposal.donation_grant);
        this.proposalFormAction.controls["approved_grant"].setValue(this.proposal.approved_grant);

        this.proposalFormAction.controls["notifyUser"].setValue(this.proposal.notifyUser === 1 ? true : false);


        if((this.proposal.requested_grant != this.proposal.approved_grant) && this.proposal.approved_grant>0 && this.proposal.approved_grant != null){
            this.administrativeExpenses = (this.proposal.approved_grant * 40 )/100;
            this.directExpenses = (this.proposal.approved_grant * 60 )/100;
        }else{
            this.administrativeExpenses = (this.proposal.requested_grant * 40 )/100;
            this.directExpenses = (this.proposal.requested_grant * 60 )/100;
        }
        
        if(this.loggedUserType == 1 && this.proposal.approved_grant>0){
            this.approveGrantStatus = true;
        }
        
        

        // Tight Budget status checking
        if((this.proposal.requested_grant != this.proposal.approved_grant) && this.proposal.approved_grant>0 && (this.proposal.application_status ==1 || this.proposal.application_status ==2 || this.proposal.application_status ==5)){
            if(this.proposal.application_status != 5){
              this.show1 = false;
              this.show2 = false;
              this.show3 = false;
              this.showReq = false;
              this.showProp = false;
              this.showBudget = true;
              this.showSubmit = false;

              this.active1 = false;
              this.active2 = false;
              this.active3 = false;
              this.activeReq = false;
              this.activeProp = false;
              this.activeBudget = true;
              this.activeSubmit = false;
            }

              this.tightBudgetStatus = true;
              this.budgetPrevStatus = false;
        }


        // populate proposal_documents key
        this.upDocKey = [];
        this.upEvidenceKey = [];

        for (let item of this.proposal.proposal_documents) {
          //this.upDocKey.push(item.document_type);

          this.upDocKey.push(item.desc);

          if(item.document_type == 'evidence_doc_file'){
            this.upEvidenceKey.push(item.desc);
          }
        }
        this.upDocKey = [...new Set(this.upDocKey)]
        this.upEvidenceKey = [...new Set(this.upEvidenceKey)]
      }
    }, (err) => {});
  }

  fileChangeEventXX(fileInput: any) {
    this.filesToUpload = < Array < File >> fileInput.target.files;
    //console.log(fileInput.target.name)
    this.urls = [];
    let files = fileInput.target.files;
    if (files) {
      for (let file of files) {
        //console.log(file.name)
        let reader = new FileReader();
        reader.onload = (e: any) => {
          this.urls.push(file.name);
        }
        reader.readAsDataURL(file);
      }
    }
    //console.log(this.urls)
  }

  fileChangeEvent_(event: any) {
    const files = event.target.files;
    //console.log('files=>', files);
    if (files) {
      for (let i = 0; i < files.length; i++) {
        const fileObj = {
          name: '',
          type: '',
          url: ''
        }
        this.allfiles.push(files[i]);
        fileObj.name = files[i].name;
        fileObj.type = files[i].type;

        const reader = new FileReader();
        reader.onload = (filedata) => {
          fileObj.url = reader.result + '';
          this.upFiles.push(fileObj);
        }
        reader.readAsDataURL(files[i]);
      }
    }
    event.srcElement.value = null;

    //console.log('upFiles=>', this.upFiles);
    //console.log('allfiles=>', this.allfiles);
  }

  removefile_(file: any) {
    const index = this.upFiles.indexOf(file);
    this.upFiles.splice(index, 1);
    this.allfiles.splice(index, 1);

    //console.log('upFiles=>', this.upFiles);
    //console.log('allfiles=>', this.allfiles);
  }

  fileChangeEvent(event: any) {
    const files = event.target.files;
    const nameObj = event.target.name;
    if (files) {
      for (let i = 0; i < files.length; i++) {
        const fileObj = {
          name: '',
          type: '',
          url: ''
        }
        this.allfiles[nameObj].push(files[i]);
        fileObj.name = files[i].name;
        fileObj.type = files[i].type;

        const reader = new FileReader();
        reader.onload = (filedata) => {
          fileObj.url = reader.result + '';
          this.upFiles[nameObj].push(fileObj);
        }
        reader.readAsDataURL(files[i]);
      }
    }
    event.srcElement.value = null;
  }

  removefile(file: any, nameObj: any) {
    const index = this.upFiles[nameObj].indexOf(file);
    this.upFiles[nameObj].splice(index, 1);
    this.allfiles[nameObj].splice(index, 1);
  }

  // onAgeSelect(value: string, isChecked: boolean) {
  //     const valueFormArray = <FormArray>this.proposalForm_1.controls.population;

  //     if (isChecked) {
  //       valueFormArray.push(new FormControl(value));
  //     } else {
  //       let index = valueFormArray.controls.findIndex(x => x.value == value)
  //       valueFormArray.removeAt(index);
  //     }
  //     console.log(valueFormArray);
  //   }

  onEvaluationSelect(value: string, isChecked: boolean) {
    if (isChecked == true) {
        for (var i = 0; i < this.typeEvaluationsArr.length; i++) {
            var evaluationValue = this.typeEvaluationsArr[i].value;
            if (evaluationValue == value) {
                this.typeEvaluationsArr[i]['checked'] = true
            }
        }
    } else {
        for (var i = 0; i < this.typeEvaluationsArr.length; i++) {
            var evaluationValue = this.typeEvaluationsArr[i].value;
            if (evaluationValue == value) {
                this.typeEvaluationsArr[i]['checked'] = false
            }
        }
    }
  }

  onLegalSelect(value: string, isChecked: boolean) {
    if (isChecked == true) {
      for (var i = 0; i < this.legalArr.length; i++) {
          var legalValue = this.legalArr[i].value;
          if (legalValue == value) {
              this.legalArr[i]['checked'] = true
          }
      }
    } else {
      for (var i = 0; i < this.legalArr.length; i++) {
          var legalValue = this.legalArr[i].value;
          if (legalValue == value) {
              this.legalArr[i]['checked'] = false
          }
      }
    }
  }

  delFile(fileInfo :any) {
      this.proposalApi.delFile(fileInfo).subscribe((response: any) => {
        if (response.success) {
          this.getProposal();
          this.toast.success(this.translate.instant('MESSAGE.FILE_REMOVE'));
        }
      }, (errorData: any) => {});
  }
  /**
   * First Step Submit
   */
  toggle1(value) {
      var param = [];
      this.f1submitted = true;
      
      if (this.proposalForm_1.invalid) { this.toast.error(this.translate.instant('MESSAGE.REQUIRED_MESSAGE')); return; }
       const formData: any = new FormData();

      /*
      const fileBrowser = this.fileInput.nativeElement;
      if (fileBrowser.files && fileBrowser.files[0]) {
        formData.append('up_file', fileBrowser.files[0]);
      }*/

      //const files: Array<File> = this.allfiles;
      /*const files: Array<File> = this.allfiles.necessity_statement_file[0];
      console.log(files);
      if(files.length > 0){
        for(let i =0; i < files.length; i++){
          //console.log(files[i]);
          formData.append("necessity_statement_file[]", files[i], files[i]['name']);
        }
      }*/

      if(this.allfiles.necessity_statement_file.length > 0){
        const files: Array<File> = this.allfiles.necessity_statement_file;
        
        if(files.length > 0){
          for(let i =0; i < files.length; i++){
            //console.log(files[i]);
            formData.append("necessity_statement_file[]", files[i], files[i]['name']);
          }
        }
      }
      for(let item of this.ageGroupArr){
          param.push(
              {
                  population : value['ageGroup_'+item.id],
                  id:item.id,
                  value:item.value
              }
          )
      }
      
      formData.append('project_id', this.proposalForm_1.value.project_id);
      formData.append('city', this.proposalForm_1.value.city);
      //formData.append('town', this.proposalForm_1.value.town);
      //formData.append('population', JSON.stringify(this.proposalForm_1.value.population));
      formData.append('population', JSON.stringify(param));
      formData.append('categories', this.proposalForm_1.value.categories);
      formData.append('community', this.proposalForm_1.value.community);
      formData.append('necessity_statement', this.proposalForm_1.value.necessity_statement);
      formData.append('programmatic_approach', this.proposalForm_1.value.programmatic_approach);
      formData.append('user_id', this.userStorage.get().user.id);
      formData.append('step', 1);

      formData.append('population_status', this.proposalForm_1.value.population_status);
      formData.append('necessity_status', this.proposalForm_1.value.necessity_status);
      formData.append('programmatic_status', this.proposalForm_1.value.programmatic_status);
      formData.append('pareo_type', this.proposalForm_1.value.pareo_type);
      formData.append('pareo_origin', this.proposalForm_1.value.pareo_origin);
      formData.append('pareo_category', this.proposalForm_1.value.pareo_category);
      

      this.ngxService.start();
      this.proposalApi.addProposal(formData).subscribe((response: any) => {
          if (response.success) {
            this.ngxService.stop();
              localStorage.setItem('stepCompleted', '1');

              if (!localStorage.getItem('proposal_id')) {
                  localStorage.setItem('proposal_id', response.data);
                  this.grant = true;
              }
              this.upFiles['necessity_statement_file'] = [];
              this.allfiles['necessity_statement_file'] = [];
              this.getProposal();

              this.clearAll();
              this.show1 = false;
              this.show2 = true;
              this.show3 = false;
              this.showProp = false;
              this.showBudget = false;
              this.showLegal = false;
              this.showReq = false;
              this.showSubmit = false;

              this.active1 = false;
              this.active2 = true;
              this.active3 = false;
              this.activeProp = false;
              this.activeBudget = false;
              this.activeLegal = false;
              this.activeReq = false;
              this.activeSubmit = false;

              this.gotoTop();
          }
      }, error => {
          this.ngxService.stop();
          this.f1submitted = false;
      }
    );
  }

  toggle2() {
    this.f2submitted = true;
    if (this.proposalForm_2.invalid) { this.toast.error(this.translate.instant('MESSAGE.REQUIRED_MESSAGE')); return; }

    const formData: any = new FormData();
    
    if(this.allfiles.work_plan_file.length > 0){
      const files: Array<File> = this.allfiles.work_plan_file;
      
      if(files.length > 0){
        for(let i =0; i < files.length; i++){
          formData.append("work_plan_file[]", files[i], files[i]['name']);
        }
      }
    }

    formData.append('goal_to_achieve',this.proposalForm_2.value.goal_to_achieve);
    formData.append('changes_or_benefits',this.proposalForm_2.value.changes_or_benefits);
    formData.append('specific_activities',this.proposalForm_2.value.specific_activities);
    formData.append('who_will_affected',this.proposalForm_2.value.who_will_affected);
    formData.append('how_will_pay',this.proposalForm_2.value.how_will_pay);
    formData.append('resources_required',this.proposalForm_2.value.resources_required);
    formData.append('step',2);

    formData.append('goal_status', this.proposalForm_2.value.goal_status);
    formData.append('changes_status', this.proposalForm_2.value.changes_status);
    formData.append('specific_status', this.proposalForm_2.value.specific_status);
    formData.append('affected_status', this.proposalForm_2.value.affected_status);
    formData.append('pay_status', this.proposalForm_2.value.pay_status);
    formData.append('resources_status', this.proposalForm_2.value.resources_status);
    formData.append('work_plan_status', this.proposalForm_2.value.work_plan_status);

    this.ngxService.start();
    this.proposalApi.editProposal(formData).subscribe((response: any) => {
          
          if (response.success) {
              this.ngxService.stop();
              localStorage.setItem('stepCompleted', '2');
              this.upFiles['work_plan_file'] = [];
              this.allfiles['work_plan_file'] = [];
              this.getProposal();
              this.clearAll();

              this.show1 = false;
              this.show2 = false;
              this.show3 = true;
              this.showProp = false;
              this.showBudget = false;
              this.showLegal = false;
              this.showReq = false;
              this.showSubmit = false;

              this.active1 = false;
              this.active2 = false;
              this.active3 = true;
              this.activeProp = false;
              this.activeBudget = false;
              this.activeLegal = false;
              this.activeReq = false;
              this.activeSubmit = false;
              this.gotoTop();
          }        
      }, error => {
          this.ngxService.stop();
          this.f2submitted = false;
          console.log('Proposal Create error: ', error);
      }
    );  
  }

  toggle3(value) {
      
      this.f3submitted = true;

      if (this.proposalForm_3.invalid) { this.toast.error(this.translate.instant('MESSAGE.REQUIRED_MESSAGE')); return; }

      const formData: any = new FormData();
      var param = [];

      for(let item of this.typeEvaluationsArr){
          param.push(
              {
                  typeevaluation : value['typeEvaluation_'+item.id],
                  id:item.id,
                  value:item.value,
                  label:item.label
              }
          )
      }

      formData.append('collecting_relevant_data', this.proposalForm_3.value.collecting_relevant_data);
      formData.append('how_often_organization_evaluates', this.proposalForm_3.value.how_often_organization_evaluates);
      formData.append('who_evaluates', this.proposalForm_3.value.who_evaluates);
      formData.append('statements_describes_type_of_evaluations', JSON.stringify(param));
      formData.append('type_of_evaluation', this.proposalForm_3.value.type_of_evaluation);
      formData.append('type_of_data', this.proposalForm_3.value.type_of_data);
      formData.append('type_of_evaluator', this.proposalForm_3.value.type_of_evaluator);
      formData.append('step', 3);

      formData.append('collecting_status',this.proposalForm_3.value.collecting_status);
      formData.append('who_evaluates_status',this.proposalForm_3.value.who_evaluates_status);
      formData.append('organization_evaluates_status',this.proposalForm_3.value.organization_evaluates_status);
      formData.append('statements_describes_type_of_evaluations_status',this.proposalForm_3.value.statements_describes_type_of_evaluations_status);
      formData.append('type_of_evaluation_status',this.proposalForm_3.value.type_of_evaluation_status);
      formData.append('type_of_data_status',this.proposalForm_3.value.type_of_data_status);
      formData.append('type_of_evaluator_status',this.proposalForm_3.value.type_of_evaluator_status);


      this.ngxService.start();
      this.proposalApi.editProposal(formData).subscribe((response: any) => {
         
          if (response.success) {
              this.ngxService.stop();
              localStorage.setItem('stepCompleted', '3');
              this.show1 = false;
              this.show2 = false;
              this.show3 = false;
              this.showProp = true;
              this.showBudget = false;
              this.showLegal = false;
              this.showReq = false;
              this.showSubmit = false;

              this.active1 = false;
              this.active2 = false;
              this.active3 = false;
              this.activeProp = true;
              this.activeBudget = false;
              this.activeLegal = false;
              this.activeReq = false;
              this.activeSubmit = false;
              this.gotoTop();
          }
      }, error => {
          this.ngxService.stop();
          this.f3submitted = false;
      }
      );
  }

  uploadInventoryFile() {
    this.fPropsubmitted = true;
    if (this.proposalFormProp.invalid) { this.toast.error(this.translate.instant('MESSAGE.REQUIRED_MESSAGE')); return; }

    const formData: any = new FormData();

    if(this.allfiles.property_inventory_file.length > 0 ){
      const files: Array<File> = this.allfiles.property_inventory_file;
      if(files.length > 0){
        for(let i =0; i < files.length; i++){
          formData.append("property_inventory_file[]", files[i], files[i]['name']);
        }
      }
    }else{
      this.toast.error(this.translate.instant('MESSAGE.FILE_ATTACH')); return; 
    }

    formData.append('inventory_of_property',this.proposalFormProp.value.inventory_of_property);
    formData.append('inventory_file_status',this.proposalFormProp.value.inventory_file_status);
    formData.append('step',4);

    this.proposalApi.editProposal(formData).subscribe((response: any) => {
          if (response.success) {
              this.getProposal();
              this.proposalFormProp.controls["inventory_of_property"].setValue('');
              this.upFiles['property_inventory_file'] = [];
              this.allfiles['property_inventory_file'] = [];
          }        
      }, error => {
          this.fPropsubmitted = false;
      }
    );    
  }

  uploadEvidenceFile() {
    this.loadingFile = true;
    this.fEvidencesubmitted = true;
    //if (this.proposalFormEvidence.invalid) { this.toast.error(this.translate.instant('MESSAGE.REQUIRED_MESSAGE')); return; }
    if(this.proposalFormEvidence.value.descEvidence == ''){ 
      this.loadingFile = false;
      this.toast.error(this.translate.instant('PROPOSAL.SELECT_EVIDENCE_ERR')); 
      return; 
    }

    const formData: any = new FormData();

    if(this.allfiles.evidence_doc_file.length > 0 ){
      const files: Array<File> = this.allfiles.evidence_doc_file;
      if(files.length > 0){
        for(let i =0; i < files.length; i++){
          formData.append("evidence_doc_file[]", files[i], files[i]['name']);
        }
      }
    }else{
      this.loadingFile = false;
      this.toast.error(this.translate.instant('MESSAGE.FILE_ATTACH')); return; 
    }

    formData.append('descEvidence',this.proposalFormEvidence.value.descEvidence);
    formData.append('evidence_doc_status',this.proposalFormEvidence.value.evidence_doc_status);
    formData.append('step',8);

    this.proposalApi.editProposal(formData).subscribe((response: any) => {
          if (response.success) {
              this.proposalFormEvidence.controls["descEvidence"].setValue('');
              this.upFiles['evidence_doc_file'] = [];
              this.allfiles['evidence_doc_file'] = [];
              this.getProposal();
              this.loadingFile = false;
          }        
      }, error => {
          this.fEvidencesubmitted = false;
      }
    );    
  }

  uploadBudgetFile() {
    
    this.fBudgetsubmitted = true;
    if (this.proposalFormBudget.invalid) { this.toast.error(this.translate.instant('MESSAGE.REQUIRED_MESSAGE')); return; }

    const formData: any = new FormData();

    if(this.allfiles.adm_expence_file.length > 0 ){
      const files: Array<File> = this.allfiles.adm_expence_file;
      if(files.length > 0){
        for(let i =0; i < files.length; i++){
          formData.append("adm_expence_file[]", files[i], files[i]['name']);
        }
      }
    }
    if(this.allfiles.direct_expence_file.length > 0 ){
      const files: Array<File> = this.allfiles.direct_expence_file;
      if(files.length > 0){
        for(let i =0; i < files.length; i++){
          formData.append("direct_expence_file[]", files[i], files[i]['name']);
        }
      }
    }

    //formData.append('budget',this.proposalFormBudget.value.budget);
    formData.append('administrative_budget_file_status',this.proposalFormBudget.value.administrative_budget_file_status);
    formData.append('direct_budget_file_status',this.proposalFormBudget.value.direct_budget_file_status);
    formData.append('step',6);

    this.proposalApi.editProposal(formData).subscribe((response: any) => {
          if (response.success) {
              this.getProposal();
              //this.fBudgetsubmitted = false;
              //this.proposalFormBudget.controls["budget"].setValue('');
              if(this.allfiles.adm_expence_file.length > 0 ){
                this.upFiles['adm_expence_file'] = [];
                this.allfiles['adm_expence_file'] = [];
              }
              if(this.allfiles.direct_expence_file.length > 0 ){
                this.upFiles['direct_expence_file'] = [];
                this.allfiles['direct_expence_file'] = [];
              }
          }        
      }, error => {
          this.fBudgetsubmitted = false;
      }
    );    
  }

  toggleProp() {
    //this.fPropsubmitted = true;
    //if (this.proposalFormProp.invalid) { this.toast.error('Please enter the required fields !!'); return; }

    const formData: any = new FormData();

    /*if(this.allfiles.property_inventory_file){
      const files: Array<File> = this.allfiles.property_inventory_file;
      
      if(files.length > 0){
        for(let i =0; i < files.length; i++){
          formData.append("property_inventory_file[]", files[i], files[i]['name']);
        }
      }
    }*/

    formData.append('inventory_file_status',this.proposalFormProp.value.inventory_file_status);
    formData.append('step',4);

    this.proposalApi.editProposal(formData).subscribe((response: any) => {
          if (response.success) {
              localStorage.setItem('stepCompleted', '4');
              
              this.clearAll();

              this.show1 = false;
              this.show2 = false;
              this.show3 = false;
              this.showProp = false;
              this.showBudget = true;
              this.showReq = false;
              this.showSubmit = false;

              this.active1 = false;
              this.active2 = false;
              this.active3 = false;
              this.activeProp = false;
              this.activeBudget = true;
              this.activeReq = false;
              this.activeSubmit = false;
              this.gotoTop();
          }        
      }, error => {
          this.fPropsubmitted = false;
      }
    );  
  }

  toggleBudget(tight_budget = false) {
    this.fBudgetsubmitted = true;
    
    if (this.proposalFormBudget.invalid ) { 
      this.toast.error(this.translate.instant('MESSAGE.REQUIRED_MESSAGE')); return;
    }else if (this.proposalFormAction.value.requested_grant=='' ) { 
      this.toast.error('Please enter the requested grant fields !!'); return; 
    }

    const formData: any = new FormData();
    
    /*if(this.allfiles.budget_requirement_file){
      const files: Array<File> = this.allfiles.budget_requirement_file;
      
      if(files.length > 0){
        for(let i =0; i < files.length; i++){
          formData.append("budget_requirement_file[]", files[i], files[i]['name']);
        }
      }
    }*/

    //console.log(this.proposalFormBudget.value.admExpences);
    //console.log(this.proposalFormBudget.value.directExpences);

    var total_expenditure_proposed = 0;
    var total_expenditure_tight_budget = 0;

    var admParam = [];
    var total_adm_budget_amount = 0;
    var total_adm_tight_budget = 0;

    for(let item of this.proposalFormBudget.value.admExpences){
      admParam.push(
        {
          adm_expenditure_item:item.description,
          adm_est_expense:item.budget_amount,
          adm_tight_budget:item.tight_budget,
          id:item.id
        }
      )
      total_adm_budget_amount = total_adm_budget_amount + item.budget_amount;
      total_adm_tight_budget = total_adm_tight_budget + item.tight_budget;
    }

    var directParam = [];
    var total_direct_budget_amount = 0;
    var total_direct_tight_budget = 0;

    for(let item of this.proposalFormBudget.value.directExpences){
      directParam.push(
        {
          direct_expenditure_item:item.description,
          direct_est_expense:item.budget_amount,
          direct_tight_budget:item.tight_budget,
          id:item.id
        }
      )
      total_direct_budget_amount = total_direct_budget_amount + item.budget_amount;
      total_direct_tight_budget = total_direct_tight_budget + item.tight_budget;
    }

    //if(admParam.length == 0){ this.toast.error('Please add administrative expenditure before proceed.'); return;}

    if(directParam.length == 0){ this.toast.error('Please add direct expenditure before proceed.'); return;}

    if(tight_budget){
      total_expenditure_tight_budget = total_adm_tight_budget + total_direct_tight_budget;
      if(total_adm_tight_budget == 0 || total_direct_tight_budget == 0){
        this.toast.error('Tight budget should be greater than zero.'); return;
      }
      if(total_adm_tight_budget > this.administrativeExpenses){
        this.toast.error('Tight budget should not be exceeded total administrative budget.'); return;
      }
      if(this.directExpenses > total_direct_tight_budget){
        this.toast.error('Minimum direct budget expenditure should be $'+this.directExpenses);
        return;
      }
      if(total_expenditure_tight_budget > this.proposal.approved_grant){
        this.toast.error('Total expenditure should not be greater than $'+this.proposal.approved_grant); return;
      }
    }else{
        total_expenditure_proposed = total_adm_budget_amount + total_direct_budget_amount;
        /*if(total_adm_budget_amount == 0 || total_direct_budget_amount == 0){
          this.toast.error('Total expenditure should be greater than zero.'); return;
        }*/
        if(total_direct_budget_amount == 0){
          this.toast.error('Total expenditure should be greater than zero.'); return;
        }
        if(total_adm_budget_amount > this.administrativeExpenses){
            this.toast.error('Total expenditure should not be exceeded total administrative budget.'); return;
        }
        if(this.directExpenses > total_direct_budget_amount){
          this.toast.error('Minimum direct budget expenditure should be $'+this.directExpenses); return;
        }
        if(total_expenditure_proposed > this.proposal.requested_grant){
          this.toast.error('Total expenditure should not be greater than $'+this.proposal.requested_grant); return;
        }
    }

    //formData.append('budget_file_status',this.proposalFormBudget.value.budget_file_status);
    formData.append('adm_budget',JSON.stringify(admParam));
    formData.append('direct_budget',JSON.stringify(directParam));
    formData.append('administrative_budget_file_status',this.proposalFormBudget.value.administrative_budget_file_status);
    formData.append('direct_budget_file_status',this.proposalFormBudget.value.direct_budget_file_status);
    formData.append('step',5);

    this.ngxService.start();
    this.proposalApi.editProposal(formData).subscribe((response: any) => {
          if (response.success) {
              this.ngxService.stop();
              localStorage.setItem('stepCompleted', '5');

              if(tight_budget){
                this.toast.success('Tight budget updated Successfully!!');
                this.router.navigate(['post-auth/proposal-list/']);
              }
              
              this.clearAll();

              this.show1 = false;
              this.show2 = false;
              this.show3 = false;
              this.showProp = false;
              this.showBudget = false;
              this.showLegal = true;
              this.showReq = false;
              this.showSubmit = false;

              this.active1 = false;
              this.active2 = false;
              this.active3 = false;
              this.activeProp = false;
              this.activeBudget = false;
              this.activeLegal = true;
              this.activeReq = false;
              this.activeSubmit = false;

              this.gotoTop();
          }        
      }, error => {
          this.ngxService.stop();
          this.fBudgetsubmitted = false;
      }
    );  
  }

  toggleLegal(value) {
      
      this.fLegalsubmitted = true;

      if (this.proposalFormLegal.invalid) { this.toast.error(this.translate.instant('MESSAGE.REQUIRED_MESSAGE')); return; }

      const formData: any = new FormData();
      var param = [];

      for(let item of this.legalArr){
          param.push(
            {
              status : value['legal_'+item.id],
              id:item.id,
              value:item.value,
              label:item.label
            }
          )
      }
   
      formData.append('legal', JSON.stringify(param));
      formData.append('legal_status',this.proposalForm_3.value.collecting_status);
      formData.append('step', 6);


      this.ngxService.start();
      this.proposalApi.editProposal(formData).subscribe((response: any) => {
         
          if (response.success) {
              this.ngxService.stop();
              localStorage.setItem('stepCompleted', '3');
              this.show1 = false;
              this.show2 = false;
              this.show3 = false;
              this.showProp = false;
              this.showBudget = false;
              this.showLegal = false;
              this.showReq = true;
              this.showSubmit = false;

              this.active1 = false;
              this.active2 = false;
              this.active3 = false;
              this.activeProp = false;
              this.activeBudget = false;
              this.activeLegal = false;
              this.activeReq = true;
              this.activeSubmit = false;
              this.gotoTop();
          }
      }, error => {
          this.ngxService.stop();
          this.fLegalsubmitted = false;
      }
      );
  }

  toggleReq() { 
    this.fReqsubmitted = true;
    if (this.proposalFormReq.invalid) { this.toast.error(this.translate.instant('MESSAGE.REQUIRED_MESSAGE')); return; }

    const formData: any = new FormData();
    formData.append('step',7);
    formData.append('requirement_file_status',this.proposalFormReq.value.requirement_file_status);

    this.ngxService.start();
    this.proposalApi.editProposal(formData).subscribe((response: any) => {
          console.log(response);
          if (response.success) {
              this.ngxService.stop();
              localStorage.setItem('stepCompleted', '7');
              this.show1 = false;
              this.show2 = false;
              this.show3 = false;
              this.showProp = false;
              this.showBudget = false;
              this.showLegal = false;
              this.showReq = false;
              this.showEvidence = true;
              this.showSubmit = false;

              this.active1 = false;
              this.active2 = false;
              this.active3 = false;
              this.activeProp = false;
              this.activeBudget = false;
              this.activeLegal = false;
              this.activeReq = false;
              this.activeReq = false;
              this.activeEvidence = true;
              this.activeSubmit = false;

              this.gotoTop();
          }        
      }, error => {
          this.ngxService.stop();
          this.fReqsubmitted = false;
          console.log('Proposal Create error: ', error);
      }
    );  
  }

  toggleEvidence() { 
    this.fEvidencesubmitted = true;
    if (this.proposalFormEvidence.invalid) { this.toast.error(this.translate.instant('MESSAGE.REQUIRED_MESSAGE')); return; }

    const formData: any = new FormData();
    formData.append('step',8);
    formData.append('evidence_doc_status',this.proposalFormEvidence.value.evidence_doc_status);

    this.ngxService.start();
    this.proposalApi.editProposal(formData).subscribe((response: any) => {
          console.log(response);
          if (response.success) {
              this.ngxService.stop();
              localStorage.setItem('stepCompleted', '8');
              this.show1 = false;
              this.show2 = false;
              this.show3 = false;
              this.showProp = false;
              this.showBudget = false;
              this.showLegal = false;
              this.showReq = false;
              this.showEvidence = false;
              this.showSubmit = true;

              this.active1 = false;
              this.active2 = false;
              this.active3 = false;
              this.activeProp = false;
              this.activeBudget = false;
              this.activeLegal = false;
              this.activeReq = false;
              this.activeReq = false;
              this.activeEvidence = false;
              this.activeSubmit = true;

              this.gotoTop();
          }        
      }, error => {
          this.ngxService.stop();
          this.fEvidencesubmitted = false;
          console.log('Proposal Create error: ', error);
      }
    );  
  }

  toggle7() {
    this.f7submitted = true;
    if (this.proposalFormSubmit.invalid) { this.toast.error(this.translate.instant('MESSAGE.REQUIRED_MESSAGE')); return; }
    //else if (!this.memberlisting ) { this.toast.error('Please add members first !!'); return; }
    if(!this.proposalFormSubmit.get('proposalAgree').value){
        this.toast.warning('You have to agree privacy policy');
        return;
    }

    const formData: any = new FormData();

    let d=new Date(this.proposalFormSubmit.value.date);
    let actualDate=d.getDate();
    let actualMonth=d.getMonth()+1;
    let actualYear=d.getFullYear();    
    let formattedDate=actualDate + '/' + actualMonth + '/' + actualYear;

    formData.append('name',this.proposalFormSubmit.value.name);
    formData.append('proposalAgree',this.proposalFormSubmit.value.proposalAgree);
    
    formData.append('date',formattedDate);
    formData.append('step',9);
    formData.append('application_status',1);

    this.ngxService.start();
    this.proposalApi.editProposal(formData).subscribe((response: any) => {
          if (response.success) {
              this.ngxService.stop();
              localStorage.setItem('stepCompleted', '9');
              
              this.clearAll();
              this.toast.success('Proposal Submitted Successfully');

              localStorage.removeItem('stepCompleted');
              localStorage.removeItem('proposal_id');
              this.router.navigate(['post-auth/proposal-list']);
          }        
      }, error => {
          this.ngxService.stop();
          this.f7submitted = false;
      }
    );  
  }

  uploadDepartmentFile() {
    this.loadingFile = true;
    //this.fReqsubmitted = true;
    //if (this.proposalFormReq.invalid) { this.toast.error('Please enter the required fields !!'); return; }
    var department = this.proposalFormReq.value.department;
    const formData: any = new FormData();

    if(this.proposalFormReq.value.desc == ''){ 
      this.loadingFile = false;
      this.toast.error(this.translate.instant('PROPOSAL.SELECT_CLASSIFICATION_ERR')); 
      return; 
    }

    if(department == 'puerto_rico_state_department'){
      if(this.allfiles.puerto_rico_state_department.length > 0){
        const files: Array<File> = this.allfiles.puerto_rico_state_department;
        if(files.length > 0){
          for(let i =0; i < files.length; i++){
            formData.append("puerto_rico_state_department[]", files[i], files[i]['name']);
          }
        }
      }else{
        this.loadingFile = false;
        this.toast.error(this.translate.instant('MESSAGE.FILE_ATTACH')); return; 
      }
    }

    if(department == 'department_of_finance'){
      if(this.allfiles.department_of_finance.length > 0){
        const files: Array<File> = this.allfiles.department_of_finance;
        if(files.length > 0){
          for(let i =0; i < files.length; i++){
            formData.append("department_of_finance[]", files[i], files[i]['name']);
          }
        }
      }else{
        this.loadingFile = false;
        this.toast.error(this.translate.instant('MESSAGE.FILE_ATTACH')); return; 
      }
    }

    if(department == 'puerto_rico_state_insurance_fund_corporation'){
      if(this.allfiles.puerto_rico_state_insurance_fund_corporation.length > 0){
        const files: Array<File> = this.allfiles.puerto_rico_state_insurance_fund_corporation;
        if(files.length > 0){
          for(let i =0; i < files.length; i++){
            formData.append("puerto_rico_state_insurance_fund_corporation[]", files[i], files[i]['name']);
          }
        }
      }else{
        this.loadingFile = false;
        this.toast.error(this.translate.instant('MESSAGE.FILE_ATTACH')); return; 
      }
    }

    if(department == 'municipal_income_collection_center'){
      if(this.allfiles.municipal_income_collection_center.length > 0){
        const files: Array<File> = this.allfiles.municipal_income_collection_center;
        if(files.length > 0){
          for(let i =0; i < files.length; i++){
            formData.append("municipal_income_collection_center[]", files[i], files[i]['name']);
          }
        }
      }else{
        this.loadingFile = false;
        this.toast.error(this.translate.instant('MESSAGE.FILE_ATTACH')); return; 
      }
    }

    if(department == 'osfl'){
      if(this.allfiles.osfl.length > 0 ){
        const files: Array<File> = this.allfiles.osfl;
        if(files.length > 0){
          for(let i =0; i < files.length; i++){
            formData.append("osfl[]", files[i], files[i]['name']);
          }
        }
      }else{
        this.loadingFile = false;
        this.toast.error(this.translate.instant('MESSAGE.FILE_ATTACH')); return; 
      }
    }



    /*if(department == 'internal_revenue_services'){
      if(this.allfiles.internal_revenue_services.length > 0){
        const files: Array<File> = this.allfiles.internal_revenue_services;
        if(files.length > 0){
          for(let i =0; i < files.length; i++){
            formData.append("internal_revenue_services[]", files[i], files[i]['name']);
          }
        }
      }else{
        this.toast.error(this.translate.instant('MESSAGE.FILE_ATTACH')); return; 
      }
    }

    if(department == 'department_of_labor_and_human_resources'){
      if(this.allfiles.department_of_labor_and_human_resources.length > 0 && department == 'department_of_labor_and_human_resources'){
        const files: Array<File> = this.allfiles.department_of_labor_and_human_resources;
        if(files.length > 0){
          for(let i =0; i < files.length; i++){
            formData.append("department_of_labor_and_human_resources[]", files[i], files[i]['name']);
          }
        }
      }else{
        this.toast.error(this.translate.instant('MESSAGE.FILE_ATTACH')); return; 
      }
    }

    if(department == 'health_department'){
      if(this.allfiles.health_department.length > 0){
        const files: Array<File> = this.allfiles.health_department;
        if(files.length > 0){
          for(let i =0; i < files.length; i++){
            formData.append("health_department[]", files[i], files[i]['name']);
          }
        }
      }else{
        this.toast.error(this.translate.instant('MESSAGE.FILE_ATTACH')); return; 
      }
    }

    if(department == 'administration_of_regulations_and_permits'){
      if(this.allfiles.administration_of_regulations_and_permits.length > 0){
        const files: Array<File> = this.allfiles.administration_of_regulations_and_permits;
        if(files.length > 0){
          for(let i =0; i < files.length; i++){
            formData.append("administration_of_regulations_and_permits[]", files[i], files[i]['name']);
          }
        }
      }else{
        this.toast.error(this.translate.instant('MESSAGE.FILE_ATTACH')); return; 
      }
    }

    if(department == 'puerto_rico_fire_fighter_body'){
      if(this.allfiles.puerto_rico_fire_fighter_body.length > 0){
        const files: Array<File> = this.allfiles.puerto_rico_fire_fighter_body;
        if(files.length > 0){
          for(let i =0; i < files.length; i++){
            formData.append("puerto_rico_fire_fighter_body[]", files[i], files[i]['name']);
          }
        }
      }else{
        this.toast.error(this.translate.instant('MESSAGE.FILE_ATTACH')); return; 
      }
    }

    if(department == 'custody_agency'){
      if(this.allfiles.custody_agency.length > 0){
        const files: Array<File> = this.allfiles.custody_agency;
        if(files.length > 0){
          for(let i =0; i < files.length; i++){
            formData.append("custody_agency[]", files[i], files[i]['name']);
          }
        }
      }else{
        this.toast.error(this.translate.instant('MESSAGE.FILE_ATTACH')); return; 
      }
    }

    if(department == 'additional_documents'){
      if(this.allfiles.additional_documents.length > 0){
        const files: Array<File> = this.allfiles.additional_documents;
        if(files.length > 0){
          for(let i =0; i < files.length; i++){
            formData.append("additional_documents[]", files[i], files[i]['name']);
          }
        }
      }else{
        this.toast.error(this.translate.instant('MESSAGE.FILE_ATTACH')); return; 
      }
    }

    if(department == 'seguro_social'){
      if(this.allfiles.seguro_social.length > 0){
        const files: Array<File> = this.allfiles.seguro_social;
        if(files.length > 0){
          for(let i =0; i < files.length; i++){
            formData.append("seguro_social[]", files[i], files[i]['name']);
          }
        }
      }else{
        this.toast.error(this.translate.instant('MESSAGE.FILE_ATTACH')); return; 
      }
    }

    if(department == 'administration_for_child_support'){
      if(this.allfiles.administration_for_child_support.length > 0){
        const files: Array<File> = this.allfiles.administration_for_child_support;
        if(files.length > 0){
          for(let i =0; i < files.length; i++){
            formData.append("administration_for_child_support[]", files[i], files[i]['name']);
          }
        }
      }else{
        this.toast.error(this.translate.instant('MESSAGE.FILE_ATTACH')); return; 
      }
    }

    if(department == 'puerto_rico_police'){
      if(this.allfiles.puerto_rico_police.length > 0){
        const files: Array<File> = this.allfiles.puerto_rico_police;
        if(files.length > 0){
          for(let i =0; i < files.length; i++){
            formData.append("puerto_rico_police[]", files[i], files[i]['name']);
          }
        }
      }else{
        this.toast.error(this.translate.instant('MESSAGE.FILE_ATTACH')); return; 
      }
    }

    if(department == 'negotiated_conventions_of_san_jun'){
      if(this.allfiles.negotiated_conventions_of_san_jun.length > 0 ){
        const files: Array<File> = this.allfiles.negotiated_conventions_of_san_jun;
        if(files.length > 0){
          for(let i =0; i < files.length; i++){
            formData.append("negotiated_conventions_of_san_jun[]", files[i], files[i]['name']);
          }
        }
      }else{
        this.toast.error(this.translate.instant('MESSAGE.FILE_ATTACH')); return; 
      }
    }

    if(department == 'higher_education_council'){
      if(this.allfiles.higher_education_council.length > 0 ){
        const files: Array<File> = this.allfiles.higher_education_council;
        if(files.length > 0){
          for(let i =0; i < files.length; i++){
            formData.append("higher_education_council[]", files[i], files[i]['name']);
          }
        }
      }else{
        this.toast.error(this.translate.instant('MESSAGE.FILE_ATTACH')); return; 
      }
    }*/

    
    formData.append('desc',this.proposalFormReq.value.desc);
    formData.append('step',7);
    formData.append('requirement_file_status',this.proposalFormReq.value.requirement_file_status);
    this.proposalApi.editProposal(formData).subscribe((response: any) => {
          if (response.success) {
            this.selectedDepartment = '';
            this.proposalFormReq.controls["department"].setValue('');
            this.proposalFormReq.controls["desc"].setValue('');
            this.upFiles[department] = [];
            this.allfiles[department] = [];
            this.getProposal();
            this.loadingFile = false;
          }        
      }, error => {
          this.fReqsubmitted = false;
      }
    );


  }

  actionSubmit() { 
    this.fActionsubmitted = true;
    if (this.proposalFormAction.invalid) { this.toast.error(this.translate.instant('MESSAGE.REQUIRED_MESSAGE')); return; }

    const formData: any = new FormData();
    
    formData.append('requested_grant',this.proposalFormAction.value.requested_grant);
    formData.append('donation_grant',this.proposalFormAction.value.donation_grant);
    formData.append('approved_grant',this.proposalFormAction.value.approved_grant);
    formData.append('open_status',this.proposalFormAction.value.open_status);
    formData.append('assigned_user',this.proposalFormAction.value.assigned_user);

    formData.append('notifyUser',this.proposalFormAction.value.notifyUser);

    this.ngxService.start();
    this.proposalApi.updateGrant(formData).subscribe((response: any) => {
          if (response.success) {
            this.getProposal();
            this.toast.success(response.message);
            this.ngxService.stop();
          }        
      }, error => {
          this.ngxService.stop();
          this.fActionsubmitted = false;
      }
    );  
  }

  gotoTop() {
    document.querySelector('mat-sidenav-content').scrollTop = 0;
  }

  back1() {
    this.ngxService.start();
    this.show1 = true;
    this.show2 = false;
    this.show3 = false;
    this.showProp = false;
    this.showBudget = false;
    this.showLegal = false;
    this.showReq = false;
    this.showSubmit = false;

    this.active1 = true;
    this.active2 = false;
    this.active3 = false;
    this.activeProp = false;
    this.activeBudget = false;
    this.activeLegal = false;
    this.activeReq = false;
    this.activeEvidence = false;
    this.activeSubmit = false;
    this.ngxService.stop();
    this.gotoTop();
  }

  back2() {
    this.ngxService.start();
    this.show1 = false;
    this.show2 = true;
    this.show3 = false;
    this.showProp = false;
    this.showBudget = false;
    this.showLegal = false;
    this.showReq = false;
    this.showSubmit = false;

    this.active1 = false;
    this.active2 = true;
    this.active3 = false;
    this.activeProp = false;
    this.activeBudget = false;
    this.activeLegal = false;
    this.activeReq = false;
    this.activeEvidence = false;
    this.activeSubmit = false;
    this.ngxService.stop();
    this.gotoTop();
  }

  back3() {
    this.ngxService.start();
    this.show1 = false;
    this.show2 = false;
    this.show3 = true;
    this.showProp = false;
    this.showBudget = false;
    this.showLegal = false;
    this.showReq = false;
    this.showEvidence = false;
    this.showSubmit = false;

    this.active1 = false;
    this.active2 = false;
    this.active3 = true;
    this.activeProp = false;
    this.activeBudget = false;
    this.activeLegal = false;
    this.activeReq = false;
    this.activeEvidence = false;
    this.activeSubmit = false;
    this.ngxService.stop();
    this.gotoTop();
  }


  backProp() {
    this.ngxService.start();
    this.getProposal();
    this.show1 = false;
    this.show2 = false;
    this.show3 = false;
    this.showProp = true;
    this.showBudget = false;
    this.showLegal = false;
    this.showReq = false;
    this.showEvidence = false;
    this.showSubmit = false;

    this.active1 = false;
    this.active2 = false;
    this.active3 = false;
    this.activeProp = true;
    this.activeBudget = false;
    this.activeLegal = false;
    this.activeReq = false;
    this.activeEvidence = false;
    this.activeSubmit = false;
    this.ngxService.stop();
    this.gotoTop();
  }

  backBudget() {
    this.ngxService.start();
    this.show1 = false;
    this.show2 = false;
    this.show3 = false;
    this.showProp = false;
    this.showBudget = true;
    this.showLegal = false;
    this.showReq = false;
    this.showEvidence = false;
    this.showSubmit = false;

    this.active1 = false;
    this.active2 = false;
    this.active3 = false;
    this.activeProp = false;
    this.activeBudget = true;
    this.activeLegal = false;
    this.activeReq = false;
    this.activeEvidence = false;
    this.activeSubmit = false;
    this.ngxService.stop();
    this.gotoTop();
  }

  backLegal() {
    this.ngxService.start();
    this.show1 = false;
    this.show2 = false;
    this.show3 = false;
    this.showProp = false;
    this.showBudget = false;
    this.showLegal = true;
    this.showReq = false;
    this.showEvidence = false;
    this.showSubmit = false;

    this.active1 = false;
    this.active2 = false;
    this.active3 = false;
    this.activeProp = false;
    this.activeBudget = false;
    this.activeLegal = true;
    this.activeReq = false;
    this.activeEvidence = false;
    this.activeSubmit = false;
    this.ngxService.stop();
    this.gotoTop();
  }

  backReq() {
    this.ngxService.start();
    this.show1 = false;
    this.show2 = false;
    this.show3 = false;
    this.showProp = false;
    this.showBudget = false;
    this.showLegal = false;
    this.showReq = true;
    this.showEvidence = false;
    this.showSubmit = false;

    this.active1 = false;
    this.active2 = false;
    this.active3 = false;
    this.activeProp = false;
    this.activeBudget = false;
    this.activeLegal = false;
    this.activeReq = true;
    this.showEvidence = false;
    this.activeSubmit = false;
    this.ngxService.stop();
    this.gotoTop();
  }

  backEvidence() {
    this.ngxService.start();

    this.show1 = false;
    this.show2 = false;
    this.show3 = false;
    this.showProp = false;
    this.showBudget = false;
    this.showLegal = false;
    this.showReq = false;
    this.showEvidence = true;
    this.showSubmit = false;

    this.active1 = false;
    this.active2 = false;
    this.active3 = false;
    this.activeProp = false;
    this.activeBudget = false;
    this.activeLegal = false;
    this.activeReq = false;
    this.activeEvidence = true;
    this.activeSubmit = false;

    this.ngxService.stop();
    this.gotoTop();
  }

  backSubmit() {
    this.ngxService.start();
    this.show1 = false;
    this.show2 = false;
    this.show3 = false;
    this.showProp = false;
    this.showBudget = false;
    this.showLegal = false;
    this.showReq = false;
    this.showEvidence = false;
    this.showSubmit = true;

    this.active1 = false;
    this.active2 = false;
    this.active3 = false;
    this.activeProp = false;
    this.activeBudget = false;
    this.activeLegal = false;
    this.activeReq = false;
    this.activeEvidence = false;
    this.activeSubmit = true;
    this.ngxService.stop();
    this.gotoTop();
  }

  calculateAdministrativeBudget(){
    this.total_adm_budget_amount = 0;
    this.total_adm_tight_budget = 0;
    for(let item of this.proposalFormBudget.value.admExpences){
      this.total_adm_budget_amount = this.total_adm_budget_amount + item.budget_amount;
      this.total_adm_tight_budget = this.total_adm_tight_budget + item.tight_budget;
      //console.log(this.total_adm_budget_amount);
    }
  }

  calculateDirectBudget(){
    this.total_direct_budget_amount = 0;
    this.total_direct_tight_budget = 0;
    for(let item of this.proposalFormBudget.value.directExpences){
      this.total_direct_budget_amount = this.total_direct_budget_amount + item.budget_amount;
      this.total_direct_tight_budget = this.total_direct_tight_budget + item.tight_budget;
      //console.log(this.total_direct_budget_amount);
    }
  }
  roundFix(value) {
     return(Math.round(value * 100) / 100);
  }

  clearAll(){
    this.filesToUpload = [];
  }

  openProposalDialog() {
    const dialogRef = this.dialog.open(PopupModalsComponent, {
      panelClass: 'dialog-xs',
      disableClose: true,
      width: '600px'
    });
  }

  openProposalattachDialog() {
    const dialogRef = this.dialog.open(AttachPopupComponent, {
      panelClass: 'dialog-xs',
      disableClose: true,
      width: '600px'
    });
  }

  approveProposalDialog() {
    const dialogRef = this.dialog.open(ApprovedModalComponent, {
      panelClass: 'dialog-xs',
      data:{},
      width: '600px'
    });
    dialogRef.afterClosed().subscribe(response =>{
      if(response.status == 'approve'){
        this.showLoader = true;
        this.loader = true;
        this.proposalApi.acceptProposal(localStorage.getItem('proposal_id')).subscribe((response: any) => {
          this.showLoader = false;
          if (response.success) {
            dialogRef.close();
            this.toast.success(response.message);
            this.getProposal();
            this.loader = false;
          }
        }, (errorData: any) => {
          this.showLoader = false;
        });

      }
    })
  }

  rejectProposalDialog() {
    const dialogRef = this.dialog.open(RejectedModalComponent, {
      panelClass: 'dialog-xs',
      data:{status:'reject'},
      width: '600px'
    });
    dialogRef.afterClosed().subscribe(response =>{
      console.log(response)
      if(response.status == 'reject'){
        //console.log('hi')
        this.showLoader = true;
        this.loader = true;
        this.proposalApi.rejectProposal(localStorage.getItem('proposal_id')).subscribe((response: any) => {
          this.showLoader = false;
          if (response.success) {
            dialogRef.close();
            this.toast.success('Proposal reject Successfully');
            this.getProposal();
            this.loader = false;
          }
        }, (errorData: any) => {
          this.showLoader = false;
        });

      }
    })
  }

  openMoreInfoDialog() {
    const dialogRef = this.dialog.open(MoreInfoComponent, {
      panelClass: 'dialog-xs',
      disableClose: true,
      width: '600px',
      data:{status:'notView'},
      
    });
  }

  onSelectPAP(id) {
    //console.log(id);
      this.UserMasterApi.pareocategoryList(id).subscribe((res: any) => {
      if (res.success) {
        this.Pareocategorylisting = res.data;
        return this.Pareocategorylisting;
      }
    }, (err) => {});
  }

  /*clearFileObj(){
    upFiles : any = {              
        necessity_statement_file: [], 
        certification_no_debt_file: [], 
        certification_of_tax_filing_file: [] 
    };
    allfiles : any = {              
        necessity_statement_file: [], 
        certification_no_debt_file: [], 
        certification_of_tax_filing_file: [] 
    };
  }*/

  // downloadPdf(){
    
  //   this.proposalApi.proposalPdf().subscribe(
  //     res=>{
  //       var newBlob = new Blob([res], { type: "application/pdf" });
  //       console.log(newBlob)

  //       // IE doesn't allow using a blob object directly as link href
  //       // instead it is necessary to use msSaveOrOpenBlob
  //       if (window.navigator && window.navigator.msSaveOrOpenBlob) {
  //         window.navigator.msSaveOrOpenBlob(newBlob);
  //         return;
  //       }
  //       // For other browsers: 
  //       // Create a link pointing to the ObjectURL containing the blob.
  //       const data = window.URL.createObjectURL(newBlob);
  //       //const orgData = data.replace('blob:http://localhost:4200/',AppConst.FILE_BASE_URL)
  //       console.log(data.replace('blob:http://localhost:4200/',AppConst.FILE_BASE_URL))
  //       var link = document.createElement('a');
  //       console.log(link)
    
  //       link.href = data;
  //       link.download = "proposal.pdf";
  //       // this is necessary as link.click() does not work on the latest firefox
  //       link.dispatchEvent(new MouseEvent('click', { bubbles: true, cancelable: true, view: window }));
  
  //       setTimeout(function () {
  //         // For Firefox it is necessary to delay revoking the ObjectURL
  //         window.URL.revokeObjectURL(data);
  //         link.remove();
  //       }, 100);
      
  //     },
  //     error=>{
        
  //       console.log(error);
  //     }
  //   )
  // }

}
@Component({
  selector: 'app-reject-modal',
  templateUrl: './reject-modal/reject-modal.component.html',
})

export class RejectedModalComponent {
  public showLoader: boolean;
  constructor(
      public dialogRef: MatDialogRef<RejectedModalComponent>,
      @Inject(MAT_DIALOG_DATA) public data:any
  ) { }
 
  rejectproposal(){
      this.dialogRef.close({status:'reject'});
  }
 deleteNews(){}
 deleteMember(){}
  onCloseClick(){
      this.dialogRef.close();
  }
}

@Component({
  selector: 'app-approve-modal',
  templateUrl: './approve-modal/approve-modal.component.html',
})

export class ApprovedModalComponent {
  public showLoader: boolean;
  constructor(
      public dialogRef: MatDialogRef<ApprovedModalComponent>,
      @Inject(MAT_DIALOG_DATA) public data:any
  ) { }
 
  approveproposal(){
      this.dialogRef.close({status:'approve'});
  }

  onCloseClick(){
      this.dialogRef.close();
  }
 
}