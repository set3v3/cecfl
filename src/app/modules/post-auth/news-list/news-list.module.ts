import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NewsListRouteModule} from './news-list.routes.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { AuthService } from '../../../services/apis/auth.service';
import { MatSortModule } from '@angular/material';
import { UserMasterService } from 'src/app/services/apis/usermaster';
import { NewsService } from '../../../services/apis/news.service';
import { NewsListComponent,NewsDeleteModalComponent} from './news-list.component';
import { NgxPaginationModule } from 'ngx-pagination';
@NgModule({
  imports: [
    NewsListRouteModule,
    SharedModule,
    MatSortModule,
    NgxPaginationModule
  ],
  declarations: [
    NewsListComponent,
    NewsDeleteModalComponent
  ],
  entryComponents: [
    NewsListComponent,
    NewsDeleteModalComponent
  ],
  providers: [AuthService, UserMasterService, NewsService]
})
export class NewsListModule { }





