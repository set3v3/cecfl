import { Component, OnInit, Inject } from '@angular/core';
import { NewsService } from '../../../services/apis/news.service';
import { Router, ActivatedRoute } from '@angular/router';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { ToastProvider } from 'src/app/shared/modules/toast/toast.provider';
import { PermissionProvider } from 'src/app/services/permission/permission.service';
import { NgxUiLoaderService,SPINNER } from 'ngx-ui-loader';
import { CommonService } from 'src/app/services/apis/common.service';

@Component({
  selector: 'app-news-list',
  templateUrl: './news-list.component.html',
  styleUrls: ['./news-list.component.scss']
 
})

export class NewsListComponent implements OnInit {
	newsliting: Array<any>;
	length;
    msg;
    p: number = 1;
    limit:number =10
	itemsPerPage:number = 10;
	newsParam: any = {};
	spinnerType = SPINNER.rectangleBounce;
	createtatus: boolean;
  	editstatus: boolean;
	deletestatus: boolean;
	searchString; 
	language: string;
	constructor(
		private newsApi: NewsService,
		public router: Router,
		public dialog: MatDialog,
		private toast: ToastProvider,
		private Permission: PermissionProvider,
		private ngxService:NgxUiLoaderService,
		private common: CommonService,
	) {
		this.newsliting = [];
		this.editstatus = Permission.sidePanelpermission('news-edit');
	    this.createtatus = Permission.sidePanelpermission('news-add');
		this.deletestatus = Permission.permission('news-list', 'delete');
	}

	ngOnInit() {
		this.newsParam.page = this.p;
        //this.newsParam.limit = this.limit;
		this.common.currLang.subscribe(
            res => {
                this.language = res;
                this.getnewslist();
            }
        )
	}

	getnewslist = () => {
		this.ngxService.start();
		this.newsApi.news(this.newsParam).subscribe((res: any) => {
			this.ngxService.stop();
			//console.log(res);
			this.newsliting = res.data;
			this.length = res['total'];

		}, (err) => {
			this.ngxService.stop()
		});
	}

	newsdelete(id) {
		const dialogRef = this.dialog.open(NewsDeleteModalComponent, {
			panelClass: 'dialog-xs',
			data:{status:'delete'},
			width: '600px'
		});
		dialogRef.afterClosed().subscribe(response =>{
			//console.log(response)
			if(response.delStatus == 'deleted'){
			  //console.log('hi')
				this.newsApi.delete(id).subscribe((res: any) => {
					//console.log(res);
					this.msg = res['message']
					this.toast.success( this.msg)
					this.getnewslist();
				}, (err) => {
					this.msg = err['message']
					this.toast.error( this.msg)
				});
			}
	  
		})
	}

	newsSearch(){
		this.newsParam.search = this.searchString;
		this.getnewslist();
	}
		
	paginationFeed(page){
        this.p = page;
        let start = (page - 1) * this.itemsPerPage;
        this.newsParam.page = page;
        this.newsApi.news(this.newsParam).subscribe(data=> {
            this.newsliting = data.data;
        });
        
	}
}


@Component({
	selector: 'app-reject-modal',
	templateUrl: '../proposal/reject-modal/reject-modal.component.html',
  })
  
  export class NewsDeleteModalComponent {
	public showLoader: boolean;
	constructor(
		public dialogRef: MatDialogRef<NewsDeleteModalComponent>,
		@Inject(MAT_DIALOG_DATA) public data:any
	) { }
   
	deleteNews(){
		this.dialogRef.close({delStatus:'deleted'});
	}
	rejectproposal(){}

	deleteMember(){}
  
	onCloseClick(){
		this.dialogRef.close();
	}
  }

