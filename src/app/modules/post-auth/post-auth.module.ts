import { NgModule } from '@angular/core';
// import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { PostAuthComponent } from './post-auth.component';
import { PostAuthRouteModule } from './post-auth.routes.module';
import { ComponentsModule } from '../../components/components.module';
import { AppMaterialModule } from '../../app.material.module';
import { AuthService } from '../../services/apis/auth.service';
import { SharedModule } from '../../shared/shared.module';
import { CryptoProvider } from '../../services/crypto/crypto.service';
//import { DashboardComponent } from './dashboard/dashboard.component';
import { ChangepasswordComponent } from './change-password/change-password.component';
import { NgxMaskModule } from 'ngx-mask';
import { FormsModule } from '@angular/forms';
import { OwlModule } from 'ngx-owl-carousel';
import { ChartsModule } from 'ng2-charts';
import { PermissionProvider } from 'src/app/services/permission/permission.service';
import { NavigationComponent } from './navigation/navigation.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { ApproveModalComponent } from './proposal/approve-modal/approve-modal.component';
import { RejectModalComponent } from './proposal/reject-modal/reject-modal.component';
import { NgxUiLoaderModule } from 'ngx-ui-loader';
import { OwlDateTimeModule, OwlNativeDateTimeModule,OWL_DATE_TIME_LOCALE } from 'ng-pick-datetime';
import { MoreInfoComponent } from './proposal/more-info/more-info.component';
//import { PopupModalsComponent } from './proposal/popup-modals/popup-modals.component';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClient } from '@angular/common/http';
import { AppConst } from 'src/app/app.constants';
import { CmsModalComponent } from './cms-modal/cms-modal.component';

export function HttpLoaderFactory(HttpClient: HttpClient) {
  return new TranslateHttpLoader(HttpClient,AppConst.PROJECT_FOLDER+'assets/i18n/', '.json');
}


@NgModule({
  imports: [
    PostAuthRouteModule,
    ComponentsModule,
    AppMaterialModule,
    SharedModule,
    NgxUiLoaderModule,
    NgxMaskModule.forRoot(),
    FormsModule,
    OwlModule,
    ChartsModule,
    NgxPaginationModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      
        
      }
    }),
  ],
  declarations: [
    PostAuthComponent,
    DashboardComponent,
    //DashboardDemoComponent,
    ChangepasswordComponent,
    NavigationComponent,
    ApproveModalComponent,
    RejectModalComponent,
    MoreInfoComponent,
    CmsModalComponent
    //PopupModalsComponent
  ],
  entryComponents: [
    ChangepasswordComponent,
    MoreInfoComponent
    //PopupModalsComponent
  ],

  providers: [AuthService
    , PermissionProvider
    , { provide: OWL_DATE_TIME_LOCALE, useValue: 'en-SG'}
  ]
})
export class PostAuthModule { }
