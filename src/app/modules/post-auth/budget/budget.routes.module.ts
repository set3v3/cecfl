import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from 'src/app/services/guards/auth-guard.service';
import { BudgetComponent } from './budget.component';
import { NgxUiLoaderModule } from  'ngx-ui-loader';
const routerConfig: Routes = [
  { path: '', component: BudgetComponent, canActivate: [AuthGuard], pathMatch: 'full' },
];
@NgModule({
  imports: [
    NgxUiLoaderModule,
    RouterModule.forChild(routerConfig)
  ],
  exports: [RouterModule],
  declarations: [],
  providers: [AuthGuard]
})
export class BudgetRouteModule { }
