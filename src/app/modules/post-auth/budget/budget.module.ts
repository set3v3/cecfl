import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BudgetRouteModule } from './budget.routes.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { AuthService } from 'src/app/services/apis/auth.service';
import { MatSortModule } from '@angular/material';
import { UserMasterService } from 'src/app/services/apis/usermaster';
import { BudgetComponent} from './budget.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { OwlDateTimeModule, OwlNativeDateTimeModule,OWL_DATE_TIME_LOCALE } from 'ng-pick-datetime';
@NgModule({
  imports: [
    BudgetRouteModule,
    SharedModule,
    MatSortModule,
    NgxPaginationModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
  ],
  declarations: [
    BudgetComponent
  ],
  entryComponents: [
    BudgetComponent
  ],
  providers: [
    AuthService, UserMasterService
    , { provide: OWL_DATE_TIME_LOCALE, useValue: 'en-SG'}
  ]
})
export class BudgetModule { }





