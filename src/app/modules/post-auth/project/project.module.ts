import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProjectRouteModule } from './project.route.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { AuthService } from '../../../services/apis/auth.service';
import { MatSortModule } from '@angular/material';
import { UserMasterService } from 'src/app/services/apis/usermaster';
import { CKEditorModule } from 'ngx-ckeditor';
import { ProjectService } from '../../../services/apis/project.service';
import { ProjectComponent } from './project.component';
import { OwlDateTimeModule, OwlNativeDateTimeModule,OWL_DATE_TIME_LOCALE } from 'ng-pick-datetime';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
export function HttpLoaderFactory(HttpClient: HttpClient) {
  return new TranslateHttpLoader(HttpClient,'../../../../assets/i18n/', '.json');
}
@NgModule({
  declarations: [
    ProjectComponent
  ],
  imports: [
    CommonModule,
    ProjectRouteModule,
    SharedModule,
    MatSortModule,
    CKEditorModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      
        
      }
    }),
  ],
  entryComponents: [
    ProjectComponent,
  ],
  providers: [AuthService,
     UserMasterService,ProjectService,
     { provide: OWL_DATE_TIME_LOCALE, useValue: 'en-SG'}
     ]
})
export class ProjectModule { }
