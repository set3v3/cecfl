import { Component, OnInit } from '@angular/core';
import { ProjectService } from '../../../services/apis/project.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastProvider } from 'src/app/shared/modules/toast/toast.provider';
import { UserStorageProvider } from '../../../services/storage/user-storage.service';
import { DatePipe } from '@angular/common';
import { NgxUiLoaderService,SPINNER } from 'ngx-ui-loader';
import { CommonService } from 'src/app/services/apis/common.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
    selector: 'app-project',
    templateUrl: './project.component.html',
    styleUrls: ['./project.component.scss']
})
export class ProjectComponent implements OnInit {
    language;
    public editorValue: string = '';
    addProjectform: FormGroup;
    msg;
    successMsg: boolean = false;
    errorMsg: boolean = false;
    projectId: number = 0;
    public formSubmit: boolean = false;
    selectedDate;
    spinnerType = SPINNER.rectangleBounce;
    constructor(
        private translate: TranslateService,
        private projectApi: ProjectService,
        public router: Router,
        public route: ActivatedRoute,
        private userStorage: UserStorageProvider,
        private toast: ToastProvider,
        private datePipe: DatePipe,
        private ngxService:NgxUiLoaderService,
        private common :CommonService
    ) {
        this.projectFormBuild();
    }

    ngOnInit() {
        this.route.params.subscribe(params => {
            if (params) {
                if (params['id']) {
                    this.projectId = +params['id'];
                    if (this.projectId > 0) {
                        this.view();
                    }
                }
            }
        });

        this.common.currLang.subscribe(
			res=>{
				this.language = res;
			}
        )
    }

    projectFormBuild() {
        this.addProjectform = new FormGroup({
            projectNameEn: new FormControl('', [ Validators.required]),
            projectNameEs:  new FormControl('', [ Validators.required]),
            startDate: new FormControl('', [ Validators.required]),
            endDate: new FormControl('', [ Validators.required]),
            period: new FormControl('', [ Validators.required]),
            status: new FormControl('', [ Validators.required]),
        });
    }

    checkDateTime(event){
        this.selectedDate = event.value
    }

    addProject(value) {
        this.formSubmit = true;
        var projectObj ={
            projectNameEn : value.projectNameEn,
            projectNameEs : value.projectNameEs,
            startDate : this.datePipe.transform(value.startDate,"yyyy-MM-dd"),
            endDate: this.datePipe.transform(value.endDate,"yyyy-MM-dd"),
            period :value.period,
            status :value.status,
        }
        
        if(this.addProjectform.valid){
            if (this.projectId > 0) {
                this.ngxService.start()
                this.projectApi.editProject(projectObj, this.projectId).subscribe(
                    (res: any) => {
                        this.ngxService.stop()
                        this.msg = res['message'];
                        this.router.navigate(['post-auth/project-list']);
                        //this.toast.success(this.msg)
                        this.toast.success(this.translate.instant('MESSAGE.PROJECT_EDIT_SUCCESS'));
                    },
                    (err) => {
                        this.ngxService.stop()
                        this.formSubmit = false;
                    });
            } else {
                this.ngxService.start()
                this.projectApi.addProject(projectObj).subscribe(
                    (res: any) => {
                        this.ngxService.stop()
                        this.msg = res['message'];
                        this.router.navigate(['post-auth/project-list']);
                        this.toast.success(this.translate.instant('MESSAGE.PROJECT_ADD_SUCCESS'));
                        this.addProjectform.reset();
                    },
                    (err) => {
                        this.ngxService.stop()
                        this.formSubmit = false;
                        this.msg = err['message'];
                        this.toast.error(this.msg)
                    }
                );
            }
        }
    }

    view() {
        this.ngxService.start()
        this.projectApi.view(this.projectId).subscribe(
            data => {
                this.ngxService.stop()
                if (this.projectId) {
                    this.addProjectform.setValue({
                        projectNameEn : data.projectNameEn,
                        projectNameEs : data.projectNameEs,
                        startDate : new Date(data.startDate),
                        endDate: new Date(data.endDate),
                        period :data.period,
                        status :data.status.toString(),
                    });
                } 
                this.addProjectform.updateValueAndValidity();
            }
        )
    }

    get projectNameEn() {
		return this.addProjectform.get('projectNameEn');
    }
    get projectNameEs() {
		return this.addProjectform.get('projectNameEs');
	}
	get startDate() {
		return this.addProjectform.get('startDate');
	}
	get endDate() {
		return this.addProjectform.get('endDate');
	}
	get period() {
		return this.addProjectform.get('period');
    }
    get status() {
		return this.addProjectform.get('status');
	}


} 
