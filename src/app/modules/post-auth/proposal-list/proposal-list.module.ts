import { NgModule } from '@angular/core';
import { SharedModule } from 'src/app/shared/shared.module';
import { ProposalListRouteModule } from './proposal-list.routes.module';
import { ProposalListComponent } from './proposal-list.component';
//import { EditProposalListComponent } from './edit-user-admin/edit-user-admin.component';
//import { AddProposalListComponent } from './add-user-admin/add-user-admin.component';
import { UserMasterService } from 'src/app/services/apis/usermaster';
import { RoleMasterService } from 'src/app/services/apis/role.service';
import { ProposalService } from 'src/app/services/apis/proposal.service';
//import { ViewAdminUserComponent } from './view-user-admin/view-user-admin.component';
import { EcoFabSpeedDialModule } from '@ecodev/fab-speed-dial';
import { MatSortModule } from '@angular/material';
import { NgxPaginationModule } from 'ngx-pagination';
@NgModule({
  imports: [
    ProposalListRouteModule,
    SharedModule,
    EcoFabSpeedDialModule,
    MatSortModule,
    NgxPaginationModule
  ],
  declarations: [
    ProposalListComponent,
    //AddProposalListComponent,
    //EditProposalListComponent,
    //ViewAdminUserComponent
  ],
  entryComponents: [
    ProposalListComponent,
    //AddProposalListComponent,
    //EditProposalListComponent,
    //ViewAdminUserComponent
  ],

  providers: [UserMasterService, RoleMasterService, ProposalService]
})
export class ProposalListModule { }
