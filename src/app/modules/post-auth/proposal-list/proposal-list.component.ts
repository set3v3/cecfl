import { AppConst } from 'src/app/app.constants';
import { Component, OnInit } from '@angular/core';
import { ProposalService } from 'src/app/services/apis/proposal.service';
import { ProposalListingApiResponce } from 'src/app/models/proposal';
import { Router, ActivatedRoute } from '@angular/router';
import { PermissionProvider } from 'src/app/services/permission/permission.service';
import { UserStorageProvider } from 'src/app/services/storage/user-storage.service';
import { NgxUiLoaderService, SPINNER } from 'ngx-ui-loader';
import { CommonService } from 'src/app/services/apis/common.service';
import { HttpHeaders } from '@angular/common/http';
import { ExcelService } from 'src/app/services/apis/excel.service';

@Component({
    selector: 'app-proposal-list',
    templateUrl: './proposal-list.component.html',
    styleUrls: ['./proposal-list.component.scss']
})

export class ProposalListComponent implements OnInit {
    spinnerType = SPINNER.rectangleBounce;
    loggedUserType: any;
    showMessage: string;
    proposallisting: any = {
        data: []
    }

    totalItem: number;
    page: number;
    pageindex: number;
    viewstatus: boolean;
    editstatus: boolean;
    createtatus: boolean;
    exportstatus: boolean;
    searchParam: any = {};
    fixVar: boolean;
    open: boolean;
    spin: boolean;
    direction: string;
    animationMode: string;
    bydefatseletedproposalType: any;
    proposalType: Array<any>;
    displayedColumns = ['proposal_unique_id', 'created_at', 'updated_at', 'application_status'];
    p: number = 1;
    limit: number = 10;
    itemsPerPage: number = 10;
    length;
    searchString;
    language;
    loggedUserRole: any;
    downloadParam:any ={}
    excelData;
    constructor(
        public router: Router,
        private proposalApi: ProposalService,
        private Permission: PermissionProvider,
        private userStorage: UserStorageProvider,
        private ngxService: NgxUiLoaderService,
        private common: CommonService,
        private excelService:ExcelService
    ) {
        this.loggedUserType = this.userStorage.get().user.user_type;
        this.proposallisting = [];
        this.showMessage = '';
        this.open = false;
        this.spin = false;
        this.direction = 'up';
        this.proposalType = AppConst.DATA_POTAL_USER_TYPE;
        this.animationMode = 'fling';
        this.bydefatseletedproposalType = 0;
        this.editstatus = true;//Permission.permission('proposal-list', 'edit');
        this.viewstatus = true;//Permission.permission('proposal-list', 'view');
        this.createtatus = true;//Permission.permission('proposal-list', 'create');
        this.exportstatus = true;//Permission.permission('proposal-list', 'export');
        if (this.editstatus) {
            this.displayedColumns.push('edit');
        }
        
        this.showMessage = '';
    }

    ngOnInit() {
        this.common.currLang.subscribe(
            res => {
                this.language = res
            }
        )
        this.loggedUserRole = this.userStorage.get().user.role.roleKey;
        this.searchParam.language = this.language;
        this.searchParam.page = this.p;
        this.searchParam.search = '';
        this.searchParam.user_id = this.userStorage.get().user.id;
        this.searchParam.user_type = this.userStorage.get().user.user_type;
        this.fetchProposalListing();
    }

    fetchProposalListing() {
        this.showMessage = '';
        this.ngxService.start();

        this.proposalApi.listProposal(this.searchParam).subscribe((res: ProposalListingApiResponce) => {
            this.ngxService.stop();
            if (res.success) {
                this.proposallisting = res.data;
                //console.log(this.proposallisting);
                this.length = res.data['total']
                this.totalItem = res.totalCount;
            }
        }, (err) => {
            this.ngxService.stop();
        });
    }

    paginationFeed(page) {
        this.p = page;
        this.searchParam.page = page
        this.fetchProposalListing();
    }

    goToLink(id: any) {
        localStorage.setItem('proposal_id', id);
        this.router.navigate(['post-auth/proposal-edit/' + id]);
    }

    goToView(id: any) {
        localStorage.setItem('proposal_id', id);
        this.router.navigate(['post-auth/proposal-view/' + id]);
    }

    goToReport(id: any) {
        localStorage.setItem('proposal_id', id);
        this.router.navigate(['post-auth/quaterly-reports/' + id]);
    }

    proposalSearch() {
        this.searchParam.search = this.searchString;
        this.fetchProposalListing();
    }

    exportAsXLSX() {
        this.searchParam.language = this.language;
        this.proposalApi.exportProposal(this.searchParam).subscribe(
            res => {
                this.excelData = res.excelData
                this.excelService.exportAsExcelFile(this.excelData, 'proposalList');
            }
        )
    }
    
}