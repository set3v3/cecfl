import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastProvider } from 'src/app/shared/modules/toast/toast.provider';
import { UserStorageProvider } from '../../../services/storage/user-storage.service';
import { DatePipe } from '@angular/common';
import { NgxUiLoaderService,SPINNER } from 'ngx-ui-loader';
import { CommonService } from 'src/app/services/apis/common.service';
import { TranslateService } from '@ngx-translate/core';
import { CmsService } from 'src/app/services/apis/cms.service';


@Component({
  selector: 'app-cms',
  templateUrl: './cms.component.html',
  styleUrls: ['./cms.component.scss']
})
export class CmsComponent implements OnInit {

    language;
    cmsForm: FormGroup;
    msg;
    successMsg: boolean = false;
    errorMsg: boolean = false;
    cmsId: number = 0;
    public formSubmit: boolean = false;
    spinnerType = SPINNER.rectangleBounce;
    constructor(
        private translate: TranslateService,
        private cmsApi: CmsService,
        public router: Router,
        public route: ActivatedRoute,
        private userStorage: UserStorageProvider,
        private toast: ToastProvider,
        private datePipe: DatePipe,
        private ngxService:NgxUiLoaderService,
        private common :CommonService
    ) {
        this.projectFormBuild();
    }

    ngOnInit() {
        this.route.params.subscribe(params => {
            if (params) {
                if (params['id']) {
                    this.cmsId = +params['id'];
                    if (this.cmsId > 0) {
                        this.view();
                    }
                }
            }
        });

        this.common.currLang.subscribe(
			res=>{
				this.language = res;
			}
        )
    }

    projectFormBuild() {
        this.cmsForm = new FormGroup({
            titleEn: new FormControl('', [ Validators.required]),
            titleEs:  new FormControl('', [ Validators.required]),
            descriptionEn: new FormControl('', [ Validators.required]),
            descriptionEs: new FormControl('', [ Validators.required]),
            link_en: new FormControl(''),
            link_es: new FormControl(''),
            status: new FormControl('', [ Validators.required]),
        });
    }

    addCms(value) {
        this.formSubmit = true;
        var cmsObj ={
            title_en : value.titleEn,
            title_es : value.titleEs,
            description_en :value.descriptionEn,
            description_es :value.descriptionEs,
            link_en :value.link_en,
            link_es :value.link_es,
            status :value.status,
        }
        
        if(this.cmsForm.valid){
            if (this.cmsId > 0) {
                this.ngxService.start()
                this.cmsApi.editCms(cmsObj, this.cmsId).subscribe(
                    (res: any) => {
                        this.ngxService.stop()
                        this.msg = res['message'];
                        this.router.navigate(['post-auth/cms-list']);
                        this.toast.success(this.msg)
                        //this.toast.success(this.translate.instant('MESSAGE.PROJECT_EDIT_SUCCESS'));
                    },
                    (err) => {
                        this.ngxService.stop()
                        this.formSubmit = false;
                    });
            } else {
                this.ngxService.start()
                this.cmsApi.addCms(cmsObj).subscribe(
                    (res: any) => {
                        this.ngxService.stop()
                        this.msg = res['message'];
                        this.router.navigate(['post-auth/cms-list']);
                        this.toast.success(this.msg);
                        this.cmsForm.reset();
                    },
                    (err) => {
                        this.ngxService.stop()
                        this.formSubmit = false;
                        this.msg = err['message'];
                        this.toast.error(this.msg)
                    }
                );
            }
        }
    }

    view() {
        this.ngxService.start()
        this.cmsApi.view(this.cmsId).subscribe(
            data => {
                this.ngxService.stop()
                if (this.cmsId) {
                    this.cmsForm.setValue({
                        titleEn : data.title_en,
                        titleEs : data.title_es,
                        descriptionEn :data.description_en,
                        descriptionEs :data.description_es,
                        link_en :data.link_en,
                        link_es :data.link_es,
                        status :data.status.toString(),
                    });
                } 
                this.cmsForm.updateValueAndValidity();
            }
        )
    }

    get titleEn() {
		return this.cmsForm.get('titleEn');
    }
    get titleEs() {
		return this.cmsForm.get('titleEs');
	}
	get descriptionEn() {
		return this.cmsForm.get('descriptionEn');
    }
    get descriptionEs() {
		return this.cmsForm.get('descriptionEs');
    }
    get status() {
		return this.cmsForm.get('status');
	}


} 
