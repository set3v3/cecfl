import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { AuthService } from '../../../services/apis/auth.service';
import { MatSortModule } from '@angular/material';
import { UserMasterService } from 'src/app/services/apis/usermaster';
import { CKEditorModule } from 'ngx-ckeditor';
import { ProjectService } from '../../../services/apis/project.service';
import { CmsComponent } from './cms.component';
import { OwlDateTimeModule, OwlNativeDateTimeModule,OWL_DATE_TIME_LOCALE } from 'ng-pick-datetime';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { CmsRouteModule } from './cms-route.module';
import { CmsService } from 'src/app/services/apis/cms.service';
export function HttpLoaderFactory(HttpClient: HttpClient) {
  return new TranslateHttpLoader(HttpClient,'../../../../assets/i18n/', '.json');
}
@NgModule({
  declarations: [
    CmsComponent
  ],
  imports: [
    CommonModule,
    CmsRouteModule,
    SharedModule,
    MatSortModule,
    CKEditorModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      
        
      }
    }),
  ],
  entryComponents: [
    CmsComponent,
  ],
  providers: [AuthService,
     UserMasterService,ProjectService,CmsService,
     { provide: OWL_DATE_TIME_LOCALE, useValue: 'en-SG'}
     ]
})
export class CmsModule { }
