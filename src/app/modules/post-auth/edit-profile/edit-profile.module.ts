import { NgModule } from '@angular/core';
import { SharedModule } from 'src/app/shared/shared.module';
import { AuthService } from '../../../services/apis/auth.service';
import { MatSortModule } from '@angular/material';
import { UserMasterService } from 'src/app/services/apis/usermaster';
import { CKEditorModule } from 'ngx-ckeditor';
import { RegistrationService } from '../../../services/apis/registration.service';
import { EditProfileComponent } from './edit-profile.component';
import { EditProfileRoutesModule } from './edit-profile-routes.module';
import { UserEditService } from 'src/app/services/apis/useredit.service';


@NgModule({
  imports: [
    EditProfileRoutesModule,
    SharedModule,
    MatSortModule,
    CKEditorModule

  ],
  declarations: [
    EditProfileComponent,
  ],
  entryComponents: [
    EditProfileComponent,
  ],
  providers: [AuthService, UserMasterService,RegistrationService,UserEditService]
})
export class EditProfileModule { }
