import { Component, OnInit, ViewChild } from '@angular/core';
import { RegistrationService } from '../../../services/apis/registration.service';
import { UserMasterService } from 'src/app/services/apis/usermaster';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ToastProvider } from '../../../shared/modules/toast/toast.provider';
import { UserEditService } from 'src/app/services/apis/useredit.service';
import { UserStorageProvider } from 'src/app/services/storage/user-storage.service';
import { NgxUiLoaderService,SPINNER } from 'ngx-ui-loader';
import { AppConst } from 'src/app/app.constants';

@Component({
    selector: 'app-edit-profile',
    templateUrl: './edit-profile.component.html',
    styleUrls: ['./edit-profile.component.scss']
})

export class EditProfileComponent implements OnInit {
    @ViewChild('fileInput', { static: false} ) fileInput;
    showLoader: boolean;
    public editorValue: string = '';
    addRegisterform: FormGroup;
    msg;
    userId: any;
    successMsg: boolean = false;
    errorMsg: boolean = false;
    startDate = new Date(1990, 0, 1);
    show: boolean = false;
    Citylisting: Array < any > ;
    searchParam: any;
    entityNameTitle: any;
    spinnerType = SPINNER.rectangleBounce;
    f1submitted: boolean;
    // postalAddress:boolean = false;
    minDate = new Date(1900, 0, 1);
    maxDate = new Date();
    imageUrl: string;
    allfiles: any = [];
    constructor(
        private router: Router,
        private toast: ToastProvider,
        private route: ActivatedRoute,
        private UserMasterApi: UserMasterService,
        private UsersupdateApi: UserEditService,
        private userStorage: UserStorageProvider,
        private ngxService:NgxUiLoaderService,

    ) {
        this.newsformbuild();
        this.Citylisting = [];
        this.getCityList();
        this.showLoader = false;
        this.imageUrl = '';
    }

    ngOnInit() {
        this.userId = this.userStorage.get().user.id;
        if (this.userId > 0) {
            this.getuserdetaiils();
        }
    }
    handleChangetax(value) {

        if (value == 1) {
            this.show = true;
            this.addRegisterform.get('taxexmptconfno').setValidators([Validators.required, Validators.pattern("^[a-zA-Z0-9\.\/\,\@\#\$\%\^\&\*\+\~\!\d./-]+(?: [a-zA-Z0-9\.\/\,\@\#\$\%\^\&\*\+\~\!\d./-]+)*$")]);
            this.addRegisterform.get('dateofissue').setValidators([Validators.required]);

        } else if (value == 0) {
            this.show = false;

            this.addRegisterform.get('taxexmptconfno').clearValidators();
            this.addRegisterform.get('taxexmptconfno').updateValueAndValidity();

            this.addRegisterform.get('dateofissue').clearValidators();
            this.addRegisterform.get('dateofissue').updateValueAndValidity();

            this.addRegisterform.patchValue({
                taxexmptconfno: '',
                dateofissue: ''
            });
        }
        this.addRegisterform.updateValueAndValidity();
    }
    get f1() { return this.addRegisterform.controls; }

    newsformbuild() {
        const emailRegx = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        const phoneNumber = "^(\+\d{1,3}[- ]?)?\d{10}$";
        const phoneRegx = /^[6-9]\d{9}$/;
        const addrRegx = "^[a-zñáéíóúüA-ZÑÁÉÍÓÚÜ0-9\.\/\,\@\#\$\%\^\&\*\+\~\!\d./-]+(?: [a-zñáéíóúüA-ZÑÁÉÍÓÚÜ0-9\.\/\,\@\#\$\%\^\&\*\+\~\!\d./-]+)*$";
        this.addRegisterform = new FormGroup({
            category: new FormControl('', [Validators.required]),

            name_of_director: new FormControl('', [Validators.required, Validators.pattern(addrRegx)]),

            entity_name: new FormControl('', [Validators.required, Validators.pattern("^[a-zñáéíóúüA-ZÑÁÉÍÓÚÜ0-9]+(?: [a-zñáéíóúüA-ZÑÁÉÍÓÚÜ0-9]+)*$")]),

            dateofregis: new FormControl('', [Validators.required]),

            dateofincorporation: new FormControl('', [Validators.required]),

            registrationnumber: new FormControl('', [Validators.required, Validators.pattern(addrRegx)]),

            taxexmpt: new FormControl('', [Validators.required]),

            email: new FormControl('', [Validators.required, Validators.pattern(emailRegx)]),

            phone: new FormControl('', [Validators.required, Validators.pattern(phoneRegx)]),

            taxexmptconfno: new FormControl(''),

            dateofissue: new FormControl(''),

            physicaladdr1: new FormControl('', [Validators.required, Validators.pattern(addrRegx)]),

            city: new FormControl('', [Validators.required]),

            state1: new FormControl('', [Validators.required]),

            zip: new FormControl('', [Validators.required]),

            postal_addr2: new FormControl('', [Validators.required, Validators.pattern(addrRegx)]),
            postal_city: new FormControl('', [Validators.required]),

            postal_state: new FormControl('', [Validators.required]),

            postal_zip: new FormControl('', [Validators.required]),

            activeStatus: new FormControl('', [Validators.required]),
            checkPostalAddress:new FormControl(''),
            service_offered:new FormControl(''),
            service_description:new FormControl(''),
            website:new FormControl(''),
            logo_name:new FormControl('')

        });
    }

    onSelectFile(event) {
        const files = event.target.files[0];
        let extFile = files.type
        console.log(extFile)
        if (event.target.files && event.target.files[0]) {
          if (extFile=="image/png" || extFile=="image/jpeg" || extFile=="image/jpg"){
            var reader = new FileReader();

            reader.readAsDataURL(event.target.files[0]);

            reader.onload = (event:any) => {
              console.log(event)
              this.imageUrl = event.target.result;
            }
          }else{
              this.fileInput.nativeElement.value = "";
              this.toast.error("Only jpg/jpeg and png files are allowed!");
          }

        }
      }

    addRegister = (value) => {
        value.id=this.userId
        this.f1submitted = true;
        
        if (this.addRegisterform.invalid) { this.toast.error('Please enter the required fields !!'); return; }
       
        const files: Array<File> = this.allfiles;
        const formData = new FormData();

        const fileBrowser = this.fileInput.nativeElement;
        if (fileBrowser.files && fileBrowser.files[0]) {
            formData.append('logo_name', fileBrowser.files[0]);
        }

        let d1=new Date(this.addRegisterform.value.dateofregis);
        let actualDate1=d1.getDate();
        let actualMonth1=d1.getMonth()+1;
        let actualYear1=d1.getFullYear();    
        let formattedDate1=actualDate1 + '-' + actualMonth1 + '-' + actualYear1;

        let d2=new Date(this.addRegisterform.value.dateofincorporation);
        let actualDate2=d2.getDate();
        let actualMonth2=d2.getMonth()+1;
        let actualYear2=d2.getFullYear();    
        let formattedDate2=actualDate2 + '-' + actualMonth2 + '-' + actualYear2;

        let d3=new Date(this.addRegisterform.value.dateofissue);
        let actualDate3=d3.getDate();
        let actualMonth3=d3.getMonth()+1;
        let actualYear3=d3.getFullYear();    
        let formattedDate3=actualDate3 + '-' + actualMonth3 + '-' + actualYear3;

        if (this.imageUrl != '') {   
            formData.append('id', value.id);       
            formData.append('category', value.category);
            formData.append('name_of_director', value.name_of_director);
            formData.append('entity_name', value.entity_name);
            formData.append('dateofregis', formattedDate1);
            formData.append('dateofincorporation', formattedDate2);
            formData.append('registrationnumber', value.registrationnumber);
            formData.append('taxexmpt', value.taxexmpt);
            formData.append('email', value.email);
            formData.append('phone', value.phone);
            formData.append('taxexmptconfno', value.taxexmptconfno);
            formData.append('dateofissue', formattedDate3);
            formData.append('physicaladdr1', value.physicaladdr1);
            formData.append('city', value.city);
            formData.append('state1', value.state1);
            formData.append('zip', value.zip);
            formData.append('activeStatus', value.activeStatus);
            formData.append('postal_addr2', value.postal_addr2);
            formData.append('postal_city', value.postal_city);
            formData.append('postal_state', value.postal_state);
            formData.append('postal_zip', value.postal_zip);
            formData.append('service_offered', value.service_offered);
            formData.append('service_description', value.service_description);
            formData.append('website', value.website);

            this.ngxService.start()
            this.UsersupdateApi.edituser(formData).subscribe(
                (res: any) => {
                    this.ngxService.stop()
                    this.showLoader = false;
                    this.successMsg = true;
                    this.msg = res['message'];
                    this.toast.success(this.msg); 
                    return; 
                },
                (err) => {
                    this.ngxService.stop();
                    this.msg = err['message'];
                    this.toast.success(this.msg); 
                    return; 
                }
            );
        }else{
            this.toast.error('Please provide logo !!');
        }
    }

    samefieldcopy(value) {
        if(value==true){
          this.addRegisterform.patchValue({
              postal_addr2: this.physicaladdr1.value,
              postal_city: this.city.value,
              postal_state: this.state1.value,
              postal_zip: this.zip.value
          })
        }else{
            this.addRegisterform.patchValue({
                postal_addr2: '',
                postal_city: '',
                postal_state:'',
                postal_zip: ''
            })
        }
    }


    getCityList() {
        this.searchParam = {
          search: 'PR',
        };
        this.UserMasterApi.citySearch(this.searchParam).subscribe((res: any) => {
            if (res.success) {
                this.Citylisting = res.data;
            }
        }, (err) => {});
    }

    get city() {
        return this.addRegisterform.get('city')
    }
    get category(){
        return this.addRegisterform.get('category')
      }
    get entity_name() {
        return this.addRegisterform.get('entity_name')
    }
    get name_of_director() {
        return this.addRegisterform.get('name_of_director')
    }
    get dateofincorporation() {
        return this.addRegisterform.get('dateofincorporation')
    }
    get dateofregis() {
        return this.addRegisterform.get('dateofregis')
    }
    get registrationnumber() {
        return this.addRegisterform.get('registrationnumber')
    }
    get taxexmpt() {
        return this.addRegisterform.get('taxexmpt')
    }
    get taxexmptconfno() {
        return this.addRegisterform.get('taxexmptconfno')
    }
    get dateofissue() {
        return this.addRegisterform.get('dateofissue')
    }
    get email() {
        return this.addRegisterform.get('email')
    }
    get phone() {
        return this.addRegisterform.get('phone')
    }
    get physicaladdr1() {
        return this.addRegisterform.get('physicaladdr1')
    }
    get state1() {
        return this.addRegisterform.get('state1')
    }
    get zip() {
        return this.addRegisterform.get('zip')
    }
    get postal_addr2() {
        return this.addRegisterform.get('postal_addr2')
    }
    get postal_city() {
        return this.addRegisterform.get('postal_city')
    }
    get postal_state() {
        return this.addRegisterform.get('postal_state')
    }

    get postal_zip() {
        return this.addRegisterform.get('postal_zip')
    }
    get activeStatus() {
        return this.addRegisterform.get('activeStatus')
    }
    get checkPostalAddress(){
        return this.addRegisterform.get('checkPostalAddress')
    }
    getuserdetaiils = () => {
        this.ngxService.start();
        this.UsersupdateApi.view(this.userId).subscribe(
            data => {
                this.ngxService.stop()
                this.handleChangetax(data.user.taxexmpt)
                this.entityNameTitle = data.user.entity_name;
                this.imageUrl = AppConst.API_BASE_URL + '/upload/entity_logo/' + data.user.logo_name;
                if( data.user.physicaladdr1 == data.user.postal_addr2 && data.user.city == data.user.postal_city && data.user.postal_state == data.user.postal_state && data.user.zip ==data.user.postal_zip){
                    // this.postalAddress = true
                    this.addRegisterform.patchValue({checkPostalAddress:true})
                }else{
                    this.addRegisterform.patchValue({checkPostalAddress:false})
                    // this.postalAddress = false
                }
                this.addRegisterform.patchValue({
                    id:this.userId,
                    category: data.user.category,
                    name_of_director: data.user.name_of_director,
                    entity_name: data.user.entity_name,
                    dateofincorporation: new Date(data.user.dateofincorporation).toISOString(),
                    dateofregis: new Date(data.user.dateofregis),
                    registrationnumber: data.user.registrationnumber,
                    taxexmpt: data.user.taxexmpt,
                    taxexmptconfno: data.user.taxexmptconfno,
                    dateofissue: new Date(data.user.dateofissue),
                    email: data.user.email,
                    phone: data.user.phone,
                    physicaladdr1: data.user.physicaladdr1,
                    city: data.user.city,
                    state1: data.user.postal_state,
                    zip: data.user.zip,
                    postal_addr2: data.user.postal_addr2,
                    postal_city: data.user.postal_city,
                    postal_state: data.user.postal_state,
                    postal_zip: data.user.postal_zip,
                    activeStatus: data.user.activeStatus.toString(),
                    service_description: data.user.service_description,
                    service_offered: data.user.service_offered,
                    website: data.user.website
                });
            }
        )
    }

    onActivate(event) {
        window.scroll(0,0);
    }

    

      

}
