import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/apis/auth.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastProvider } from 'src/app/shared/modules/toast/toast.provider';
import { ConsoleProvider } from 'src/app/shared/modules/console/console.provider';
import { UserStorageProvider } from 'src/app/services/storage/user-storage.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

    public loginForm: FormGroup;
    public loading: boolean;
    public referer: string;

    constructor(
        private translate: TranslateService,
        private fb: FormBuilder,
        private authProvider: AuthService,
        private router: Router,
        private route: ActivatedRoute,
        private toast: ToastProvider,
        private consoleProvider: ConsoleProvider,
        private userStorage: UserStorageProvider
    ) {
        this.loading = false;
        this.buildLoginForm();
    }

    ngOnInit() {}

    buildLoginForm() {
        const emailRegx = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        this.loginForm = this.fb.group({
            email: ['', [Validators.required, Validators.pattern(emailRegx)]],
            password: ['', Validators.required],
            rememberMe: ['',],
        });
    }

    /*
     * function use for  dologin
     * @ param userName ,userType,password
     * @memberof LoginComponent
     */
    onSubmit() {
        this.loading = true;
        this.authProvider.dologin(this.loginForm.value).subscribe((responseData: any) => {
            this.loading = false;
            if (responseData.success) {
                //this.toast.success(responseData.message);
                //this.toast.success(this.translate.instant('MESSAGE.LOGIN_SUCCESS'));
                this.userStorage.clear();
                this.userStorage.set(responseData);
                if (responseData.user.user_type == 0) {
                    this.router.navigate(['post-auth/dashboard']);
                } else {
                    this.router.navigate(['post-auth/dashboard']);
                }
            }
        }, error => {
            this.loading = false;
        });
    }

}
