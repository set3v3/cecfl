import { Component, OnInit } from '@angular/core';
import { RegistrationService } from 'src/app/services/apis/registration.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ToastProvider } from 'src/app/shared/modules/toast/toast.provider';
import { UserMasterService } from 'src/app/services/apis/usermaster';
import { NgxUiLoaderService,SPINNER } from 'ngx-ui-loader';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-registered',
  templateUrl: './registered.component.html',
  styleUrls: ['./registered.component.scss'],
  providers: [RegistrationService, UserMasterService]
})

export class RegisteredComponent implements OnInit {
  public editorValue: string = '';
  addRegisterform: FormGroup;
  msg;
  successMsg: boolean = false;
  errorMsg: boolean = false;
  startDate = new Date(1990, 0, 1);
  show: boolean = false;
  f1submitted: boolean;
  searchParam: any;
  Citylisting: Array < any > ;
  minDate = new Date(1900, 0, 1);
  maxDate = new Date();
  spinnerType = SPINNER.rectangleBounce;
  
  constructor(
      private translate: TranslateService,
      private registrationApi: RegistrationService,
      private UserMasterApi: UserMasterService,
      public router: Router,
      private ngxService:NgxUiLoaderService,
      private toast: ToastProvider
  ) {
      this.newsformbuild();
      this.Citylisting = [];
      this.getCityList();
  }

  ngOnInit() {
      ///this.register();
      

  }

  handleChangetax(value) {

      if (value == 1) {
          this.show = true;
          this.addRegisterform.addControl('taxexmptconfno',new FormControl('', [ Validators.required, Validators.pattern("^[a-zA-Z0-9\.\/\,\@\#\$\%\^\&\*\+\~\!\d./-]+(?: [a-zA-Z0-9\.\/\,\@\#\$\%\^\&\*\+\~\!\d./-]+)*$") ]) );
          this.addRegisterform.addControl('dateofissue',new FormControl('', [ Validators.required]) );
      } else if (value == 0) {
          this.show = false;
          this.addRegisterform.removeControl('taxexmptconfno');
          this.addRegisterform.removeControl('dateofissue')
          console.log('false')
      }
  }
  

 // get f1() { return this.addRegisterform.controls; };
  newsformbuild() {
      const emailRegx = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      const phoneNumber = "^(\+\d{1,3}[- ]?)?\d{10}$";
      const addmissiondirector = /[a-zA-Z]/g;
      const phoneRegx = /^[6-9]\d{9}$/;
      const addrRegx = "^[a-zñáéíóúüA-ZÑÁÉÍÓÚÜ0-9\.\/\,\@\#\$\%\^\&\*\+\~\!\d./-]+(?: [a-zñáéíóúüA-ZÑÁÉÍÓÚÜ0-9\.\/\,\@\#\$\%\^\&\*\+\~\!\d./-]+)*$";
      this.addRegisterform = new FormGroup({

          category: new FormControl('', [Validators.required]),

          name_of_director: new FormControl('', [Validators.required, Validators.pattern(addrRegx) ]),

          entity_name: new FormControl('', [Validators.required, Validators.pattern("^[a-zñáéíóúüA-ZÑÁÉÍÓÚÜ0-9]+(?: [a-zñáéíóúüA-ZÑÁÉÍÓÚÜ0-9]+)*$") ]),

          dateofregis: new FormControl('', [Validators.required ]),

          dateofincorporation: new FormControl('', [Validators.required]),

          registrationnumber: new FormControl('', [ Validators.required, Validators.pattern(addrRegx) ]),

          taxexmpt: new FormControl('', [Validators.required]),

          email: new FormControl('', [ Validators.required, Validators.pattern(emailRegx) ]),

          phone: new FormControl('', [ Validators.required, Validators.pattern(phoneRegx) ]),


          physicaladdr1: new FormControl('', [Validators.required, Validators.pattern(addrRegx) ]),

          city: new FormControl('', [ Validators.required]),

          state1: new FormControl('', [ Validators.required ]),

          zip: new FormControl('', [ Validators.required ]),

          postal_addr2: new FormControl('', [ Validators.required, Validators.pattern(addrRegx) ]),
          postal_city: new FormControl('', [Validators.required]),

          postal_state: new FormControl('', [Validators.required ]),

          postal_zip: new FormControl('', [Validators.required ]),

      });
  }

  addRegister = (value) => {
    //console.log('Here!!!!')
      //console.log(this.addRegisterform)
      this.f1submitted = true;
      if (this.addRegisterform.valid) {
        this.ngxService.start();
          this.registrationApi.register(value).subscribe(
              (res: any) => {
                  //console.log(res);
                  this.ngxService.stop();
                  this.toast.success(this.translate.instant('MESSAGE.REGISTRATION_SUCCESS'));
                  this.router.navigate(['/']);
              },
              (err) => {
                this.ngxService.stop();
                this.msg = err['message'];
                this.toast.success(this.msg); return; 
            }
          );
      }
      
  }
  samefieldcopy(value) {
      console.log(value)
      console.log('value.target.value' ,value)
      if(value==true){
        this.addRegisterform.patchValue({
            postal_addr2: this.physicaladdr1.value,
            postal_city: this.city.value,
            postal_state: this.state1.value,
            postal_zip: this.zip.value
        })
      }else{
        this.addRegisterform.patchValue({
            postal_addr2: '',
            postal_city: '',
            postal_state:'',
            postal_zip: ''
        })
      }
      
  }

  getCityList() {
    this.searchParam = {
      search: 'PR',
    };
    this.UserMasterApi.citySearch(this.searchParam).subscribe((res: any) => {
      if (res.success) {
        this.Citylisting = res.data;
      }
    }, (err) => {});
  }

  get city() {
      return this.addRegisterform.get('city')
  }
  get category(){
    return this.addRegisterform.get('category')
  }
  get entity_name(){
      return this.addRegisterform.get('entity_name')
  }
  get name_of_director(){
      return this.addRegisterform.get('name_of_director')
  }
  get dateofincorporation(){
      return this.addRegisterform.get('dateofincorporation')
  }
  get dateofregis(){
      return this.addRegisterform.get('dateofregis')
  }
  get registrationnumber(){
      return this.addRegisterform.get('registrationnumber')
  }
  get taxexmpt(){
      return this.addRegisterform.get('taxexmpt')
  }

  get taxexmptconfno(){
      return this.addRegisterform.get('taxexmptconfno')
  }
  get dateofissue(){
      return this.addRegisterform.get('dateofissue')
  }
  get email(){
      return this.addRegisterform.get('email')
  }

  get phone(){
      return this.addRegisterform.get('phone')
  }

  get physicaladdr1(){
      return this.addRegisterform.get('physicaladdr1')
  }

  get state1() {
      return this.addRegisterform.get('state1')
  }

  get zip() {
      return this.addRegisterform.get('zip')
  }

  get postal_addr2() {
      return this.addRegisterform.get('postal_addr2')
  }

  get postal_city() {
      return this.addRegisterform.get('postal_city')
  }

  get postal_state() {
      return this.addRegisterform.get('postal_state')
  }

  get postal_zip() {
      return this.addRegisterform.get('postal_zip')
  }

  onActivate(event) {
    window.scroll(0,0);
  }
  
}









