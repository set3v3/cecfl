import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, NgForm, AbstractControl, ValidatorFn } from '@angular/forms';
import { AuthService } from '../../../services/apis/auth.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastProvider } from '../../../shared/modules/toast/toast.provider';
import { equalvalidator } from '../../../directives/validators/equal-validator';
import { ForGotPasswordApiResponce } from 'src/app/models/auth';
import { NgxUiLoaderService,SPINNER } from 'ngx-ui-loader';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';


@Component({
    selector: 'app-forgot-password',
    templateUrl: './forgot-password.component.html',
    styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit {

    public forgotPasswordForm: FormGroup;
    public resetPasswordForm: FormGroup;
    public loading: boolean;
    forgetPasswordParam:any ={}
    spinnerType = SPINNER.rectangleBounce;

    constructor(
        private fb: FormBuilder,
        private authProvider: AuthService,
        private router: Router,
        private toast: ToastProvider,
        private ngxService:NgxUiLoaderService
    ) {
        // set default value of properties
        this.loading = false;

        // invoke Building forgotPasswordForm form
        this.buildForgotPasswordForm();
    }
    ngOnInit() {

    }


    buildForgotPasswordForm() {
        /* tslint:disable */
        const emailRegx = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        /* tslint:enable */
        this.forgotPasswordForm = this.fb.group({
            email: ['', [Validators.required, Validators.pattern(emailRegx)]]
        });

    }

    onForgotPasswordSubmit(value) {
        this.ngxService.start()
        this.authProvider.forgotPassword(value).subscribe((responseData: ForGotPasswordApiResponce) => {
            this.ngxService.stop()
            if (responseData.success) {
                this.toast.success(responseData.message);
                this.router.navigate(['/pre-auth/login']);
            }else{
                this.toast.success(responseData.message);
            }
        }, error => {
            this.ngxService.stop()
        });

    }
}
