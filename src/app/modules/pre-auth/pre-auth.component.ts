import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd} from '@angular/router';

@Component({
  selector: 'app-pre-auth',
  template: `<router-outlet></router-outlet>`
  //templateUrl: './pre-auth.component.html',
  //styleUrls: ['./pre-auth.component.scss']
})
export class PreAuthComponent implements OnInit {
	showHead: boolean = false;
	constructor( public router: Router) { 
		router.events.forEach((event) => {
		  if (event instanceof NavigationEnd) {
			const urarry = event['url'].split('?');
			const url = urarry[0];
		    if (url == '/pre-auth/login' || url == '/') {
		      this.showHead = false;
		    } else {
		      this.showHead = true;
		    }
		  }
		});
	}

  ngOnInit() {
  }

}
