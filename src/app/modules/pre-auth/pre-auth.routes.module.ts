import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PreAuthComponent } from './pre-auth.component';
import { SessionGuard } from '../../services/guards/session-guard.service';
import { LoginComponent } from './login/login.component';
import { RegisteredComponent } from './registered/registered.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { VerifyComponent } from './verify/verify.component';
import { AccountVerifyComponent } from './account-verify/account-verify.component';


const routerConfig: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  {
    path: '',
    component: PreAuthComponent,
    children: [
      { path: 'login', component: LoginComponent },
      { path: 'registered', component: RegisteredComponent },
      { path: 'forgot-password', component: ForgotPasswordComponent },
      { path: 'forgotpasswordverify/:id/:token', component: VerifyComponent },
      { path: 'accountverify/:id/:token', component: AccountVerifyComponent },

    ],
    canActivate: [SessionGuard]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routerConfig)
  ],
  exports: [RouterModule],
  providers: [SessionGuard]
})
export class PreAuthRouteModule {
}
