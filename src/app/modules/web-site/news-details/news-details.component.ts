import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CommonService } from 'src/app/services/apis/common.service';
import { DashBordService } from 'src/app/services/apis/dashbord.service';
@Component({
  selector: 'app-news-details',
  templateUrl: './news-details.component.html',
  styleUrls: ['./news-details.component.scss']
})
export class NewsDetailsComponent implements OnInit {
  newsData:any = []
  newsParam:any = {};
  language
  constructor(
    private myRoute: Router,
    private common:CommonService,
    private dasboardApi:DashBordService,
  ) { }

  ngOnInit() {
    this.common.currLang.subscribe(
      res=>{
          this.language = res
      }
  )
  this.getCmsContent()
}

  getCmsContent(){
    this.newsParam.short_code='news-details'
    this.dasboardApi.getCmsContent(this.newsParam).subscribe(
        res=>{
            this.newsData = res
        },
        error=>{

        }
    )
  }

}
