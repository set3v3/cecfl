import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { webSiteComponent } from './web-site.component';
import { HomeComponent } from './home/home.component';
import { SessionGuard } from '../../services/guards/session-guard.service';
import { OrganizationChartComponent } from './organization-chart/organization-chart.component';
import { SearchComponent } from './search/search.component';
import { SearchDetailsComponent } from './search-details/search-details.component';
import { SearchListViewComponent } from './search-list-view/search-list-view.component';
import { SearchMapViewComponent } from './search-map-view/search-map-view.component';
import { OurCommissionComponent } from './our-commission/our-commission.component';
import { FaqDetailsComponent } from './faq-details/faq-details.component';
import { NewsDetailsComponent } from './news-details/news-details.component';

const routerConfig: Routes = [
  //{ path: '', redirectTo: 'home', pathMatch: 'full' },
  {
    path: '',
    component: webSiteComponent,
    children: [
      { path: '', loadChildren: () => import('./home/home.module').then(m => m.HomeModule) },
      { path: 'organization-chart', component: OrganizationChartComponent },
      { path: 'search', component: SearchComponent },
      { path: 'search-details', component: SearchDetailsComponent },

      //{ path: 'search-list-view/:cId/:category', component: SearchListViewComponent },
      //{ path: 'search-map-view/:cId/:category', component: SearchMapViewComponent },

      { path: 'search-list-view/:category/:cId', component: SearchListViewComponent },
      { path: 'search-map-view/:category/:cId', component: SearchMapViewComponent },
      { path: 'our-commission', component: OurCommissionComponent },
      { path: 'faq-details', component: FaqDetailsComponent },
      { path: 'news-details', component: NewsDetailsComponent },
    ],
    //canActivate: [SessionGuard]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routerConfig)
  ],
  exports: [RouterModule],
   //providers: [SessionGuard]
})
export class WebsiteRouteModule {
}

