import { NgModule } from '@angular/core';
// import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { ComponentsModule } from '../../components/components.module';
import { AppMaterialModule } from '../../app.material.module';
import { AuthService } from '../../services/apis/auth.service';
import { SharedModule } from '../../shared/shared.module';
import { NgxMaskModule } from 'ngx-mask';
import { FormsModule } from '@angular/forms';
import { OwlModule } from 'ngx-owl-carousel';
import { ChartsModule } from 'ng2-charts';
import { PermissionProvider } from 'src/app/services/permission/permission.service';
import { NgxUiLoaderModule } from 'ngx-ui-loader';
import { WebsiteRouteModule } from './web-site.routes.module';
import { webSiteComponent } from './web-site.component';
import { SearchComponent } from './search/search.component';
import { SearchMapViewComponent } from './search-map-view/search-map-view.component';
import { SearchListViewComponent } from './search-list-view/search-list-view.component';
import { SearchDetailsComponent } from './search-details/search-details.component';
import { OurCommissionComponent } from './our-commission/our-commission.component';
import { OrganizationChartComponent } from './organization-chart/organization-chart.component';
import { InnearHeaderComponent } from '../../components/innear-header/innear-header.component';
import { NewsService } from 'src/app/services/apis/news.service';
import { HttpClient } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { AppConst } from 'src/app/app.constants';
import { CommonService } from 'src/app/services/apis/common.service';
import { ProjectService } from 'src/app/services/apis/project.service';
import { DashBordService } from 'src/app/services/apis/dashbord.service';

export function HttpLoaderFactory(HttpClient: HttpClient) {
  return new TranslateHttpLoader(HttpClient,AppConst.PROJECT_FOLDER+'assets/i18n/', '.json');
}




@NgModule({
  imports: [
    WebsiteRouteModule,
    ComponentsModule,
    AppMaterialModule,
    SharedModule,
    NgxUiLoaderModule,
    NgxMaskModule.forRoot(),
    FormsModule,
    OwlModule,
    ChartsModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      
        
      }
    }),
   
      
  ],
  declarations: [
    webSiteComponent , SearchComponent ,
    SearchMapViewComponent,
    SearchListViewComponent,
    OurCommissionComponent,
    OrganizationChartComponent,
  ],
  providers: [AuthService, NewsService, PermissionProvider, CommonService, ProjectService,DashBordService],
  entryComponents: [
    
  ],

})
export class WebSiteModule { }
