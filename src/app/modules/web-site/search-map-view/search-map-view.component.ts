import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { UserMasterService } from 'src/app/services/apis/usermaster';
import { NgxUiLoaderService, SPINNER } from 'ngx-ui-loader';
declare var google: any;
@Component({
  selector: 'app-search-map-view',
  templateUrl: './search-map-view.component.html',
  styleUrls: ['./search-map-view.component.scss']
})
export class SearchMapViewComponent implements OnInit {
  cityId : string;
  category : string;
  showNoData : boolean = false;
  spinnerType = SPINNER.rectangleBounce;
  locations : Array<any>;
  constructor(
    private route: ActivatedRoute,
    private UserMasterApi: UserMasterService,
    private ngxService :NgxUiLoaderService,
    private router: Router,
  ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.cityId = params['cId'];
      this.category = params['category'];
  })
    this.fetchListdata();
    this.locations = [];    
  }
  citySrchFunc(data){
    this.cityId = data.city;
    this.category = data.category;
    this.fetchListdata();
  }

  fetchListdata() {
    this.ngxService.start();
    this.UserMasterApi.getSearchList(this.cityId,this.category).subscribe((res) => {
      if (res.success) {        
        if(res.data.length > 0){
          for(let i=0;i<res.data.length;i++){
            this.locations = res.data
            // const latLng = {lat : res.data[i].latitude,lng : res.data[i].longitude};
            // this.locations.push(latLng);            
          } 
          this.showNoData = false;
          setTimeout(() => {
            this.loadMap();  
          }, 200);                
          
        }else{
          this.locations = [];
          this.showNoData = true;
        }
        setTimeout(() => {
          this.ngxService.stop();
        }, 1000);        
      }
    }, error => {
      this.ngxService.stop();
      this.locations = [];
      this.showNoData = true;
    });
  }
  loadMap() {
    let map = new google.maps.Map(document.getElementById('map'), {
      zoom: 10,
      center: new google.maps.LatLng(this.locations[0].latitude, this.locations[0].longitude),
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });
    let marker, i;
    for (i = 0; i < this.locations.length; i++) { 
      marker = new google.maps.Marker({
        position: new google.maps.LatLng(this.locations[i].latitude, this.locations[i].longitude),
        map: map
      });
      var self = this;
      google.maps.event.addListener(marker, 'click', (function(marker, i) {
        return function() {
          self.router.navigateByUrl('/search-details', { state: self.locations[i] });
        }
      })(marker, i));
    }
    
  }
}
