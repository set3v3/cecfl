import { Component, OnInit, OnChanges } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { UserMasterService } from 'src/app/services/apis/usermaster';
import { NgxUiLoaderService, SPINNER } from 'ngx-ui-loader';
import { AppConst } from 'src/app/app.constants';
import { CommonService } from 'src/app/services/apis/common.service';

@Component({
  selector: 'app-search-list-view',
  templateUrl: './search-list-view.component.html',
  styleUrls: ['./search-list-view.component.scss']
})
export class SearchListViewComponent implements OnInit {
  language;
  cityId : string;
  category : string;
  searchList : Array<any>;
  showNoData : boolean = false;
  imageUrl : string;
  spinnerType = SPINNER.rectangleBounce;
  constructor(
    private route: ActivatedRoute,
    private UserMasterApi: UserMasterService,
    private router: Router,
    private ngxService :NgxUiLoaderService,
    private common: CommonService,
  ) {
    this.searchList =[];
   }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.cityId = params['cId'];
      this.category = params['category'];

      this.common.currLang.subscribe(
        res =>{
          this.language = res
        }
      )
  })
    this.imageUrl = AppConst.API_BASE_URL + '/upload/entity_logo/';
    this.fetchListdata();
  }
  citySrchFunc(data){
    this.cityId = data.city;
    this.category = data.category;
    this.fetchListdata();
  }

  fetchListdata() {
    this.ngxService.start();
    this.UserMasterApi.getSearchList(this.cityId,this.category).subscribe((res) => {
      if (res.success) {        
        if(res.data.length > 0){
          this.searchList = res.data;
          this.showNoData = false;
        }else{
          this.searchList = [];
          this.showNoData = true;
        }
        this.ngxService.stop();
      }
    }, error => {
      this.ngxService.stop();
      this.searchList = [];
      this.showNoData = true;
    });
  }
  gotoDetails(data){
    this.router.navigateByUrl('/search-details', { state: data });
  }

  jsonDecode(item) {
    return JSON.parse(item);
  }

}
