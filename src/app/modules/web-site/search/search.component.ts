import { Component, OnInit } from '@angular/core';
import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import { Label } from 'ng2-charts';
import { UserMasterService } from 'src/app/services/apis/usermaster';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {
  barChart:boolean = false;
  public StaticColor: any;
  public barchartObj: any;
  Citylisting: Array < any > ;
  barData: Array < any > ;
  totOrg: any;
  searchParam: any;
  searchCity: any;
  constructor(
    private UserMasterApi: UserMasterService,
    ) { 
    this.barchartObj = {};
    this.Citylisting = [];
    this.getCityList();
    this.getCategoryReportChart();
    this.StaticColor = ['#5271ba', '#3da3e8', '#51c0bf', '#fbb03b', '#f47b50',
    '#5271ba', '#3da3e8', '#51c0bf', '#fbb03b', '#f47b50',
    '#5271ba', '#3da3e8', '#51c0bf', '#fbb03b', '#f47b50',
    '#5271ba', '#3da3e8', '#51c0bf', '#fbb03b', '#f47b50',
    '#5271ba', '#3da3e8', '#51c0bf', '#fbb03b', '#f47b50',
    '#5271ba', '#3da3e8', '#51c0bf', '#fbb03b', '#f47b50',
    ];
  }
  ngOnInit() {}
  getCityList() {
    this.searchParam = {
      search: 'PR',
    };
    this.UserMasterApi.citySearch(this.searchParam).subscribe((res: any) => {
      if (res.success) {
        this.Citylisting = res.data;
      }
    }, (err) => {});
  }

  cityChangeEvent(v) {
    this.searchCity = {
      search: v,
    };
    this.getCategoryReportChart();
  }

  getCategoryReportChart() {
    this.UserMasterApi.getCategoryReport(this.searchCity).subscribe((res: any) => {
      if (res.success) {
        this.barData = res.data;
        this.totOrg = res.totalOrg;
        var labelList = [];
        var dataList = [];

        for (let item of res.data) {
          labelList.push(item.category);
          dataList.push(item.tot);
        }
        this.setBarChart(labelList, dataList);
      }
    }, (err) => {});
  }

  setBarChart(label?, datas?) {
      this.barChart =  true;
      this.barchartObj.barChartOptions = {
          scaleShowVerticalLines: false,
          responsive: true,
          legend: false,
          scales: {
              xAxes: [{
                  barPercentage: 1,
                  barThickness: 25,
                  maxBarThickness: 35,
                  minBarLength: 2,
                  gridLines: {
                    offsetGridLines: false,
                    display: false
                  }
              }]
          }
      };
      this.barchartObj.barChartLabels = label ? label : [];
      this.barchartObj.barChartType = 'bar';
      this.barchartObj.barChartLegend = true;
      this.barchartObj.barChartPlugins = [];
      this.barchartObj.barChartData = [
        {
          data: datas ? datas : [],
          backgroundColor: this.StaticColor,
          hoverBackgroundColor: this.StaticColor
        }
      ];
  }

}
