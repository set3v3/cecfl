import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CommonService } from 'src/app/services/apis/common.service';
import { DashBordService } from 'src/app/services/apis/dashbord.service';
import { DomSanitizer } from '@angular/platform-browser';
import * as $ from 'jquery';

@Component({
  selector: 'app-video',
  templateUrl: './video.component.html',
  styleUrls: ['./video.component.scss']
})
export class VideoComponent implements OnInit {
	cmsData:any = []
	cmsParam:any = {};
	language;
	safeURL_en;
	safeURL_es;
  constructor(
  	private myRoute: Router,
    private common:CommonService,
    private dasboardApi:DashBordService,
    private _sanitizer: DomSanitizer
  	) { }

  ngOnInit() {
    this.common.currLang.subscribe(
        res=>{
            this.language = res
        }
    )
    this.getCmsContent();
  }

  getCmsContent(){
    this.cmsParam.short_code='video'
    this.dasboardApi.getCmsContent(this.cmsParam).subscribe( (res) => {
            this.cmsData = res[0];
        	this.safeURL_en = this._sanitizer.bypassSecurityTrustResourceUrl(this.cmsData.link_en)
        	this.safeURL_es = this._sanitizer.bypassSecurityTrustResourceUrl(this.cmsData.link_es)
        },
        error=>{}
    )
  }

 
}
