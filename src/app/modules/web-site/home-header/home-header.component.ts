import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CommonService } from 'src/app/services/apis/common.service';
import { TranslateService } from '@ngx-translate/core';
import { ProjectService } from 'src/app/services/apis/project.service';
import { UserStorageProvider } from 'src/app/services/storage/user-storage.service';
import { AppConst } from 'src/app/app.constants';
import { interval } from 'rxjs';
import { map } from 'rxjs/operators'




@Component({
  selector: 'app-home-header',
  templateUrl: './home-header.component.html',
  styleUrls: ['./home-header.component.scss']
})
export class HomeHeaderComponent implements OnInit {

  showSearch: boolean = false;
  showTopNav: boolean = true;
  language;
  projectParam : any={}
  projectData : any=[]
  userInfo: any;
  projectUrl: any;
  remainDays : any;
  remainHrs : any;
  remainMin : any;
  remainSec : any;
  constructor( 
    private myRoute: Router,
    private common: CommonService,
    private translateService: TranslateService,
    private projectApi: ProjectService,
    private userStorage: UserStorageProvider,
    private router: Router,
  ) { 
    this.userInfo = this.userStorage.get();
    this.projectUrl = AppConst.PROJECT_FOLDER;
  }

  ngOnInit() {


    this.common.currLang.subscribe(
      res =>{
        this.language = res
      }
    )

    this.latestProject()

    // window.onresize = function() {
    //   if (window.innerWidth  < 767) { 
    //     document.getElementById('menu').style.display = 'none';
    //   }
    // }
    

  }
  

  //mobile menu toggle
  menuToggle() {
    var x = document.getElementById("menu");
    if (x.style.display === "block") {
      x.style.display = "none";
    } else {
      x.style.display = "block";
    }
  } 


  // Top Search Show Hide
  toggleSearch() {
    this.showSearch = !this.showSearch;
    this.showTopNav = !this.showTopNav;
  }




  navigateRegister(){
    this.myRoute.navigateByUrl('/pre-auth/registered')
  }
  navigateLogin(){
    this.myRoute.navigateByUrl('/pre-auth/login')
  }


  changeLang(language: string) {
		this.language = language;
		this.common.currLang.next(this.language);
    this.translateService.use(this.language);
  }


  latestProject(){
    this.projectParam.page = 1;
    this.projectApi.activeProject().subscribe(
      res=>{
        
        this.projectData=res.data;        
        let self = this;
        setInterval(function(){ 
          let date1 ;
          let date2 ;  
          date1 = new Date(self.projectData[0].endDate);
          date2 = new Date();  
          let res = Math.abs(date1 - date2) / 1000;            
          // get total days between two dates
          self.remainDays = Math.floor(res / 86400);                                 
          // get hours        
          self.remainHrs = Math.floor(res / 3600) % 24;               
          // get minutes
          self.remainMin = Math.floor(res / 60) % 60;          
          // get seconds
          self.remainSec = Math.floor(res % 60);  
        }, 1000);
      }
    )
  }
}
