import { Component, OnInit } from '@angular/core';
import { NewsService } from 'src/app/services/apis/news.service';
import { HttpModule } from '@angular/http';
import { AppConst } from 'src/app/app.constants';
import { CommonService } from 'src/app/services/apis/common.service';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.scss']
})
export class NewsComponent implements OnInit {

  newsParam : any={}
  newsData : any=[]
  language;

  constructor(
    private newsApi: NewsService,
    private common: CommonService,
  ) { }

  ngOnInit() {
    this.common.currLang.subscribe(
      res=>{
        this.language = res
      }
    )
    this.latestNews()
  }

  getImgUrl(imagename) {
    // console.log(imagename);
    if (imagename && imagename !== '') {
      return AppConst.API_BASE_URL + '/' + imagename;
    } else {
      return 'assets/images/no-img.png';
    }

  }

  latestNews(){
    this.newsParam.page = 1;
    this.newsApi.activenews(this.newsParam).subscribe(
      res=>{
        //console.log(res);
        this.newsData=res.data
      }
    )
  }

}
