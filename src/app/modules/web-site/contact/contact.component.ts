import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ToastProvider } from 'src/app/shared/modules/toast/toast.provider';
import { CommonService } from 'src/app/services/apis/common.service';
import { TranslateService } from '@ngx-translate/core';
import { UserMasterService } from 'src/app/services/apis/usermaster';
import { NgxUiLoaderService, SPINNER } from 'ngx-ui-loader';
import { UserStorageProvider } from 'src/app/services/storage/user-storage.service';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {

  public Citylisting: Array < any > ;
  public searchParam: any;
  public addContactform: FormGroup;
  public language;
  public formSubmit: boolean = false;
  public spinnerType = SPINNER.rectangleBounce;
  constructor(
    private myRoute: Router,
    private common: CommonService,
    private translate: TranslateService,
    private UserMasterApi: UserMasterService,
    private router: Router,
    private ngxService :NgxUiLoaderService,
    private userStorage: UserStorageProvider,
    private toast: ToastProvider,
  ) {
    this.Citylisting = [];
    this.contactFormBuild();
   }

  ngOnInit() {
    this.getCityList();
    this.common.currLang.subscribe(
      res =>{
        this.language = res
      }
    )
  }

  getCityList() {
    if(this.userStorage.getCity()){
      this.Citylisting = this.userStorage.getCity();
    }else{
      this.searchParam = {
        search: 'PR',
      };
      this.ngxService.start();
      this.UserMasterApi.citySearch(this.searchParam).subscribe((res: any) => {
        if (res.success) {
          this.userStorage.setCity(res.data);
          this.Citylisting = res.data;
          this.ngxService.stop();
        }
      }, (err) => {
        this.ngxService.stop();
      });
    }
    
  }

  contactFormBuild() {
    const emailRegx = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    this.addContactform = new FormGroup({
      name: new FormControl('', [ Validators.required]),
      ofsl_name:  new FormControl('', [ Validators.required]),
      town: new FormControl('', [ Validators.required]),
      telephone: new FormControl('', [ Validators.required]),
      email: new FormControl('', [ Validators.required, Validators.pattern(emailRegx) ]),
      comment: new FormControl(''),
    });
  }


  addContact(value) {
    //console.log(value);
    this.formSubmit = true;
    var contactObj ={
      name : value.name,
      ofsl_name : value.ofsl_name,
      town : value.town,
      telephone : value.telephone,
      email : value.email,
      comment : value.comment,
    }
    
        if(this.addContactform.valid){

            this.ngxService.start()
            this.UserMasterApi.addContact(contactObj).subscribe(
                (res: any) => {
                    this.ngxService.stop();
                    this.toast.success(this.translate.instant('MESSAGE.CONTACT_SENT_SUCCESS'));
                    this.addContactform.reset();
                    this.router.navigate(['/']);
                },
                (err) => {
                    this.ngxService.stop();
                    this.formSubmit = false;
                    this.toast.error(this.translate.instant('MESSAGE.CONTACT_SEND_ERROR'))
                }
            );
        }
  }

  get name() {
		return this.addContactform.get('name');
  }
  get ofsl_name(){
    return this.addContactform.get('ofsl_name');
  }
  get town() {
		return this.addContactform.get('town');
  }
  get telephone(){
    return this.addContactform.get('telephone');
  }
  get email() {
		return this.addContactform.get('email');
  }
  get comment(){
    return this.addContactform.get('comment');
  }

}
