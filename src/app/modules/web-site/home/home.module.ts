import { NgModule } from '@angular/core';
import { HomeRouteModule } from './home.routes.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { AuthService } from '../../../services/apis/auth.service';
import { HomeComponent } from './home.component';
import { MatSortModule } from '@angular/material';
import { UserMasterService } from 'src/app/services/apis/usermaster';
import { ComponentsModule } from '../../../components/components.module';





@NgModule({
  imports: [
    HomeRouteModule,
    SharedModule,
    MatSortModule,
    ComponentsModule,
   
    
  ],
  declarations: [
    HomeComponent,
  ],
  entryComponents: [
    HomeComponent,
  ],
  providers: [AuthService, UserMasterService ]
})
export class HomeModule { }
