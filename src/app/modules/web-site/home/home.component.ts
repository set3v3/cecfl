import { Component, OnInit } from '@angular/core';
declare var $: any;
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor() { }

  ngOnInit() {

    $(function () {
      $(window).on('scroll', function () {
        var WindowTop = $(window).scrollTop();
        $('.scrollSection').each(function (i) {
          if (WindowTop > $(this).offset().top - 50 &&
            WindowTop < $(this).offset().top + $(this).outerHeight(true)
          ) {
            $('.floatingMenu li a').removeClass('activeScroll');
            $('.floatingMenu li').eq(i).find('a').addClass('activeScroll');
          }
        });
      });
      $('.floatingMenu li a').click(function () {
        // if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
        //   var target = $(this.hash);
        //   target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        //   if (target.length) {
        //     $('html,body').animate({
        //       scrollTop: target.offset().top
        //     }, 1000);
        //     return false;
        //   }
        // }
      });
    });
  }

}
