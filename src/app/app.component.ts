import { Component } from '@angular/core';
import { Router} from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { UserStorageProvider } from './services/storage/user-storage.service';
import { UserMasterService } from './services/apis/usermaster';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'cecfl';
  searchParam: any;
  constructor(
    public router: Router,
    public translateService: TranslateService,
    private userStorage: UserStorageProvider,
    private UserMasterApi: UserMasterService,
    ) {
      translateService.setDefaultLang('es');
      // if (localStorage.getItem('locale') === null) {
      //   localStorage.setItem('locale', 'en');
      // }
      // const locale = localStorage.getItem('locale');
      // // this language will be used as a fallback when a translation isn't found in the current language
      // translate.setDefaultLang(locale);
      // // the lang to use, if the lang isn't available, it will use the current loader to get them
      // translate.use(locale);
    }

    ngOnInit() {
      this.getCityList();
    }

    getCityList() {
      this.searchParam = {
        search: 'PR',
      };
      this.UserMasterApi.citySearch(this.searchParam).subscribe((res: any) => {
        if (res.success) {
          this.userStorage.setCity(res.data);
        }
      }, (err) => {
      });
      
    }
  
  }

  


