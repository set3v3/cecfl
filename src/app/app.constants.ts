import { HttpStatus } from './models/http';
import { environment } from '../environments/environment';
/**
 * All application constant should define here.
 *
 * @export
 * AppConst
 * @author Mohan
 */
export class AppConst {
  public static readonly API_BASE_URL: string = environment.api_base_url;
  public static readonly FILE_BASE_URL: string = environment.file_base_url + '/';
  public static readonly IMG_BASE_URL: string = environment.assets_image_url + '/';
  public static readonly PROJECT_FOLDER: string = environment.project_folder_url + '/';
  public static readonly TEMPLATE_DOWNLOAD_URL: string = 'http://impactocomunitariopr.net/plantillas.html';

  

  /**  Data encryption secret key */
  // public static readonly ENC_KEY: string = '!InT@TbAdmin!#';
  public static readonly ENC_KEY: string = '1234564y56yw45tw45tyw4twqtg';
  /**  API key */
  // tslint:disable-next-line:max-line-length
  public static readonly API_KEY: string = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJ0YkFwcEBpbnRAMTIzIiwibmFtZSI6InBhc3MiLCJpYXQiOjE1MTYyMzkwMjJ9.CwnApT_ogvtZRF9Mwkzox2ieAh61rw08Xdv7LgTKLHw';
  /** Enviromental variable */
  public static readonly ENV_VARIABLE: string = 'development';
  // public static readonly ENV_VARIABLE: string = 'prod';


  /**  server HTTP header status code */
  public static readonly HTTP_STATUS: HttpStatus = {
    OK: 200,
    BAD_REQUEST: 400,
    UN_AUTHORIZED: 401,
    NOT_FOUND: 404,
    SERVER_INTERNAL_ERROR: 500,
    NO_RECORD: 204
  };
  public static readonly MONTH: Array<any> = [
    { name: 'Last 7 Days', id: 1 },
    { name: 'Last 15 Days', id: 2 },
    { name: 'This Month', id: 3 },
    { name: 'Last Year', id: 4 },
    { name: 'Custom', id: 5 }
  ];
  public static readonly MOBILE_USER_TYPE: Array<any> = [
    { name: 'Ppmv', id: 1 },
    { name: 'Labtech', id: 2 },
    { name: 'Clinician', id: 3 },
  ];
  public static readonly DATA_POTAL_USER_TYPE: Array<any> = [
    { name: 'Generalist user', id: 1 },
    { name: 'Program Manager', id: 2 },
    { name: 'Super-admin', id: 3 },
  ];
  public static readonly TB: Array<any> = [
    { name: 'Non-Presumptive', id: 0 },
    { name: 'Presumptive', id: 1 }
  ];
  public static readonly HIV: Array<any> = [
    { name: 'Positive', id: 1 },
    { name: 'Negative', id: 0 },
    { name: 'Unknown', id: 2 }
  ];
  public static readonly ReferralStatus: Array<any> = [
    { name: 'Referred', id: 1 },
    { name: 'Not Referred', id: 0 },
  ];
  public static readonly TreatmentInitiated: Array<any> = [
    { name: 'Yes', id: 1 },
    { name: 'No', id: 0 },
  ];
  public static readonly TbStatus: Array<any> = [
    { name: 'Positive', id: 1 },
    { name: 'Negative', id: 0 },
  ];
  public static readonly TbConfirmStatus: Array<any> = [
    { name: 'Presumptive', id: 1 },
    { name: 'TB Confirmed', id: 2 },
  ];
  public static readonly TbConfirmNotTretStatus: Array<any> = [
    { name: 'Presumptive', id: 0 },
    { name: 'TB Confirmed', id: 1 },
  ];
  public static readonly RStatus: Array<any> = [
    { name: 'Referred', id: 1 },
    { name: 'Not Referred', id: 0 },
  ];
  public static readonly Issuesource: Array<any> = [
    { name: 'App', id: 'App' },
    // { name: 'Data Portal', id: 'Data Portal' },
  ];
  public static readonly IssueType: Array<any> = [
    { name: 'low', id: 'low' },
    { name: 'Medium', id: 'medium' },
    { name: 'Severe', id: 'severe' },
  ];
  public static readonly USER_TYPE: Array<any> = [
    { name: 'Generalist user', id: 'Generalist user' },
    { name: 'Program Manager', id: 'Program Manager' },
    { name: 'Super-admin', id: 'Super-admin' }
  ];
  public static readonly APP_USER_TYPE: Array<any> = [
    { name: 'Clinician', id: 'clinician' },
    { name: 'Independent', id: 'independent' },
    { name: 'Lab', id: 'lab' },
    { name: 'PPMV', id: 'ppmv' }
  ];
  public static readonly LANGUAGE: Array<any> = [
    { name: 'English', id: 'English' },
    // { name: 'Housa', id: 'Housa' }
  ];
  public static readonly MODE: Array<any> = [
    // { name: 'Offline', id: 'offline' },
    { name: 'Online', id: 'online' }
  ];
  public static readonly DefaultUserType: Array<any> = [
    { name: 'Independent', id: 0 },
    { name: 'Network', id: 1 },
  ];
  public static readonly MONTH_LIST: Array<any> = [
    { name: 'January', id: '01' },
    { name: 'February', id: '02' },
    { name: 'March', id: '03' },
    { name: 'April', id: '04' },
    { name: 'May', id: '05' },
    { name: 'June', id: '06' },
    { name: 'July', id: '07' },
    { name: 'August', id: '08' },
    { name: 'September', id: '09' },
    { name: 'October', id: '10' },
    { name: 'November', id: '11' },
    { name: 'December', id: '12' },
  ];
  public static readonly YEAR_LIST: Array<any> = [
    '2018'
  ];
  public static readonly DEFAULT_ROLE: Array<any> = [
    /*{
      name: 'role',
      status: '1',
      url: "user-role",
      permission: [
        { permissionName: 'create', status: true },
        { permissionName: 'view', status: true },
        { permissionName: 'edit', status: true },
        { permissionName: 'delete', status: true }
      ]
    },*/
    {
      name: 'entity-list',
      status: '1',
      url: "entity-list",
      permission: []
    },
    {
      name: 'entity-view',
      status: '1',
      url: "entity-view",
      permission: []
    },
    {
      name: 'entity-add',
      status: '1',
      url: "entity-add",
      permission: []
    },
    {
      name: 'admin-user',
      status: '1',
      url: "admin-user",
      permission: [
        { permissionName: 'create', status: true },
        { permissionName: 'view', status: true },
        { permissionName: 'edit', status: true }
      ]
    },
    {
      name: 'quaterly-reports',
      status: '1',
      url: "quaterly-reports",
      permission: []
    },
    {
      name: 'proposal-edit',
      status: '1',
      url: "proposal-edit",
      permission: [
        { permissionName: 'grant', status: true }
      ]
    },
    {
      name: 'proposal-list',
      status: '1',
      url: "proposal-list",
      permission: []
    },
    {
      name: 'proposal-view',
      status: '1',
      url: "proposal-view",
      permission: []
    },
    {
      name: 'news-add',
      status: '1',
      url: "news-add",
      permission: []
    },
    {
      name: 'news-edit',
      status: '1',
      url: "news-edit",
      permission: []
    },
    {
      name: 'news-list',
      status: '1',
      url: "news-list",
      permission: [{ permissionName: 'delete', status: true }]
    },
    {
      name: 'project-add',
      status: '1',
      url: "project-add",
      permission: []
    },
    {
      name: 'project-edit',
      status: '1',
      url: "project-edit",
      permission: []
    },
    {
      name: 'project-list',
      status: '1',
      url: "project-list",
      permission: [{ permissionName: 'delete', status: true }]
    },
    {
      name: 'cms-list',
      status: '1',
      url: "cms-list",
      permission: [{ permissionName: 'delete', status: true }]
    },
    {
      name: 'cms-add',
      status: '1',
      url: "cms-add",
      permission: []
    },
    {
      name: 'cms-edit',
      status: '1',
      url: "cms-edit",
      permission: []
    },
    {
      url: "audittrail",
      name: "audittrail",
      status: "1",
      permission: []
    }
  ];
  public static readonly PPMVMENU: Array<any> = [
    { name: 'Home', id: 'Home ' },
    { name: 'Screen New Client', id: 'Screen New Client' },
    { name: 'My Clients', id: 'My Clients' },
    { name: 'Dashboard', id: 'Dashboard' },
    { name: 'Edit Profile', id: 'Edit Profile' },
    { name: 'Change Password', id: 'Change Password' },
    { name: 'Help & Support', id: 'Help & Support' },
    { name: 'Logout', id: 'Logout' }
  ];
  public static readonly IP_MENU: Array<any> = [
    { name: 'Home', id: 'Home ' },
    { name: 'Dashboard', id: 'Dashboard' },
    { name: 'Edit Profile', id: 'Edit Profile' },
    { name: 'Change Password', id: 'Change Password' },
    { name: 'Help & Support', id: 'Help & Support' },
    { name: 'Logout', id: 'Logout' }
  ];
  

}
