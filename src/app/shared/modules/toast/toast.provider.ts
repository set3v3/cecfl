import { Injectable } from '@angular/core';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material';
import { ToastComponent } from './toast.component';
import { ToastExtraData } from './toast.model';

@Injectable()
export class ToastProvider {

  private config: MatSnackBarConfig;
  private extraData: ToastExtraData;

  constructor(public toast: MatSnackBar) {
    // intialization of properties
    this.config = new MatSnackBarConfig();
    this.extraData = {
      type: 'success',
      message: ''
    };
    // set default value to toast configuration
    this.config.duration = 3000;
    this.config.verticalPosition = 'top';
    this.config.horizontalPosition = 'right';
  }

  success(message: string, duratiopn?: number) {
    this.extraData.message = message;
    this.extraData.type = 'success';
    this.config.data = this.extraData;
    duratiopn ? this.config.duration = duratiopn : this.config.duration = 3000;
    this.toast.openFromComponent(ToastComponent, this.config);
  }

  error(message: string, duratiopn?: number) {
    this.extraData.message = message;
    this.extraData.type = 'error';
    this.config.data = this.extraData;
    duratiopn ? this.config.duration = duratiopn : this.config.duration = 3000;
    this.toast.openFromComponent(ToastComponent, this.config);
  }

  warning(message: string, duratiopn?: number) {
    this.extraData.message = message;
    this.extraData.type = 'warning';
    this.config.data = this.extraData;
    duratiopn ? this.config.duration = duratiopn : this.config.duration = 3000;
    this.toast.openFromComponent(ToastComponent, this.config);
  }
}
