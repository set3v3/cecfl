
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { AppMaterialModule } from '../app.material.module';
import { ToastModule } from './modules/toast/toast.module';
import { RandomBackgroundDirective } from '../directives/utilities/randombackground-color/randombackground';
import { FileValidator } from '../directives/validators/file-validation';
import { WhiteSpaceValidator } from '../directives/validators/white-space-validation';
import { FileValueAccessorDirective } from '../directives/validators/read-input-file';
import { ConsoleProvider } from './modules/console/console.provider';
import { NgxMaskModule } from 'ngx-mask';
//import { AmazingTimePickerModule } from 'amazing-time-picker';
import { CryptoProvider } from '../services/crypto/crypto.service';
import { UserStorageProvider } from '../services/storage/user-storage.service';
import { NgxUiLoaderModule } from 'ngx-ui-loader';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';




@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    TranslateModule,
    AppMaterialModule,
    NgxUiLoaderModule,
    ToastModule,
    NgxMaskModule,
    //AmazingTimePickerModule,
    FormsModule,
      NgbModule,
    
  ],
  declarations: [
    RandomBackgroundDirective,
    FileValueAccessorDirective,
    FileValidator,
    WhiteSpaceValidator
  ],
  exports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    AppMaterialModule,
    TranslateModule,
    ToastModule,
    RandomBackgroundDirective,
    FileValueAccessorDirective,
    FileValidator,
    WhiteSpaceValidator,
    NgxMaskModule,
    NgxUiLoaderModule,
    NgbModule,
    

    //AmazingTimePickerModule
],
  providers: [
    CryptoProvider,
    ConsoleProvider,
    UserStorageProvider
  ]
})
export class SharedModule { }
