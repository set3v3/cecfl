import { Injectable } from '@angular/core';
import { AppConst } from '../../app.constants';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { LoginApiResponce,RegisterApiResponce, ResetPasswordApiResponce, LogOutApiResponce, ForGotPasswordApiResponce } from 'src/app/models/auth';


@Injectable()
export class AuthService {

  private LOGIN_URL: string;
  private REGISTER_URL: string;
  private CHNAGE_PASSWORD: string;
  private LOGOUT_URL: string;
  private FORGOT_PASS_URL: string;
  private VERIFY_URL: string;
  private CHNAGE_PASSWORD_FORGOT:string;
  private ACCOUNT_VERIFY_URL: string
  constructor(
    private httpClient: HttpClient
  ) {
    this.LOGIN_URL = '/api/login';
    this.LOGOUT_URL = '/api/logout';
    this.REGISTER_URL = '/api/register';
    this.CHNAGE_PASSWORD = '/api/changePassword';
    this.FORGOT_PASS_URL = '/api/forgotpassword';
    this.VERIFY_URL = '/api/forgotpasswordverify/';
    this.CHNAGE_PASSWORD_FORGOT = '/api/savepassword';
    this.ACCOUNT_VERIFY_URL = '/api/verify/';

  }
  dologin(credentials): Observable<LoginApiResponce> {
    const request = {
      url: AppConst.API_BASE_URL + this.LOGIN_URL
    };
    return this.httpClient.post<LoginApiResponce>(request.url, credentials);
  }
  doRegister(credentials): Observable<RegisterApiResponce> {
    const request = {
      url: AppConst.API_BASE_URL + this.REGISTER_URL
    };
    return this.httpClient.post<RegisterApiResponce>(request.url, credentials);
  }
  resetPassword(credentials): Observable<ResetPasswordApiResponce> {
    const request = {
      url: AppConst.API_BASE_URL + this.CHNAGE_PASSWORD  
    };
    return this.httpClient.post<ResetPasswordApiResponce>(request.url, credentials);
  }
  resetPasswordForForgot(credentials,id): Observable<ResetPasswordApiResponce>{
    const request = {
      url: AppConst.API_BASE_URL + this.CHNAGE_PASSWORD_FORGOT +'/'+ id 
    };
    return this.httpClient.post<ResetPasswordApiResponce>(request.url, credentials);
  }
  logout(credentials): Observable<LogOutApiResponce> {
    const request = {
      url: AppConst.API_BASE_URL + this.LOGOUT_URL
    };
    return this.httpClient.post<LogOutApiResponce>(request.url,credentials);
  }
  forgotPassword(param): Observable<ForGotPasswordApiResponce> {
    const request = {
      url: AppConst.API_BASE_URL + this.FORGOT_PASS_URL
    };
    return this.httpClient.post<ForGotPasswordApiResponce>(request.url, param);
  }
  doverify(params): Observable<LoginApiResponce> {
     //console.log('params=>',params);
    const request = {
      url: AppConst.API_BASE_URL + this.VERIFY_URL + params.id +'/'+ params.token 
    };
    return this.httpClient.get<LoginApiResponce>(request.url);
  }
  accountdoverify(params): Observable<LoginApiResponce> {
    //console.log('params=>',params);
   const request = {
     url: AppConst.API_BASE_URL + this.ACCOUNT_VERIFY_URL + params.id +'/'+ params.token 
   };
   return this.httpClient.get<LoginApiResponce>(request.url);
 }

}
