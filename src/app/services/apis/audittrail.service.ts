import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { AppConst } from '../../app.constants';


@Injectable()
export class AuditTrailService  {
  private LOG_LIST :string;
  private USER_LIST:string;
  constructor(
    private httpClient: HttpClient
  ) {
    this.LOG_LIST = '/api/getLogList';
    this.USER_LIST= '/api/getAllUsers'
   
}
  LOGLIST(param): Observable<any> {
    const request = {
      url: AppConst.API_BASE_URL + this.LOG_LIST
    };
    return this.httpClient.get<any>(request.url,{params:param});
  }
  
  getUser(): Observable<any>{
    const request = {
      url: AppConst.API_BASE_URL + this.USER_LIST
    };
    return this.httpClient.get<any>(request.url);
  }

}
