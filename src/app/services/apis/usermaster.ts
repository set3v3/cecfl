import { Injectable } from '@angular/core';
import { AppConst } from '../../app.constants';
import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpParams } from '@angular/common/http';
import {
  UserlistApiResponse, UsereditPermissionApiResponse,
  ADMINUserlistApiResponse, AddAdminUserApiResponse,
  EditAdminUserApiResponse, ImeiApiResponse, AdminUserReportExportApiResponse, AppUserReportExportApiResponse, AddressDetailsApiResponse,StateApiResponse,ProjectApiResponse,MemberApiResponse,MapSearchApiResponce
} from 'src/app/models/usermaster';


@Injectable({
  providedIn : 'root'
})
export class UserMasterService {
 
  private USER_LIST: string;
  private USER_STATUS_UPDATE: string;
  private ADD_ADMIN_USER: string;
  private ADMIN_USER_LIST: string;
  private ASSIGN_USER_LIST: string;
  private EDIT_ADMIN_USER: string;
  private SHOW_IMEI_NO: string;
  private ADMIN_USER_REPORT_EXEL_EXPORT: string;
  private APP_USER_REPORT_EXEL_EXPORT: string;
  private STATE_LGA_RELETIONSHIP: string;
  private STATE_LIST: string;
  private CITY_LIST: string;
  private CITY_SEARCH: string;
  private PROJECT_LIST: string;
  private EDIT_MEMBER: string;
  private ADD_MEMBER: string;
  private SINGLE_MEMBER: string;
  private MEMBER_LIST: string;
  private DELETE_MEMBER: string;
  private citySearchList : string;
  private ADD_CONTACT : string;
  private SEND_CONTACT : string;
  private GETSEARCHCOUNT : string;
  private PAREO_TYPE_LIST : string;
  private PAREO_CAT_LIST : string;
  constructor(
    private httpClient: HttpClient
  ) {
    this.USER_LIST = '/api/userList';
    this.USER_STATUS_UPDATE = '/api/editStatus';

    this.ADD_ADMIN_USER = '/api/adminUserAdd';
    this.ADMIN_USER_LIST = '/api/adminUserList';
    this.ASSIGN_USER_LIST = '/api/getAssignedUsers';
    this.EDIT_ADMIN_USER = '/api/adminUserUpdate/';

    this.SHOW_IMEI_NO = '/admin/imeiVerification';
    this.ADMIN_USER_REPORT_EXEL_EXPORT = '/admin/adminUserListExport';
    this.APP_USER_REPORT_EXEL_EXPORT = '/admin/userListExport';
    this.STATE_LGA_RELETIONSHIP = '/admin/deatilsForindependentUser';
    this.STATE_LIST = '/api/states';
    this.CITY_LIST = '/api/cities';
    this.CITY_SEARCH = '/api/getCities';

    this.PROJECT_LIST = '/api/getProjetList';

    this.ADD_MEMBER= '/api/addEntityMember'
    this.EDIT_MEMBER = '/api/updateEntityMember';
    this.SINGLE_MEMBER = '/api/singleMember';
    this.MEMBER_LIST = '/api/getMembers';
    this.DELETE_MEMBER = '/api/deleteMember';

    this.citySearchList = '/api/getUsersWithApprovedProposal';

    this.ADD_CONTACT= '/api/addContact';
    this.SEND_CONTACT= '/api/sendContact';
    this.GETSEARCHCOUNT= '/api/getSearchCount';

    this.PAREO_TYPE_LIST = '/api/pareoType';
    this.PAREO_CAT_LIST = '/api/pareoCategory';
  }
  getUserList(info): Observable<UserlistApiResponse> {
    const request = {
      url: AppConst.API_BASE_URL + this.USER_LIST
    };
    const params = new HttpParams()
      .set('search', info.search);
    return this.httpClient.get<UserlistApiResponse>(request.url, {params});
  }
  UserUpdate(userinfo): Observable<UsereditPermissionApiResponse> {
    const request = {
      url: AppConst.API_BASE_URL + this.USER_STATUS_UPDATE
    };
    return this.httpClient.post<UsereditPermissionApiResponse>(request.url, userinfo);

  }
  AddAdminUser(param): Observable<AddAdminUserApiResponse> {
    const request = {
      url: AppConst.API_BASE_URL + this.ADD_ADMIN_USER
    };
    return this.httpClient.post<AddAdminUserApiResponse>(request.url, param);
  }
  getAdminUserList(info): Observable<ADMINUserlistApiResponse> {
    //console.log(info)
    const request = {
      url: AppConst.API_BASE_URL + this.ADMIN_USER_LIST
    };
    const params = new HttpParams()
      .set('search', info.search);
    return this.httpClient.get<ADMINUserlistApiResponse>(request.url, { params });
  }
  getAssignedUser(): Observable<any>{
    const request = {
      url: AppConst.API_BASE_URL + this.ASSIGN_USER_LIST
    };
    return this.httpClient.get<any>(request.url);
  }
  AdminUserUpdate(userinfo): Observable<EditAdminUserApiResponse> {
    const request = {
      url: AppConst.API_BASE_URL + this.EDIT_ADMIN_USER + userinfo.id
    };
    return this.httpClient.put<EditAdminUserApiResponse>(request.url, userinfo);
  }
  ShowImeiNo(info): Observable<ImeiApiResponse> {
    const request = {
      url: AppConst.API_BASE_URL + this.SHOW_IMEI_NO
    };
    return this.httpClient.post<ImeiApiResponse>(request.url, info);
  }
  downloadAdminUserExport(info): Observable<AdminUserReportExportApiResponse> {
    const request = {
      url: AppConst.API_BASE_URL + this.ADMIN_USER_REPORT_EXEL_EXPORT
    };
    const params = new HttpParams()
      .set('search', info.search);
    return this.httpClient.get<AdminUserReportExportApiResponse>(request.url, { params });
  }
  downloadAppUserReportExport(info): Observable<AppUserReportExportApiResponse> {
    const request = {
      url: AppConst.API_BASE_URL + this.APP_USER_REPORT_EXEL_EXPORT
    };
    const params = new HttpParams()
      .set('search', info.search);
    return this.httpClient.get<AppUserReportExportApiResponse>(request.url, { params });
  }
  getlgaState(): Observable<AddressDetailsApiResponse> {
    const request = {
      url: AppConst.API_BASE_URL + this.STATE_LGA_RELETIONSHIP
    };
    return this.httpClient.get<AddressDetailsApiResponse>(request.url);
  }
  getState(): Observable<StateApiResponse> {
    const request = {
      url: AppConst.API_BASE_URL + this.STATE_LIST
    };
    return this.httpClient.get<StateApiResponse>(request.url);
  }
  cityList(): Observable<StateApiResponse> {
    const request = {
      url: AppConst.API_BASE_URL + this.CITY_LIST
    };
    return this.httpClient.get<StateApiResponse>(request.url);
  }
  citySearch(info): Observable<StateApiResponse> {
    const request = {
      url: AppConst.API_BASE_URL + this.CITY_SEARCH
    };
    const params = new HttpParams()
      .set('search', info.search);
    return this.httpClient.get<StateApiResponse>(request.url, { params });
  }
  projectList(): Observable<ProjectApiResponse> {
    const request = {
      url: AppConst.API_BASE_URL + this.PROJECT_LIST
    };
    return this.httpClient.get<ProjectApiResponse>(request.url);
  }
  editMember(param, id): Observable<any> {
    const request = {
      url: AppConst.API_BASE_URL + this.EDIT_MEMBER + '/' + id
    };

    return this.httpClient.post<any>(request.url, param);
  }
  addMember(param): Observable<any> {
    const request = {
        url: AppConst.API_BASE_URL + this.ADD_MEMBER
    };
    return this.httpClient.post<any>(request.url, param);
  }
  singleViewMember(id): Observable<any> {
    const request = {
      url: AppConst.API_BASE_URL + this.SINGLE_MEMBER + '/' + id
    };
    return this.httpClient.get<any>(request.url);
  }
  memberList(id): Observable<MemberApiResponse> {
    const request = {
      url: AppConst.API_BASE_URL + this.MEMBER_LIST + '/' + id
    };
    return this.httpClient.get<any>(request.url);
  }
  deleteMember(id): Observable<any> {
    const request = {
      url: AppConst.API_BASE_URL + this.DELETE_MEMBER + '/' + id
    };
    return this.httpClient.delete<any>(request.url );
  }
  getSearchList(cityId,catId): Observable<any> {
    const request = {
      url: AppConst.API_BASE_URL + this.citySearchList+'?city='+cityId+'&category='+catId
    };
    return this.httpClient.get<any>(request.url);
  }
  addContact(param): Observable<any> {
    const request = {
        url: AppConst.API_BASE_URL + this.ADD_CONTACT
    };
    return this.httpClient.post<any>(request.url, param);
  }
  sendContact(param): Observable<any> {
    const request = {
        url: AppConst.API_BASE_URL + this.SEND_CONTACT
    };
    return this.httpClient.post<any>(request.url, param);
  }
  getCategoryReport(param): Observable<MapSearchApiResponce> {
    const request = {
      url: AppConst.API_BASE_URL + this.GETSEARCHCOUNT
    };
    return this.httpClient.get<MapSearchApiResponce>(request.url, {params:param});
  }

  pareotypeList(): Observable<any> {
    const request = {
      url: AppConst.API_BASE_URL + this.PAREO_TYPE_LIST
    };
    return this.httpClient.get<any>(request.url);
  }

  pareocategoryList(id): Observable<any> {
    const request = {
      url: AppConst.API_BASE_URL + this.PAREO_CAT_LIST + '/' + id
    };
    return this.httpClient.get<any>(request.url);
  }

}
