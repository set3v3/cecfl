import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { AppConst } from '../../app.constants';


@Injectable()
export class NewsService  {
  private ADD_NEWS: string;
  private NEWS: string;
  private EDIT_NEWS: string;
  private DELETE: string;
  private VIEW: string;
  private NEWS_FILE_DEL :string;
  private ACTIVENEWS :string;
  constructor(
    private httpClient: HttpClient
  ) {
    this.ADD_NEWS = '/api/addNews';
    this.NEWS = '/api/news';
    this.EDIT_NEWS = '/api/editNews';
    this.DELETE = '/api/news';
    this.VIEW = '/api/news';
    this.NEWS_FILE_DEL = '/api/newsFileDelete';
    this.ACTIVENEWS = '/api/activeNews';
    
   
  }

  addnews(newsinfo): Observable<any> {
    const request = {
      url: AppConst.API_BASE_URL + this.ADD_NEWS
    };
    return this.httpClient.post<any>(request.url, newsinfo);
  }

  news(param): Observable<any> {
    const request = {
      url: AppConst.API_BASE_URL + this.NEWS
    };
    return this.httpClient.get<any>(request.url,{params:param});
  }

  editnews(newsinfo,id): Observable<any> {
    const request = {
      url: AppConst.API_BASE_URL + this.EDIT_NEWS + '/' + id
    };
   
    return this.httpClient.post<any>(request.url, newsinfo);
  }

  view(id): Observable<any> {
    const request = {
      url: AppConst.API_BASE_URL + this.VIEW + '/' + id
    };
    return this.httpClient.get<any>(request.url);
  }

  

  delete(id): Observable<any> {
    const request = {
      url: AppConst.API_BASE_URL + this.DELETE + '/' + id
    };
    return this.httpClient.delete<any>(request.url );
  }

  delFile(params): Observable<any> {
    const request = {
      url: AppConst.API_BASE_URL + this.NEWS_FILE_DEL +'/'+ params.id
    };
    return this.httpClient.delete<any>(request.url,{});  
  }

  activenews(param): Observable<any> {
    const request = {
      url: AppConst.API_BASE_URL + this.ACTIVENEWS
    };
    return this.httpClient.get<any>(request.url,{params:param});
  }
  

}
