import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { AppConst } from '../../app.constants';


@Injectable()
export class QualityService  {
 
  private QUALITY_REPORTS:string;
  private EXPENSE_SUMMARY:string;
  private EXPENDITURE_LIST:string;
  private EXPENDITURE_SAVE:string;
  private EXPENSE_LIST:string
  private DELETE_DIRECT_ADMINISTRATOR:string;
  private CLASSIFICATION:string;
  private EVIDENCE_SAVE:string;
  private EVIDENCE_LIST:string;
  private DELETE_EVIDENCE:string;
 
  constructor(
    private httpClient: HttpClient
  ) {
  
    this.QUALITY_REPORTS = '/api/getUserProposalDetails';
    this.EXPENSE_SUMMARY = '/api/addQuarterlyReport';
    this.EXPENDITURE_LIST = '/api/getExpenditure';
    this.EXPENDITURE_SAVE = '/api/addExpenses';
    this.EXPENSE_LIST = '/api/getExpenseList';
    this.DELETE_DIRECT_ADMINISTRATOR = '/api/deleteExpense';
    this.CLASSIFICATION = '/api/getEvidences';
    this.EVIDENCE_SAVE = '/api/moveEvidenseFile';
    this.EVIDENCE_LIST ='/api/getEvidencefileList';
    this.DELETE_EVIDENCE = '/api/evidencefileDelete'
   
  }
  qualityreports(id): Observable<any> {
    const request = {
      url: AppConst.API_BASE_URL + this.QUALITY_REPORTS + '/' + localStorage.getItem('proposal_id')
    };
    return this.httpClient.get<any>(request.url);
  }
  expensesummary(newsinfo): Observable<any> {
    const request = {
      url: AppConst.API_BASE_URL + this.EXPENSE_SUMMARY
    };
    return this.httpClient.post<any>(request.url,newsinfo);
  }
  getExpenditureList(id){
    const request = {
      url: AppConst.API_BASE_URL + this.EXPENDITURE_LIST +'/'+id +'/Administrative'
    };
    return this.httpClient.get<any>(request.url);
  }
  getDirectExpenditureList(id){
    const request = {
      url: AppConst.API_BASE_URL + this.EXPENDITURE_LIST +'/'+id +'/Direct'
    };
    return this.httpClient.get<any>(request.url);
  }
  saveAdministrativeExpenditure(param){
    const request = {
      url: AppConst.API_BASE_URL + this.EXPENDITURE_SAVE
    };
    return this.httpClient.post<any>(request.url,param);
  }
  getAdmExpenceList(id){
    const request = {
      url: AppConst.API_BASE_URL + this.EXPENSE_LIST +'/'+id +'/Administrative'
    };
    return this.httpClient.get<any>(request.url);
  }
  getDirectExpenceList(id){
    const request = {
      url: AppConst.API_BASE_URL + this.EXPENSE_LIST +'/'+id +'/Direct'
    };
    return this.httpClient.get<any>(request.url);
  }
  deleteAdministratorDirectExpance(id){
    const request = {
      url: AppConst.API_BASE_URL + this.DELETE_DIRECT_ADMINISTRATOR +'/'+id
    };
    return this.httpClient.delete<any>(request.url);
  }
  getClassification(){
    const request = {
      url: AppConst.API_BASE_URL + this.CLASSIFICATION
    };
    return this.httpClient.get<any>(request.url);
  }
  saveEvidence(param){
    const request = {
      url: AppConst.API_BASE_URL + this.EVIDENCE_SAVE
    };
    return this.httpClient.post<any>(request.url,param);
  }
  getEvidenceList(id){
    const request = {
      url: AppConst.API_BASE_URL + this.EVIDENCE_LIST +'/'+id 
    };
    return this.httpClient.get<any>(request.url);
  }
  deleteEvidence(id){
    const request = {
      url: AppConst.API_BASE_URL + this.DELETE_EVIDENCE +'/'+id
    };
    return this.httpClient.delete<any>(request.url);
  }
}
