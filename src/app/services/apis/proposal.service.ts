import { Injectable } from '@angular/core';
import { AppConst } from '../../app.constants';
import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpParams } from '@angular/common/http';
import { ProposalListingApiResponce, EDITProposalApiResponce, ADDProposalApiResponce } from 'src/app/models/proposal';


@Injectable()
export class ProposalService {
  private ADD_PROPOSAL: string;
  private EDIT_PROPOSAL: string;
  private VIEW_PROPOSAL: string;
  private PROPOSAL_LISTING: string;
  private PROPOSAL_FILE_DEL: string;
  private PROPOSAL_ACCEPT: string;
  private PROPOSAL_REJECT: string;
  private UPDATE_GRANT: string;
  private ADD_ATTACH :string;
  private ATTACH_LISTING :string;
  PROPOSAL_EXPANCE_DEL:string;
  PROPOSAL_PDF:string;
  constructor(
    private httpClient: HttpClient
  ) {
    this.ADD_PROPOSAL = '/api/addProposal';
    this.EDIT_PROPOSAL = '/api/editProposal/';
    this.PROPOSAL_LISTING = '/api/listProposal';
    this.VIEW_PROPOSAL = '/api/proposal/';
    this.PROPOSAL_FILE_DEL = '/api/proposalDoc/';
    this.PROPOSAL_FILE_DEL = '/api/proposalDoc/';
    this.PROPOSAL_ACCEPT = '/api/acceptProposal/';
    this.PROPOSAL_REJECT = '/api/rejectProposal/';
    this.UPDATE_GRANT = '/api/updateGrant/';
    this.ADD_ATTACH = '/api/attachFile';
    this.ATTACH_LISTING = '/api/getAttachFileList';
    this.PROPOSAL_EXPANCE_DEL ='/api/deleteBudgetBreak/';
    this.PROPOSAL_PDF = '/api/getPdf'
  }
  addProposal(param): Observable<ADDProposalApiResponce> {
    if(localStorage.getItem('proposal_id')){
      const request = {
        url: AppConst.API_BASE_URL + this.EDIT_PROPOSAL + localStorage.getItem('proposal_id')
      };
      return this.httpClient.post<EDITProposalApiResponce>(request.url, param);
    }else{
      const request = {
        url: AppConst.API_BASE_URL + this.ADD_PROPOSAL
      };
      return this.httpClient.post<ADDProposalApiResponce>(request.url, param);
    }
  }
  listProposal(param): Observable<ProposalListingApiResponce> {
    const request = {
      url: AppConst.API_BASE_URL + this.PROPOSAL_LISTING
    };
    return this.httpClient.get<ProposalListingApiResponce>(request.url, {params:param});
  }
  viewProposal(): Observable<EDITProposalApiResponce> {
    const request = {
      url: AppConst.API_BASE_URL + this.VIEW_PROPOSAL + localStorage.getItem('proposal_id')
    };
    return this.httpClient.get<EDITProposalApiResponce>(request.url, {});  
  }
  delFile(params): Observable<EDITProposalApiResponce> {
    const request = {
      url: AppConst.API_BASE_URL + this.PROPOSAL_FILE_DEL + params.id
    };
    return this.httpClient.delete<EDITProposalApiResponce>(request.url,{});  
  }
  editProposal(params): Observable<EDITProposalApiResponce> {
    const request = {
      url: AppConst.API_BASE_URL + this.EDIT_PROPOSAL + localStorage.getItem('proposal_id')
    };
    return this.httpClient.post<EDITProposalApiResponce>(request.url, params);
  }
  acceptProposal(params): Observable<EDITProposalApiResponce> {
    const request = {
      url: AppConst.API_BASE_URL + this.PROPOSAL_ACCEPT + localStorage.getItem('proposal_id')
    };
    return this.httpClient.post<EDITProposalApiResponce>(request.url, params);
  }
  rejectProposal(params): Observable<EDITProposalApiResponce> {
    const request = {
      url: AppConst.API_BASE_URL + this.PROPOSAL_REJECT + localStorage.getItem('proposal_id')
    };
    return this.httpClient.post<EDITProposalApiResponce>(request.url, params);
  }
  updateGrant(params): Observable<EDITProposalApiResponce> {
    const request = {
      url: AppConst.API_BASE_URL + this.UPDATE_GRANT + localStorage.getItem('proposal_id')
    };
    return this.httpClient.post<EDITProposalApiResponce>(request.url, params);
  }

  addAttach(param): Observable<any>{
    const request = {
      url: AppConst.API_BASE_URL + this.ADD_ATTACH
    };
    return this.httpClient.post<any>(request.url, param);
  }

  getAttachDoc(id): Observable<any>{
    const request = {
      url: AppConst.API_BASE_URL + this.ATTACH_LISTING + '/' + id
    };
    return this.httpClient.get<any>(request.url);
  }
  exportProposal(param):Observable<any>{
    const request = {
      url: AppConst.API_BASE_URL + this.PROPOSAL_LISTING
    };
    return this.httpClient.get<ProposalListingApiResponce>(request.url, {params:param});
  }
  deleteExpance(id): Observable<EDITProposalApiResponce> {
    const request = {
      url: AppConst.API_BASE_URL + this.PROPOSAL_EXPANCE_DEL + id
    };
    return this.httpClient.delete<EDITProposalApiResponce>(request.url,{});  
  }

  // proposalPdf(){
  //   const request = {
  //     url: AppConst.API_BASE_URL + this.PROPOSAL_PDF
  //   };
  //   return this.httpClient.get<ProposalListingApiResponce>(request.url);
  // }
  
}
