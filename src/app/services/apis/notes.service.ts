import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { AppConst } from '../../app.constants';

@Injectable()
export class NotesService  {
    private ADD_NOTES: string;
    private LIST_NOTES: string;
    private NOTE_DELETE: string;
    constructor(
        private httpClient: HttpClient
    ) {
        this.ADD_NOTES = '/api/addNotes';
        this.LIST_NOTES = '/api/noteList';
        this.NOTE_DELETE = '/api/deleteNote';
    }

    addnotes(notesinfo): Observable<any> {
        const request = {
            url: AppConst.API_BASE_URL + this.ADD_NOTES
        };
        return this.httpClient.post<any>(request.url, notesinfo);
    }

    listnotes(proposal_id): Observable<any> {
        const request = {
          url: AppConst.API_BASE_URL + this.LIST_NOTES + '/' + proposal_id
        };
        return this.httpClient.get<any>(request.url);
    }

    notedelete(id): Observable<any> {
    const request = {
        url: AppConst.API_BASE_URL + this.NOTE_DELETE + '/' + id
    };
    return this.httpClient.delete<any>(request.url );
    }

}