import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { AppConst } from '../../app.constants';


@Injectable()


export class ProjectService {

    private ADD_PROJECT: string;
    private PROJECT: string;
    private EDIT_PROJECT: string;
    private DELETE: string;
    private VIEW: string;
    private ACTIVE_PROJECT: string;

    constructor(
        private httpClient: HttpClient
    ) {
        this.ADD_PROJECT = '/api/addProject';
        this.PROJECT = '/api/project';
        this.EDIT_PROJECT = '/api/editProject';
        this.DELETE = '/api/project';
        this.VIEW = '/api/project';
        this.ACTIVE_PROJECT = '/api/getProjetList';
    }

    addProject(param): Observable<any> {
        const request = {
            url: AppConst.API_BASE_URL + this.ADD_PROJECT
        };
        return this.httpClient.post<any>(request.url, param);
    }

    projectList(param): Observable<any> {
        const request = {
            url: AppConst.API_BASE_URL + this.PROJECT
        };
        return this.httpClient.get<any>(request.url, { params: param });
    }

    editProject(param, id): Observable<any> {
        const request = {
            url: AppConst.API_BASE_URL + this.EDIT_PROJECT + '/' + id
        };

        return this.httpClient.put<any>(request.url, param);
    }

    view(id): Observable<any> {
        const request = {
            url: AppConst.API_BASE_URL + this.VIEW + '/' + id
        };
        return this.httpClient.get<any>(request.url);
    }
    
    activeProject(): Observable<any>{
        const request = {
            url: AppConst.API_BASE_URL + this.ACTIVE_PROJECT
        }
        return this.httpClient.get<any>(request.url);
    }


}