import { Injectable } from '@angular/core';
import { AppConst } from '../../app.constants';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import {
  DashbordApiResponce, LgaWiseReportApiResponce,
  StateWiseReportApiResponce, AdminDashboardGraphApiResponce
} from 'src/app/models/dashbord';


@Injectable()
export class DashBordService {
  private REPORTTAB: string;
  private REPORTGRAPH: string;
  private STATEWISEREPORT: string;
  private LGAWISEREPORT_TABLE: string;
  private COMMISSION_LIST:string
  constructor(
    private httpClient: HttpClient
  ) {
    this.REPORTTAB = '/api/proposalCount';
    this.REPORTGRAPH = '/api/datewiseProposalCount';
    this.STATEWISEREPORT = '/admin/stateWiseReport';
    this.LGAWISEREPORT_TABLE = '/admin/lgaWiseReport';
    this.COMMISSION_LIST = '/api/pagecontent'
  }
  getreportTab(param): Observable<DashbordApiResponce> {
    const request = {
      url: AppConst.API_BASE_URL + this.REPORTTAB
    };
    return this.httpClient.get<DashbordApiResponce>(request.url, {params:param});
  }

  getGraphData(param): Observable<AdminDashboardGraphApiResponce> {
    // console.log(param);
    const request = {
      url: AppConst.API_BASE_URL + this.REPORTGRAPH
    };
    return this.httpClient.get<AdminDashboardGraphApiResponce>(request.url, {params:param});
  }
  getstateWiseReportData(param): Observable<StateWiseReportApiResponce> {
    const request = {
      url: AppConst.API_BASE_URL + this.STATEWISEREPORT
    };
    return this.httpClient.post<StateWiseReportApiResponce>(request.url, param);
  }
  getLGAWiseInformationTable(param): Observable<LgaWiseReportApiResponce> {
    const request = {
      url: AppConst.API_BASE_URL + this.LGAWISEREPORT_TABLE
    };
    return this.httpClient.post<LgaWiseReportApiResponce>(request.url, param);
  }
  getCmsContent(param){
    const request = {
      url: AppConst.API_BASE_URL + this.COMMISSION_LIST
    };
    return this.httpClient.get<LgaWiseReportApiResponce>(request.url, {params:param});
  }
  
}
