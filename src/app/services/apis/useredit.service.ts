import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { AppConst } from '../../app.constants';


@Injectable()
export class UserEditService  {
  private VIEW: string;
  private UPDATEUSER: string;
  
  constructor(
    private httpClient: HttpClient
  ) {
    this.VIEW = '/api/users';
    this.UPDATEUSER ='/api/updateUser';
    
   
  }
  view(id): Observable<any> {
    const request = {
      url: AppConst.API_BASE_URL + this.VIEW + '/' + id
    };
    return this.httpClient.get<any>(request.url);
  }

  edituser(newsinfo): Observable<any> {
    const request = {
      url: AppConst.API_BASE_URL + this.UPDATEUSER 
    };
   
    return this.httpClient.post<any>(request.url, newsinfo);
  }

}
