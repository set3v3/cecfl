import { Injectable } from '@angular/core';
import { UserStorageProvider } from '../storage/user-storage.service';

//   */
@Injectable()

export class PermissionProvider {
  permissionlist: any;
  constructor(
    private userStorage: UserStorageProvider,
  ) {

  }
  permission(facility, permissionNmae): any {
    if(this.userStorage.get()){
      this.permissionlist = this.userStorage.get().user.role.facility;
      const index = this.permissionlist.findIndex(x => x.name === facility);
      if (index > -1) {
        const index2 = this.permissionlist[index].permission.findIndex(x => x.permissionName === permissionNmae);
        // console.log(this.permissionlist[index].permission);
        if (index2 > -1) {
          return this.permissionlist[index].permission[index2].status;
        }
      }
    }
    
  }
  sidePanelpermission(facility): any {
    if (this.userStorage.get()) {
      this.permissionlist = this.userStorage.get().user.role.facility;
      const index = this.permissionlist.findIndex(x => x.name === facility);
      if (index > -1) {
        // tslint:disable-next-line:triple-equals
        return this.permissionlist[index].status == '1' ? true : false;
      }
    }
  }
  sidePanelURL(facility): any {
    if (this.userStorage.get()) {
      this.permissionlist = this.userStorage.get().user.role.facility;
      const index = this.permissionlist.findIndex(x => x.name === facility);
      if (index > -1) {
        // tslint:disable-next-line:triple-equals
        return this.permissionlist[index].url;
      }
    }
    
  }
  AuthGuardPermission(facility): any {
    if (this.userStorage.get()) {
      this.permissionlist = this.userStorage.get().user.role.facility;
      const index = this.permissionlist.findIndex(x => x.url === facility);
      if (index > -1) {
        // tslint:disable-next-line:triple-equals
        return this.permissionlist[index].status == '1' ? true : false;
      }
    }
    
  }
}

