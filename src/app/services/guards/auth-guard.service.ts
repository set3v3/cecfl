import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';
import { UserStorageProvider } from '../storage/user-storage.service';
import { PermissionProvider } from '../permission/permission.service';
import { ToastProvider } from '../../shared/modules/toast/toast.provider';
@Injectable()
export class AuthGuard implements CanActivate {
  constructor(
    private myRoute: Router,
    private userStorage: UserStorageProvider,
    private Permission: PermissionProvider) {
  }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
      if (this.userStorage.get()) {
        //console.log('permission=>',this.userStorage.get().user.role.facility)
        const urarry = state.url.split('/');
        if (urarry.length > 0) {
          const length = urarry.length > 3 ? 3 : urarry.length;
          const url = urarry[length - 1];
          //console.log('url=>', url);
          if(url === 'proposal-add' && urarry.length == 3){ 
            localStorage.removeItem('proposal_id');
            localStorage.removeItem('stepCompleted');
          }

          if (
            url === 'dashboard' || 
            url === 'registration' ||
            url === 'no-access' || 
            //url === 'edit-profile' ||
            url === '404' 
            //||   url === 'fontend'
            ) {
            return true;
          } else if (this.Permission.AuthGuardPermission(url)) {
            return true;
          }
          this.myRoute.navigate(['post-auth/no-access']);
        }

      } else {
        this.myRoute.navigate(['pre-auth/login']);

      }
  }
}


@Injectable({
  providedIn: 'root'
})
export class EntityGuard implements CanActivate {
  constructor(
    private myRoute: Router,
    private userStorage: UserStorageProvider,
    private toast: ToastProvider,
  ) { }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    
      if (this.userStorage.get()) {
        if(this.userStorage.get().user.user_type == 1){
          return true;
        }
        else{
          this.toast.error('You don\'t have previlage to access this page.');
          this.myRoute.navigate(['post-auth/dashboard']);
        }
      } else {
        this.myRoute.navigate(['pre-auth/login'], {
          queryParams: { referer: state.url }
        });
      }
  }

}
