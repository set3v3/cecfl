import { UserStorageProvider } from './../storage/user-storage.service';
import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpResponse, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { AppConst } from '../../app.constants';
import { ToastProvider } from '../../shared/modules/toast/toast.provider';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import { CryptoProvider } from '../crypto/crypto.service';
import { Router } from '@angular/router';
import { AuthService } from '../apis/auth.service';
import { LogOutApiResponce } from 'src/app/models/auth';
//import { Observable } from 'rxjs/Rx';

@Injectable()
export class HttpConfigInterceptor implements HttpInterceptor {
  // userInfo: UserInfo;
  param: any;
  constructor(private toast: ToastProvider,
    // tslint:disable-next-line:align
    private userStorage: UserStorageProvider,
    // tslint:disable-next-line:align
    private fileEncryption: CryptoProvider,
    // tslint:disable-next-line:align
    private router: Router,
    // tslint:disable-next-line:align
    private authApi: AuthService

  ) {
  }
  intercept(req: HttpRequest<any>, next: HttpHandler) {
    let customReq;
    const urarry = req.url.split('/');
    let url;
    if (urarry.length > 0) {
      url = urarry[urarry.length - 1];
      //console.log(url);
    }
    //console.log('storage=>',this.userStorage.get());
    if ((url !== 'supportForm') && (req.method.toLowerCase() === 'post' || req.method.toLowerCase() === 'put')) {
      customReq = req.clone({
        withCredentials: true,
        headers: new HttpHeaders({
          apikey: AppConst.API_KEY,
          Authorization: this.userStorage.get() ? 'Bearer ' + this.userStorage.get().accessToken : ''
        }),
        /*setHeaders: {
            'Authorization': this.userStorage.get() ? 'Bearer ' + this.userStorage.get().accessToken : '',
        },*/
        //body: { EncryptionData: this.fileEncryption.encryptObj(req.body) }
        body: req.body
      });
    } else {
      customReq = req.clone({
        withCredentials: true,
        headers: new HttpHeaders({
          apikey: AppConst.API_KEY,
          Authorization: this.userStorage.get() ? 'Bearer ' + this.userStorage.get().accessToken : ''
        })
        /*setHeaders: {
            'Authorization': this.userStorage.get() ? 'Bearer ' + this.userStorage.get().accessToken : '',
        },*/
      });
    }
    return next.handle(customReq).do((event: HttpEvent<any>) => {
      //console.log("event=>",event);
      if (event instanceof HttpResponse) {
        const urarry = event.url.split('/');
        if (urarry.length > 0) {
          const url = urarry[urarry.length - 1];
          if (event.body.hasOwnProperty('data') && url != 'en.json') {
            //event.body.data = this.fileEncryption.decryptObj(event.body.data);  // Encryption
            event.body.data = event.body.data;  // Encryption
            return event;
          }
        }
      }
    }, (err: any) => {
      //console.log('err', err);
      if (err instanceof HttpErrorResponse) {
        console.log(err);
        if (err.status === AppConst.HTTP_STATUS.BAD_REQUEST || err.status === AppConst.HTTP_STATUS.UN_AUTHORIZED) {
          this.toast.error(err.error.message);
          if (err.status === AppConst.HTTP_STATUS.UN_AUTHORIZED) {
            this.logOut();
          }
        } else if (err.status === AppConst.HTTP_STATUS.SERVER_INTERNAL_ERROR) {
          const error = err.error.Errors;
          //console.log('err status ', error);
          for (const k in error) {
            if (k) {
              this.toast.error(error[k][0].message);
            }
          }
        } else {
          console.log('err status 2 ', err);
          const error = err.error;
          for (const k in error) {
            if (k) {
              this.toast.error(error[k][0]);
            }
          }
        }
      }
    });
  }
  logOut() {
    this.userStorage.clear();
    this.router.navigate(['/']);
    this.authApi.logout(this.param).subscribe((responseData: LogOutApiResponce) => {
      if (responseData.success) {
        this.toast.success(responseData.message);
      }
    }, error => {
    });
  }
}
