/**
 * @param name translate key should be set.
 * @param matIcon for icon material icon only allowed. check the full list from here https://material.io/icons/
 * @param routeUrl this is for active url.
 */
export interface AppSideNav {
  name: string;
  matIcon: string;
  routeUrl: string;
  canAccess: boolean;
  childmenu: Array<Accordionmenu>;
}
/**
 * @param name translate key should be set.
 * @param matIcon for icon material icon only allowed. check the full list from here https://material.io/icons/
 * @param childmenu this is for chid menu list.
 */
export interface AccordionSideNav {

  name: string;
  matIcon: string;
  childmenu: Array<AppSideNav>;
}
export interface Accordionmenu {
  name: string;
  matIcon: string;
  routeUrl: string;
}
