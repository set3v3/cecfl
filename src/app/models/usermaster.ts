export interface UserListInfo {
  firstName: string;
  surName: string;
  designation: string;
  phoneNumber: string;
  secondPhoneNumber: string;
  activeStatus: string;
  _id: number;
  facilityId: FacilityInfo;
}
export interface UserlistApiResponse {
  resCode: number;
  success: boolean;
  message: string;
  result: string;
  totalCount: number;
  totalPage: number;
  page: number;
  data: Array<UserListInfo>;
}
export interface FacilityInfo {
  facilityName: string;
  _id: number;
}
export interface UsereditPermissionApiResponse {
  resCode: number;
  success: boolean;
  message: string;
  result: string;
}

export interface ADMINUserlistApiResponse {
  resCode: number;
  success: boolean;
  page: number;
  message: string;
  result: string;
  totalCount: number;
  totalPage: number;
  data: Array<AdminUserListInfo>;
}
export interface AdminUserListInfo {
  user_id: string;
  email: string;
  phoneNumber: string;
  firstName: string;
  surName: string;
  roleId: AdminUserRoleInfo;
  activeStatus: string;
}
export interface AdminUserRoleInfo {
  _id: string;
  roleName: string;
}
export interface AddAdminUserApiResponse {
  resCode: number;
  success: boolean;
  message: string;
}
export interface EditAdminUserApiResponse {
  resCode: number;
  success: boolean;
  message: string;
  result: string;
}
export interface ImeiApiResponse {
  resCode: number;
  data: ImeiApiInfo;
}
export interface ImeiApiInfo {
  deviceId: string;
  _id: string;
  deviceToken: string;
}
export interface AdminUserReportExportApiResponse {
  resCode: number;
  success: boolean;
  message: string;
  result: string;
  data: string;
}
export interface AppUserReportExportApiResponse {
  resCode: number;
  success: boolean;
  message: string;
  result: string;
  data: string;
}
export interface AddressDetailsApiResponse {
  resCode: number;
  success: boolean;
  designationDeatils: Array<any>;
  addressDetails: Array<AddressDetailsInfo>;
  message: string;
  result: string;
}
export interface AddressDetailsInfo {
  _id: string;
  Zone: string;
  State: string;
  LGA: Array<any>;
}
export interface StateApiResponse {
  resCode: number;
  success: boolean;
  totalCount: number;
  data: Array<StateInfo>;
}
export interface StateInfo {
  id: number;
  state: string;
  state_code: string;
}

export interface ProjectApiResponse {
  resCode: number;
  success: boolean;
  totalCount: number;
  data: Array<ProjectInfo>;
}
export interface ProjectInfo {
  id: number;
  projectUniqueId: string;
  projectName: string;
}

export interface MemberApiResponse {
  resCode: number;
  success: boolean;
  totalCount: number;
  data: Array<any>;
}
export interface MapSearchApiResponce {
  success: boolean;
  result: string;
  message: string;
  resCode: number;
}
