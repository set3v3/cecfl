import { NgModule } from '@angular/core';
import { Error404Component } from './error-404/error-404.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { SharedModule } from '../shared/shared.module';
import { NoAccessComponent } from './no-access/no-access-component';
import { FormsModule } from '@angular/forms';
import { HomeHeaderComponent } from '../modules/web-site/home-header/home-header.component';
import { VideoComponent } from '../modules/web-site/video/video.component';
import { NewsComponent } from '../modules/web-site/news/news.component';
import { ResourcesComponent } from '../modules/resources/resources.component';
import { ProvidersComponent } from '../modules/web-site/providers/providers.component';
import { CommissionComponent } from '../modules/web-site/commission/commission.component';
import { FaqComponent } from '../modules/web-site/faq/faq.component';
import { FooterWebsiteComponent } from '../modules/web-site/footer-website/footer-website.component';
import { ContactComponent } from '../modules/web-site/contact/contact.component';
import { MenuComponent } from '../modules/web-site/menu/menu.component';
import { InnearHeaderComponent } from './innear-header/innear-header.component';
import { SearchDetailsComponent } from '../modules/web-site/search-details/search-details.component';
import { FaqDetailsComponent } from '../modules/web-site/faq-details/faq-details.component';
import { NewsDetailsComponent } from '../modules/web-site/news-details/news-details.component';








@NgModule({
  imports: [
    SharedModule,
    FormsModule,
   
  ],
  declarations: [Error404Component, HeaderComponent, FooterComponent,
     NoAccessComponent, MenuComponent, HomeHeaderComponent,NewsComponent,
      VideoComponent, ResourcesComponent, ProvidersComponent, CommissionComponent, 
      FaqComponent, FaqDetailsComponent, NewsDetailsComponent, FooterWebsiteComponent, ContactComponent ,InnearHeaderComponent, SearchDetailsComponent,],


  exports: [ InnearHeaderComponent ,Error404Component, HeaderComponent, FooterComponent, MenuComponent, HomeHeaderComponent,NewsComponent, VideoComponent, ResourcesComponent,
     ProvidersComponent, CommissionComponent, FaqComponent, FaqDetailsComponent, NewsDetailsComponent, FooterWebsiteComponent, ContactComponent, SearchDetailsComponent, ]
})
export class ComponentsModule { }
