import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { Router } from '@angular/router';
import { CommonService } from 'src/app/services/apis/common.service';
import { TranslateService } from '@ngx-translate/core';
import { UserMasterService } from 'src/app/services/apis/usermaster';
import { NgxUiLoaderService, SPINNER } from 'ngx-ui-loader';
import { UserStorageProvider } from 'src/app/services/storage/user-storage.service';
import { ProjectService } from 'src/app/services/apis/project.service';
import { ToastProvider } from 'src/app/shared/modules/toast/toast.provider';

@Component({
  selector: 'app-innear-header',
  templateUrl: './innear-header.component.html',
  styleUrls: ['./innear-header.component.scss']
})
export class InnearHeaderComponent implements OnInit {

  showSearch: boolean = false;
  showTopNav: boolean = true;
  Citylisting: Array < any > ;
  searchParam: any;
  srchCity : any;
  srchCat : any;
  spinnerType = SPINNER.rectangleBounce;
  language;
  userInfo: any;
  pageurl:any;
  projectParam : any={}
  projectData : any=[]
  remainDays : any;
  remainHrs : any;
  remainMin : any;
  remainSec : any;
  @Output('citySrchListner') citySrchListner = new EventEmitter<any>();
  constructor(
    private myRoute: Router,
    private common: CommonService,
    private translateService: TranslateService,
    private UserMasterApi: UserMasterService,
    private router: Router,
    private ngxService :NgxUiLoaderService,
    private userStorage: UserStorageProvider,
    private projectApi: ProjectService,
    private translate: TranslateService,
    private toast: ToastProvider,
  ) {
    this.userInfo = this.userStorage.get();
    this.Citylisting = [];
    this.srchCity = '';
    this.srchCat = '';
    
   }

  ngOnInit() {
    this.common.currLang.subscribe(
      res =>{
        this.language = res
      }
    )
    this.getCityList();
    this.latestProject();

    this.pageurl= this.myRoute.url;
    //console.log(this.pageurl)
  }

  latestProject(){
    this.projectParam.page = 1;
    this.projectApi.activeProject().subscribe(
      res=>{
        
        this.projectData=res.data;        
        let self = this;
        setInterval(function(){ 
          let date1 ;
          let date2 ;  
          date1 = new Date(self.projectData[0].endDate);
          date2 = new Date();  
          let res = Math.abs(date1 - date2) / 1000;            
          // get total days between two dates
          self.remainDays = Math.floor(res / 86400);                                 
          // get hours        
          self.remainHrs = Math.floor(res / 3600) % 24;               
          // get minutes
          self.remainMin = Math.floor(res / 60) % 60;          
          // get seconds
          self.remainSec = Math.floor(res % 60);  
        }, 1000);
      }
    )
  }
  getCityList() {
    if(this.userStorage.getCity()){
      this.Citylisting = this.userStorage.getCity();
    }else{
      this.searchParam = {
        search: 'PR',
      };
      this.ngxService.start();
      this.UserMasterApi.citySearch(this.searchParam).subscribe((res: any) => {
        if (res.success) {
          this.userStorage.setCity(res.data);
          this.Citylisting = res.data;
          this.ngxService.stop();
        }
      }, (err) => {
        this.ngxService.stop();
      });
    }
    
  }

  //mobile menu toggle
  menuToggle() {
    var x = document.getElementById("menu");
    if (x.style.display === "block") {
      x.style.display = "none";
    } else {
      x.style.display = "block";
    }
  } 


  // Top Search Show Hide
  toggleSearch() {
    this.showSearch = !this.showSearch;
    this.showTopNav = !this.showTopNav;
  }


  navigateRegister(){
    this.myRoute.navigateByUrl('/pre-auth/registered')
  }
  navigateLogin(){
    this.myRoute.navigateByUrl('/pre-auth/login')
  }


  changeLang(language: string) {
		this.language = language;
		this.common.currLang.next(this.language);
    this.translateService.use(this.language);
  }

  gotoSearchList(){
    const srchData = {
      city : this.srchCity,
      category : this.srchCat
    }
    if(this.srchCat!=''){
      this.citySrchListner.emit(srchData);
      this.router.navigate(['/search-list-view',this.srchCat,this.srchCity]);
    }
    
  }

  gotoSearchListMap(){
    const srchData = {
      city : this.srchCity,
      category : this.srchCat
    }
    if(this.srchCity!= '' && this.srchCat!=''){
      this.citySrchListner.emit(srchData);
      this.router.navigate(['/search-map-view',this.srchCat,this.srchCity]);
    }else{
      this.toast.error(this.translate.instant('MESSAGE.REQUIRED_MESSAGE'));
    }
  }
}
