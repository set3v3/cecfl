// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  api_base_url: 'http://localhost:8000',
  file_base_url: 'http://localhost:8000',
  assets_image_url: 'http://localhost:4200/assets/images',
  project_folder_url: 'http://localhost:4200',
  
  /*api_base_url: 'https://10.0.10.200/esteps-fund/estep-server/public',
  file_base_url: 'https://10.0.10.200/esteps-fund/estep-server/public',
  assets_image_url: 'https://10.0.10.200/esteps-fund/estep/assets/images',
  project_folder_url: 'http://localhost:4200',*/
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
