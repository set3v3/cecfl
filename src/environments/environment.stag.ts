export const environment = {
	environmentName: 'Staging',
	production: true,
	api_base_url: 'http://34.196.238.149/staging_esteps/estep-server/public',
	file_base_url: 'http://34.196.238.149/staging_esteps/estep-server/public',
	assets_image_url: 'http://34.196.238.149/staging_esteps/assets/images',
	project_folder_url: 'http://34.196.238.149/staging_esteps',
};
