// export const environment = {
//   production: true,
//   api_base_url: 'https://10.0.10.200/esteps-fund/estep-server/public',
//   file_base_url: 'https://10.0.10.200/esteps-fund/estep-server/public',
//   assets_image_url: 'https://10.0.10.200/esteps-fund/estep-server/assets/images',
//   project_folder_url: 'https://10.0.10.200/esteps-fund/estep',
// };

export const environment = {
	production: true,
	api_base_url: 'https://www.impactocomunitariopr.org/estep-server/public',
	file_base_url: 'https://www.impactocomunitariopr.org/estep-server/public',
	assets_image_url: 'https://www.impactocomunitariopr.org/assets/images',
	project_folder_url: 'https://www.impactocomunitariopr.org',
};